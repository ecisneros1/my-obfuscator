package com.itpatagonia.obfuscator.configuration;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Getter
@Setter
public class IdentityConfig {

    private String hostMSC;
    private int portMSC;
    private String certificateFileMSC;
    private String keystorePasswordMSC;
    private String keyPasswordMSC;
    private String applicationName;
    private String moduleName;
    private String userOfuMSC;
    private String domainMSC;
    private String defaultIdMSC;
    
}
