package com.itpatagonia.obfuscator.configuration;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class AppContext {
    
    public static final ApplicationContext context = new ClassPathXmlApplicationContext("file:/apps/obfuscator/config/config.xml",
                                                                                        "file:/apps/obfuscator/config/msc.xml");
    
    //public static final ApplicationContext context = new ClassPathXmlApplicationContext("file:/sistemas/eproyectos/OFU/app_root/cfg/config/config.xml");
    //public static final ApplicationContext mscContext = new ClassPathXmlApplicationContext("file:/sistemas/eproyectos/OFU/app_root/cfg/config/msc.xml");
    //public static final ApplicationContext context = new ClassPathXmlApplicationContext("/config-testing.xml");
    
    public static MainConfig mainConfig = (MainConfig) context.getBean("mainConfig");

    public static IdentityConfig identityConfig = (IdentityConfig) context.getBean("identityConfig");

}
