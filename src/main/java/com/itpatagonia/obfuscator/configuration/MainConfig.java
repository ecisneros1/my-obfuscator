package com.itpatagonia.obfuscator.configuration;

import lombok.Getter;
import lombok.Setter;

/**
 * This bean maps the basic configurations for the application setted on config.xml
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Getter @Setter
public class MainConfig {

    private String logoPath;
    private String logsDir;
    private String xmlDir;
    private String xmlHistoryDir;
    
    private String ldapHost;
    private String ldapPort;
    private String ldapBaseDN;
    private String ldapUser;
    private String ldapPass;
    
    private String identityProvider;

}
