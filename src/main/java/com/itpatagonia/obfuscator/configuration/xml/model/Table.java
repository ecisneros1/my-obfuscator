/**
 * This class maps a Table, this represents a table of the DB to be obfuscated.
 */
package com.itpatagonia.obfuscator.configuration.xml.model;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@XStreamAlias("table")
@Getter @Setter
public class Table {
    
    @XStreamAlias("name")
    private String name;
    
    @XStreamAlias("blockSize")
    private String blockSize;
    
    @XStreamAlias("columns")
    private List<Column> columns;
    
    @XStreamAlias("hasTrigger")
    private boolean hasTrigger = false;

    public Table(){
        
    }    
}
