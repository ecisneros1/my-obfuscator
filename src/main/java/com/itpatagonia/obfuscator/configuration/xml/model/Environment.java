package com.itpatagonia.obfuscator.configuration.xml.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@XStreamAlias("environment")
public class Environment {
    /**
     * The name of this environment.
     */
    @XStreamAlias("name")
    private String name;
    /**
     * The server ip or domain
     */
    @XStreamAlias("host")
    private String host;
    /**
     * The port number of the base
     */
    @XStreamAlias("port")
    private String port;
    /**
     * The service name or instance
     */
    @XStreamAlias("serviceName")
    private String serviceName;
    /**
     * The id used by the identityManager to return a specific credential.
     */
    @XStreamAlias("idMSC")
    private String idMSC;
    /**
     * The environment specific DB name to connect with, instead of Database.name.
     */
    @XStreamAlias("overrideName")
    private String overrideName;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return 
     */
    public String getHost() {
        return host;
    }

    /**
     * 
     * @param host 
     */
    public void setHost(String host) {
        this.host = host;
    }

     /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the idMSC
     */
    public String getIdMSC() {
        return idMSC;
    }

    /**
     * @param idMSC the idMSC to set
     */
    public void setIdMSC(String idMSC) {
        this.idMSC = idMSC;
    }
    /**
     * Get the overrideName 
     * @see #overrideName 
     * @return overrideName  
     */
    public String getOverrideName() {
        return overrideName;
    }
    /**
     * Set the overrideName
     * @see #overrideName
     * @param overrideName 
     */
    public void setOverrideName(String overrideName) {
        this.overrideName = overrideName;
    }

    
    @Override
    public String toString() {
        return "Environment{" + "name=" + name + ", host=" + host + ", port=" + port + ", serviceName=" + serviceName + ", overrideName=" + overrideName +'}';
    }
    
}
