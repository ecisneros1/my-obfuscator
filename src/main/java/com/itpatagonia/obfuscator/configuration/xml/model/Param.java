/*
 * 
 */
package com.itpatagonia.obfuscator.configuration.xml.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@XStreamAlias("param")
public class Param {
    
    @XStreamAlias("name")
    private String name;
    
    @XStreamAlias("value")
    private String value;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
