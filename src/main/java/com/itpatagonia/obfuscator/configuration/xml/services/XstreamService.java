package com.itpatagonia.obfuscator.configuration.xml.services;

import com.itpatagonia.obfuscator.configuration.AppContext;
import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.thoughtworks.xstream.XStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author arodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class XstreamService {

    public XStream xstream = new XStream();
    
    private final String path = AppContext.mainConfig.getXmlDir();

    public String convertToXML(Object obj) {
        xstream.processAnnotations(Database.class);
        return xstream.toXML(obj);
    }

    public String convertToXML(Object obj, Class clase) {
        xstream.processAnnotations(clase);
        return xstream.toXML(obj);
    }

    public Object convertToObject(String xml) {
        return xstream.fromXML(xml);
    }

    public void writeToFile(Object xml, String fileName) {

        File xmlFile = new File(path, fileName);
        BufferedWriter bw;
        try {
            bw = new BufferedWriter(new FileWriter(xmlFile));
            xstream.processAnnotations(Database.class);
            xstream.toXML(xml, bw);
            bw.close();
        } catch (IOException ex) {
            log.error("Ocurrió un error al escribir el objeto xml!");
            log.error("Error: " + ex.getMessage());
        }
    }

    public Object loadXmlFile(String fileName) {
        File xmlFile = new File(path, fileName);
        try {
            xstream.processAnnotations(Database.class);
            return xstream.fromXML(xmlFile);
        } catch (Exception ex) {
            log.error("Ocurrió un error al leer el archivo xml!");
            log.error("Error: " + ex.getMessage());
        }

        return null;
    }

}
