/**
 * This class maps a Column, this represents a Column of a table to be obfuscated.
 */
package com.itpatagonia.obfuscator.configuration.xml.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@XStreamAlias("column")
@Getter @Setter
public class Column {
    @XStreamAlias("name")
    private String name;
    
    @XStreamAlias("dataType")
    private String dataType;
    
    @XStreamAlias("ofuscationMethod")
    private OfuscationMethod ofuscationMethod;

    @XStreamAlias("isPK")
    private boolean isPK = false;
    
    @XStreamAlias("isFK")
    private boolean isFK = false;
    
    public Column(){
        
    }
    
    public String toString(){
        return name;
    }
    
}
