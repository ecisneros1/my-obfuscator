/**
 * This class maps a Database, this represents the configuration of the DB to be obfuscated.
 */
package com.itpatagonia.obfuscator.configuration.xml.model;

import com.itpatagonia.obfuscator.process.exceptions.EnvironmentNotFoundException;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@XStreamAlias("database")
public class Database {

    @XStreamAlias("name")
    private String name;
    
    @XStreamAlias("appOwner")
    private String appOwner;
    
    @XStreamAlias("vendorDataBase")
    private String vendorDataBase;
    
    @XStreamAlias("versionDataBase")
    private String versionDataBase;
    
    @XStreamAlias("environments")
    private List<Environment> environments;
    
   
    
    @XStreamAlias("dateModification")
    private String dateModification;
    
    @XStreamAlias("dateCreation")
    private String dateCreation;
            
    @XStreamAlias("tables")
    private List<Table> tables;
    
    
    
    public Database(){
        
    }
    
    /**
     * @return the name
     */
    
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the vendorDataBase
     */
    public String getVendorDataBase() {
        return vendorDataBase;
    }

    /**
     * @param vendorDataBase the vendorDataBase to set
     */
    public void setVendorDataBase(String vendorDataBase) {
        this.vendorDataBase = vendorDataBase;
    }

    /**
     * @return the versionDataBase
     */
    public String getVersionDataBase() {
        return versionDataBase;
    }

    /**
     * @param versionDataBase the versionDataBase to set
     */
    public void setVersionDataBase(String versionDataBase) {
        this.versionDataBase = versionDataBase;
    }

   
    /**
     * @return the dateModification
     */
    public String getDateModification() {
        return dateModification;
    }

    /**
     * @param dateModification the dateModification to set
     */
    public void setDateModification(String dateModification) {
        this.dateModification = dateModification;
    }

    /**
     * @return the tables
     */
    public List<Table> getTables() {
        return tables;
    }

    /**
     * @param tables the tables to set
     */
    public void setTables(List<Table> tables) {
        this.tables = tables;
    }

    /**
     * @return the dateCreation
     */
    public String getDateCreation() {
        return dateCreation;
    }

    /**
     * @param dateCreation the dateCreation to set
     */
    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    /**
     * @return the environments
     */
    public List<Environment> getEnvironments() {
        return environments;
    }

    /**
     * @param environments the environments to set
     */
    public void setEnvironments(List<Environment> environments) {
        this.environments = environments;
    }
    
    /**
     * @return the environments
     */
    public Environment getEnvironment(String environmentWanted) throws EnvironmentNotFoundException {
        Iterator iEnvironment = getEnvironments().iterator();
        
        while(iEnvironment.hasNext()){
            Environment e = (Environment) iEnvironment.next();
            if(environmentWanted.equals(e.getName())){
               return e; 
            }
        }
        throw new EnvironmentNotFoundException();
    }

    @Override
    public String toString() {
        return "Database{" + "name=" + name + ", vendorDataBase=" + vendorDataBase + ", versionDataBase=" + versionDataBase + ", environments=" + environments + ", dateModification=" + dateModification + ", dateCreation=" + dateCreation + ", tables=" + tables + '}';
    }

//    
//    public String toStringq(){
//        StringBuilder sb = new StringBuilder(1000);
//        
//        sb.append("Name: " + this.getName());
//        sb.append(",\n vendorDataBase: " );
//        sb.append( this.vendorDataBase);
//        sb.append(",\n versionDataBase: ");
//        sb.append( this.versionDataBase);
//        sb.append(", \n environments:{ \n");
//        for(Environment environment : getEnvironments()){
//            sb.append(environment.toString());
//            sb.append("\n");
//        }        
//        sb.append("},\ndateCreation: ");
//        sb.append(this.dateCreation);
//        sb.append(",dateModification: ");
//        sb.append(this.dateModification);
//        sb.append("\n-Tables: \n");
//        Iterator i = this.getTables().iterator();
//        Iterator ib;
//        while(i.hasNext()){
//            Table table = (Table) i.next();
//            sb.append( "-" );
//            sb.append( table.toString() );
//            ib = table.getColumns().iterator();
//            sb.append("\n--Columns: \n");
//            
//            while(ib.hasNext()){
//                Column column = (Column) ib.next();
//                sb.append( "---" );
//                sb.append( column.toString() );
//                sb.append( "\n" );
//            }
//            
//        }
//        
//        return sb.toString();
//    }

    /**
     * @return the appOwner
     */
    public String getAppOwner() {
        return appOwner;
    }

    /**
     * @param appOwner the appOwner to set
     */
    public void setAppOwner(String appOwner) {
        this.appOwner = appOwner;
    }

   
    
}
