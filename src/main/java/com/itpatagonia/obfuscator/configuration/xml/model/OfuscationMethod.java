/**
 * This class maps a Obfuscation Method, this represents a method of obfuscation to be used on a specific column.
 */
package com.itpatagonia.obfuscator.configuration.xml.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.util.List;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@XStreamAlias("ofuscationMethod")
public class OfuscationMethod {
    
    public static enum Methods {
        VALOR_FIJO("Valor fijo"),
        COPIAR_COLUMNA("Copiar de otra columna"),
        INTERCAMBIO_LETRAS("Intercambio de letras");
//        DICCIONARIO("diccionario"),
//        REORDENAMIENTO(""),
//        CONJUNTO("");    
        
        String name;
        
        Methods(String name) {
            this.name = name;
        }

        public String getDisplayName() {
            return this.name;
        }
    }
    
    @XStreamAlias("name")
    private String name;

    @XStreamAlias("params")
    private List<Param> params;
    
    public OfuscationMethod(){
        
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the params
     */
    public List<Param> getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(List<Param> params) {
        this.params = params;
    }
    
    public Methods getMethod(){       
       return Methods.valueOf(name.toUpperCase());   
    }
}
