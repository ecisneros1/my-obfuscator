package com.itpatagonia.obfuscator.web.actions;

import com.itpatagonia.obfuscator.process.exceptions.MSCCertificateException;
import com.itpatagonia.obfuscator.process.exceptions.NoLoggedUserException;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.services.AuthentificationLDAP;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import com.opensymphony.xwork2.ActionSupport;
import com.unboundid.ldap.sdk.LDAPException;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class LoginAction extends ActionSupport implements SessionAware {
    
    private String username;
    private String password;
    private Map<String, Object> session;
    
    @Override
    public String execute() {
        log.info("Usuario " + username + " intentando logearse...");
        try {
            if (username == null || password == null) {
                addActionError(getText("global.action.login.error.required.username"));
                return INPUT;
            }
            if (!AuthentificationLDAP.authenticate(username, password)) {
                log.error("Credenciales Invalidas");
                addActionError(getText("global.action.login.error.invalidcrentials"));
                return INPUT;
            }
            
            //Log in to MSC.
            IdentityService identityService = IdentityService.getInstance(username);
            
            //Setting session variables            
            this.session.put("username", username);
            this.session.put("rol", "user");

            //MSC SESSION
            this.session.put("identityService", identityService);

            //Obteniendo permisos 
            this.session.put("permisos", identityService.getPermissions());

            log.info("Usuario logeado: " + username);

        } catch (LDAPException ex) {
            log.error("Credenciales Invalidas LDAP, username: " + username, ex);
            addActionError(getText("global.action.login.error.invalidcrentials"));
            return INPUT;

        } catch (NoLoggedUserException ex) {
            log.error("Credenciales Invalidas para MSC o permisos insuficientes, username: " + username, ex);
            addActionError(getText("global.action.login.error.invalidMSCCrentials"));
            return INPUT;
        } catch (MSCCertificateException ex) {
            log.error("Certificado o keys inválidos.");
            addActionError(getText("global.action.login.error.invalidMSCCertificate"));
            return INPUT;
        } catch(ObfuscatorException ex){
            log.error(ex.getMessage());        
        } catch(Exception ex){
            log.error("Ha ocurrido un error inesperado.", ex);   
        }
        
        return SUCCESS;

    }
    
    @Override
    public void validate() {        
        if(username == null || username.isEmpty()){
            addActionError(getText("global.action.login.error.required.username"));
        }
        if(password == null || password.isEmpty()){
            addActionError(getText("global.action.login.error.required.password"));
        }
    }
    
    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {        
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    } 

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

}
