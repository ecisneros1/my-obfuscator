package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.services.GenericObfuscator;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class ObfuscateProcessAction extends ActionSupport implements SessionAware {

    private Map<String, Object> session;

    //in
    private String databaseName;
    private String environment;
    //outs
    private String result;
    private String message;
    private int errorCode;

    @Override
    public String execute() {
        if (!validateSession()) {
            return ERROR;
        }
        result = "error";
        log.info("-------------obfuscating----------");
        log.info("databaseName: " + databaseName);
        log.info("environment: " + environment);

        GenericObfuscator gObfuscator = new GenericObfuscator();

        try {
            gObfuscator.loadXML(databaseName);
            IdentityService identityService = (IdentityService) session.get("identityService");
            Credential credential = identityService.getCredential(gObfuscator.getDatabaseIdMSC(environment));

            errorCode = gObfuscator.process(databaseName, environment, credential);
            
        } catch (Exception e) {
            log.error("Error al obtener credencial DB - " + e.getMessage());
            errorCode = 7;
        }

        if (errorCode > 0) {
            result = "error";
            return ERROR;
        } else {
            result = "success";
        }
        return SUCCESS;
    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    private boolean validateSession() {
        log.debug("-------------VERIFICANDO SESSION");
        String username;

        try {
            username = (String) this.session.get("username");
        } catch (NullPointerException ex) {
            log.error("Usuario no logeado ha intentado realizar TestModelAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        if (StringUtils.isBlank(username)) {
            log.error("Usuario no logeado ha intentado realizar TestModelAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        return true;
    }

}
