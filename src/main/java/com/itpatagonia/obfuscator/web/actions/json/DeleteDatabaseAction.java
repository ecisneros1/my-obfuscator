package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.process.utils.ObfuscatorUtils;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author arodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class DeleteDatabaseAction extends ActionSupport implements SessionAware {
    
    private String name;     
    
    private String result; 
    private String message;
    
    private Map<String, Object> session;
    
    public String execute(){
        String usernameParam = "";
        try{
            usernameParam = (String) this.session.get("username");
        }catch(Exception ex){
            usernameParam = "";
        }
        if(StringUtils.isBlank(usernameParam)){
            result="error";
            message="Usuario no logeado ha intentado eliminar la BD: " + name ;
            return ERROR;
        }
        boolean resultOperation = ObfuscatorUtils.operateFile(name, ObfuscatorUtils.FileOperation.DELETED);
        log.info("El usuario " + usernameParam + " esta eliminando la Base de datos: " + name);
        
        if(resultOperation){
            result="success";
            message="Se ha eliminado la base de datos en el sistema OFU con éxito.";
        }else{
            result="error";
            message="No se pudo eliminar la base de datos.";
        }
                
        return SUCCESS;
    }
    
    /**
     * @param database the database to delete
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the result of the action (success or error)
     */
    public String getResult() {
        return result;
    }
    /**
     * @return the message related to this action
     */
    public String getMessage() {
        return message;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}
