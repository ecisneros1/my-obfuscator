package com.itpatagonia.obfuscator.web.actions;

import com.itpatagonia.obfuscator.configuration.AppContext;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import com.itpatagonia.obfuscator.process.services.ObfuscatorManager;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

/**
 * GUI Principal Panel's Action.
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class PanelPrincipalAction extends ActionSupport implements SessionAware {

    private Map<String, String> vendors;
    private Map<String, String> methods;
    private Map<String, Object> session;
    private String functions;
    private String defaultMSC;

    @Override
    public String execute() {
        vendors = DatabasesEnum.getVendors();
        methods = ObfuscatorManager.getMethods();

        if (this.session != null) {
            String username = (String) this.session.get("username");
            String rol = (String) this.session.get("rol");
            try {
                List listFunctions = (List) this.session.get("permisos");
                functions = StringUtils.join(listFunctions, ",");
            } catch (NullPointerException ex) {
                log.error("Un usuario intento ingresar al panelPrincipal una session válida.");
                functions = "";
            } catch (Exception ex) {
                log.error("Ocurrió un error inesperado al obtener la session.", ex);
                functions = "";
            }

            defaultMSC = AppContext.identityConfig.getDefaultIdMSC();
            if (StringUtils.isEmpty(username) && StringUtils.isEmpty(rol)) {
                return ERROR;
            }
        } else {
            return ERROR;
        }

        return SUCCESS;
    }

    public Map<String, String> getVendors() {
        return this.vendors;
    }

    /**
     * @return the methods
     */
    public Map<String, String> getMethods() {
        return methods;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @return the defaultMSC
     */
    public String getDefaultMSC() {
        return defaultMSC;
    }

    /**
     * @return the permisos
     */
    public String getFunctions() {
        return functions;
    }

}
