package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.configuration.xml.model.Environment;
import com.itpatagonia.obfuscator.process.exceptions.DatabaseConnectionException;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import com.itpatagonia.obfuscator.process.services.DatabaseManager;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import static com.opensymphony.xwork2.Action.ERROR;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 * Action for test the model and operations related.
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class CountRowsInTablesAction extends ActionSupport implements SessionAware {

    private Map<String, Object> session;

    //in
    private String databaseName;
    private Environment environment;
    private String vendorDataBase;
    private String versionDataBase;
    private List<String> tableList;
    //outs
    private String result;
    private String message;
    private Map<String, Integer> tableRowCounter;

    @Override
    public String execute() {
        if (!validateSession()) {
            return ERROR;
        }
        if (StringUtils.isBlank(databaseName)) {
            result = ERROR;
            message = "El nombre de la bd es obligatorio.";
            return ERROR;

        }
        String mscID = "";
        if (environment != null) {
            mscID = environment.getIdMSC();
        } else {
            result = ERROR;
            message = "Debe escoger un ambiente.";
            return ERROR;
        }
        DatabaseManager dm = DatabaseManager.getInstance(databaseName, environment.getHost(), environment.getPort(), environment.getServiceName(), DatabasesEnum.getDatabaseEnum(vendorDataBase, versionDataBase));

        try {
            // obtiene credential DB
            IdentityService identityService = (IdentityService) this.session.get("identityService");
            Credential credential = identityService.getCredential(mscID);

            // obtiene conexion DB
            Connection conn = dm.getConnection(credential);
            dm.setIdentifierQuoteString(conn);
            
            tableRowCounter = dm.getNumberOfRowsPerTable(conn, tableList);
            result = SUCCESS;

        } catch (DatabaseConnectionException ex) {
            result = ERROR;
            message = "No se pudo conectar debido a: " + ex.getMessage();
            log.error("Error al comprobar la conexión con la BD: " + databaseName + ", No se pudo conectar debido a: " + ex.getMessage());
            return ERROR;
        } catch (ObfuscatorException ex) {
            result = ERROR;
            message = "No se pudo realizar el conteo debido a: " + ex.getMessage();
            log.error("Error al obtener la credencial o al realizar alguna tarea del conteo para la BD con MSCID: " + mscID + ", database: " + databaseName + ", Exception: " + ex.getMessage());
            return ERROR;
        } catch (Exception ex) {
            result = ERROR;
            message = "Error inesperado, revisar los logs para obtener información sobre el error. ";
            log.error("Error inesperado al contar los registros para las tablas de la BD: " + databaseName + ", Exception: " + ex.getMessage());
            return ERROR;
        }
        message = "Se ha obtenido con éxito el conteo de registros para las tablas.";

        return result;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    /**
     *
     * @param tableList
     */
    public void setTableList(List<String> tableList) {
        this.tableList = tableList;
    }

    /**
     *
     * @param vendorDataBase
     */
    public void setVendorDataBase(String vendorDataBase) {
        this.vendorDataBase = vendorDataBase;
    }

    /**
     *
     * @param versionDataBase
     */
    public void setVersionDataBase(String versionDataBase) {
        this.versionDataBase = versionDataBase;
    }

    // Return parameters to the view
    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    public Map<String, Integer> getTableRowCounter() {
        return tableRowCounter;
    }

    private boolean validateSession() {
        log.debug("-------------VERIFICANDO SESSION");
        String username = "";

        try {
            username = (String) this.session.get("username");
        } catch (NullPointerException ex) {
            log.error("Usuario no logeado ha intentado realizar TestModelAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        if (StringUtils.isBlank(username)) {
            log.error("Usuario no logeado ha intentado realizar TestModelAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        return true;
    }

}
