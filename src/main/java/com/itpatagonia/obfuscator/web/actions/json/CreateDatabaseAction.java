package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.services.XstreamService;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.itpatagonia.obfuscator.process.utils.ObfuscatorUtils;
import java.util.ArrayList;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author arodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class CreateDatabaseAction extends ActionSupport implements SessionAware {
    private Database database;
    private boolean isNewDataBase;
    private String previousName; 
    
    private String result; 
    private String message;
    
    private Map<String, Object> session;
    
    public String execute(){
        if(!validateSession()){
            return ERROR;
        }
        String username = (String) this.session.get("username");
        try{            
            XstreamService xstreamService = new XstreamService();
            log.info("-----------Checking data before saving xml file-------------");
            log.info("Database Name: "+ database);
            ArrayList<String> databasesXmlList = (ArrayList) ObfuscatorUtils.getXMLFiles();
            log.info("isNewDataBase: "+isNewDataBase);
            log.info("previousName: "+previousName);
            
            if(isNewDataBase) {
                if(databasesXmlList.contains(database.getName())){
                    log.info("Ya existe una BD con ese nombre, editela");
                    message = "Ya existe una BD con ese nombre, editela";
                    result = "error";
                    return ERROR;
                }                
            }else{
                if(!previousName.equals(database.getName())){
                    log.info("Actual Name is diferent from previous Name");
                    log.info("Checking if: " +database.getName()+ " exist on the list, result: " +databasesXmlList.contains(database.getName()));
                    if(databasesXmlList.contains(database.getName())){
                        log.info("Ya existe una BD con este nombre, si las guarda con el mismo nombre borrará la otra existente");
                        message = "Ya existe una BD con este nombre, si las guarda con el mismo nombre borrará la otra existente";
                        result = "error";
                        return ERROR;
                    }else{
                        log.info("El usuario " + username + " ha modificado la BD: " + database.getName() );
                        boolean result = ObfuscatorUtils.operateFile(previousName+".xml", ObfuscatorUtils.FileOperation.MODIFIED);
                        log.info("operateFile resul: "+result);
                    }  
                }else{
                    boolean result = ObfuscatorUtils.operateFile(previousName+".xml", ObfuscatorUtils.FileOperation.MODIFIED);
                    if(!result) log.error("No se pudo crear el archivo historico de: "+ previousName+".xml al modificarlo");
                    else log.info("Se modificó el archivo  "+ previousName+".xml, y se creo su archivo historico.");
                }
            }
            log.info("-----------Guardando el archivo "+previousName+".xml -------------");
            xstreamService.writeToFile(database, database.getName()+".xml");
            log.info("-----------Se ha guardado "+previousName+".xml con exito -------------");
            
            result = "success";
        }catch(SecurityException ex){
            log.error("No se pudo obtener la lista de archivos, debido a permisos insuficientes");
            return ERROR;
        } catch(Exception ex){
            log.error("Error on DatabaseAction: "+ ex);
            result = "error";
            return ERROR;
        }
        return SUCCESS;
    }
   

    /**
     * @param database the database to set
     */
    public void setDatabase(Database database) {
        this.database = database;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param isNewDataBase the isNewDataBase to set
     */
    public void setIsNewDataBase(boolean isNewDataBase) {
        this.isNewDataBase = isNewDataBase;
    }

    /**
     * @param previousName the previousName to set
     */
    public void setPreviousName(String previousName) {
        this.previousName = previousName;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    
    private boolean validateSession(){
        
        String username = "";        
        try{            
            username = (String) this.session.get("username");
        }catch(NullPointerException ex){
            
            log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");          
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";            
        
            return false;
        }
        if(StringUtils.isBlank(username)){
          log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");          
          message = "Se ha caido la session, vuelva a logearse por favor.";
          result = "error-session";
          return false;
        }
        return true;
    }
    
}
