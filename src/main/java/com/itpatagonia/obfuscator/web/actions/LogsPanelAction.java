package com.itpatagonia.obfuscator.web.actions;

import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;
/**
 * GUI Logs Panel's Action.
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class LogsPanelAction extends ActionSupport implements SessionAware {
   
    private Map<String, Object> session;
    private String functions;    
    
    public String execute() {    
        if(this.session != null){
            String username = (String) this.session.get("username");
            String rol = (String) this.session.get("rol");
            try{                
                List listFunctions = (List) this.session.get("permisos");
                functions = StringUtils.join(listFunctions, ",");
            }catch(NullPointerException ex){
                functions = "";
            }catch(Exception ex){
                functions = "";
            }            
            if( StringUtils.isEmpty(username) && StringUtils.isEmpty(rol) ) {
                return ERROR;
            } 
        }else{
             return ERROR;
        }       
        return SUCCESS;
    }
   
    @Override
    public void setSession(Map<String, Object> session) {
         this.session = session;
    }
    /**
     * @return the permisos
     */
    public String getFunctions() {
        return functions;
    }
    
}
