package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.services.XstreamService;
import com.itpatagonia.obfuscator.process.services.LogsService;
import static com.opensymphony.xwork2.Action.ERROR;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author cd115986
 */
@Log4j2
public class LogAction extends ActionSupport implements SessionAware{
    
    private String name;    
    private ArrayList<String> logs;    
    private Map<String, Object> session;
    private String result; 
    private String message;
    
    public String execute(){
        try{        
            result = "success";
            validateSession();            
            log.info("-----------Reading xml file-------------");
            logs = (ArrayList<String>) LogsService.readLog(name);
            
            return SUCCESS;
        }catch(Exception ex){
            log.error("Error: "+ ex);
            result = "error";
            return ERROR;
        }
    }
    /**
     * @return the name
     */
    public String getName() {
        log.info("xmlFileName getter call");
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        log.info("xmlFileName setter call: " + name);
        this.name = name;
    }
   
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    
    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    
    private boolean validateSession(){
        
        String username = "";        
        try{            
            username = (String) this.session.get("username");
        }catch(NullPointerException ex){
            
            log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");          
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";            
        
            return false;
        }
        if(StringUtils.isBlank(username)){
          log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");          
          message = "Se ha caido la session, vuelva a logearse por favor.";
          result = "error-session";
          return false;
        }
        return true;
    }

    /**
     * @return the logs
     */
    public ArrayList<String> getLogs() {
        return logs;
    }
}
