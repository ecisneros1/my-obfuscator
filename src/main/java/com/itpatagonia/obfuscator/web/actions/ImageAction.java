package com.itpatagonia.obfuscator.web.actions;

import com.itpatagonia.obfuscator.configuration.AppContext;

import com.opensymphony.xwork2.ActionInvocation;
import java.io.IOException;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Result;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.ServletActionContext;

@Log4j2
public class ImageAction extends ActionSupport implements Result {

    public static final String CONTENT_TYPE_SVG_XML = "image/svg+xml";

    public static final Path SVG_FILE_PATH = Paths.get(AppContext.mainConfig.getLogoPath());

    @Override
    public void execute(ActionInvocation ai) {
        try {
            log.debug("Cargando imagen: " + SVG_FILE_PATH.toString());
            
            HttpServletResponse response = ServletActionContext.getResponse();
            response.setContentType(CONTENT_TYPE_SVG_XML);
            response.getOutputStream().write(Files.readAllBytes(SVG_FILE_PATH));
            response.getOutputStream().flush();
            
        } catch (IOException e) {
            log.error("Error al leer imagen: " + e.getMessage(), e);
        }
    }

}
