package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.services.XstreamService;
import static com.opensymphony.xwork2.Action.ERROR;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 * Action to get the database definition.
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class DatabaseAction extends ActionSupport implements SessionAware{
    
    private String name;
    private Database database;
    private Map<String, Object> session;
    private String result; 
    private String message;
    
    public String execute(){
        try{        
            result = "success";
            validateSession();
            XstreamService xstreamService = new XstreamService();
            log.info("-----------Reading xml file-------------");
            log.info("xmlFileName: "+name);

            database = (Database) xstreamService.loadXmlFile(name);
            log.info("Database Name: "+ database);
            return SUCCESS;
        }catch(Exception ex){
            log.error("Error on DatabaseAction: "+ ex);
            result = "error";
            return ERROR;
        }
    }
    /**
     * @return the name
     */
    public String getName() {
        log.info("xmlFileName getter call");
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        log.info("xmlFileName setter call: " + name);
        this.name = name;
    }

    /**
     * @return the database
     */
    public Database getDatabase() {
        return database;
    }

    /**
     * @param database the database to set
     */
    public void setDatabase(Database database) {
        this.database = database;
    }
    
   
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    
    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    private boolean validateSession(){
        
        String username = "";        
        try{            
            username = (String) this.session.get("username");
        }catch(NullPointerException ex){
            
            log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");          
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";            
        
            return false;
        }
        if(StringUtils.isBlank(username)){
          log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");          
          message = "Se ha caido la session, vuelva a logearse por favor.";
          result = "error-session";
          return false;
        }
        return true;
    }
}
