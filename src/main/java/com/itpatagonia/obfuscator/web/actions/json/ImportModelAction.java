package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import com.itpatagonia.obfuscator.process.services.DatabaseManager;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 * Action for import the model and operations related.
 *
 * @author Aetcheverry
 */
@Log4j2
public class ImportModelAction extends ActionSupport implements SessionAware {

    private Map<String, Object> session;
    private String databaseName;
    private String host;
    private String port;
    private String serviceName;
    private String vendorDataBase;
    private String versionDataBase;
    private String mscID;
    private String result;
    private String message;
    private List<Table> modelImported;

    @Override
    public String execute() {

        if (!validateSession()) {
            return ERROR;
        }
        if (StringUtils.isBlank(databaseName)) {
            result = ERROR;
            message = "El nombre de la bd es obligatorio.";
            return ERROR;

        }

        DatabaseManager dm = DatabaseManager.getInstance(databaseName, host, port, serviceName, DatabasesEnum.getDatabaseEnum(vendorDataBase, versionDataBase));
        Connection conn = null;

        // obtiene credential DB
        try {
            IdentityService identityService = (IdentityService) this.session.get("identityService");
            Credential credential = identityService.getCredential(mscID);

            // obtiene conexion DB
            conn = dm.getConnection(credential);
            modelImported = dm.getModelFromMetadata(conn);

        } catch (ObfuscatorException ex) {
            log.error("No se pudo importar el modelo. " + ex.getMessage());
            result = ERROR;
            message = "Ocurrió un error inesperado al importar la metadata: " + ex.getMessage();
            return ERROR;
        } catch (Exception ex) {
            log.error("Ocurrió un error inesperado al consultar la metadata, exception: " + ex.getMessage());
            result = ERROR;
            message = "Ocurrió un error inesperado al importar la metadata, verifique la conexión y que posee permisos para leer la metadata.";
            return ERROR;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    result = ERROR;
                    message = "Se conectó a la base de datos, pero no se pudo cerrar la conexión, exception: " + ex.getMessage();
                    log.error("Error al comprobar la conexión con la BD: " + databaseName + ", No se pudo cerrar la conexión debido a: " + ex.getMessage());
                    return ERROR;
                }
            }
        }

        result = SUCCESS;

        return result;

    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    private boolean validateSession() {
        log.debug("-------------VERIFICANDO SESSION");
        String username;
        try {
            username = (String) this.session.get("username");
        } catch (NullPointerException ex) {
            log.error("Usuario no logeado ha intentado realizar ImportModel.");
            message = "Se ha caido la session, vuelva a loguearse por favor.";
            result = "error-session";

            return false;
        }
        if (StringUtils.isBlank(username)) {
            log.error("Usuario no logeado ha intentado realizar ImportModel.");
            message = "Se ha caido la session, vuelva a loguearse por favor.";
            result = "error-session";
            return false;
        }
        return true;
    }

    /**
     * @param vendorDatabase the vendorDatabase to set
     */
    public void setVendorDataBase(String vendorDatabase) {
        this.vendorDataBase = vendorDatabase;
    }

    /**
     * @param versionDatabase the versionDatabase to set
     */
    public void setVersionDataBase(String versionDatabase) {
        this.versionDataBase = versionDatabase;
    }

    /**
     * @param mscID the mscID to set
     */
    public void setMscID(String mscID) {
        this.mscID = mscID;
    }

    /**
     * @return the modelImported
     */
    public List<Table> getModelImported() {
        return modelImported;
    }

}
