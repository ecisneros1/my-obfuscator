package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.services.GenericObfuscator;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class RemoteObfuscateAction extends ActionSupport implements ServletRequestAware {
        
    //in
    private String databaseName;
    private String environment;
    private String username;
    //outs
    private String result;
    private String message;
    private int errorCode;
    private HttpServletRequest request;
    public String execute(){
        
        result = "error";        
        log.info("INI RemoteObfuscateAction----------");
        log.info("...databaseName: "+databaseName);
        log.info("...environment: " +environment);
        log.info("...user: " +username);        
        logIP();
        
        GenericObfuscator gObfuscator = new GenericObfuscator();
        
        try {
            gObfuscator.loadXML(databaseName+".xml");
            IdentityService identityService = IdentityService.getInstance(username);
            Credential credential = identityService.getCredential(gObfuscator.getDatabaseIdMSC(environment));
            
            errorCode = gObfuscator.process(databaseName, environment, credential);
        } catch (Exception e) {
            log.error("...Error al obtener credencial DB - " + e.getMessage());
            errorCode = 7;
        }
        
        if (errorCode > 0) {
            result = "error";
            return ERROR;
        } else {
            message = "Se ofusco con exito la BD: "+databaseName;
            result = "success";
        }
        return SUCCESS;
    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }
    
    /**
     * @param username the environment to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.request = hsr;
    }
    
    private void logIP(){
        // Proxy
        String userIpAddress = request.getHeader("X-Forwarded-For");
        if(userIpAddress == null) {
           userIpAddress = request.getRemoteAddr();
        }
        log.info("...IP: "+userIpAddress);
    }
    

}
