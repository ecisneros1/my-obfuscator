package com.itpatagonia.obfuscator.web.actions.reports;

import com.itpatagonia.obfuscator.process.services.ListService;
import com.itpatagonia.obfuscator.web.actions.DownloadExcelAction;
import com.itpatagonia.obfuscator.web.reports.export.AbstractReportExporter;
import com.itpatagonia.obfuscator.web.reports.export.DatabaseDetailReportExporter;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author cd115986
 */
@Log4j2
public class DatabaseDetailReportAction extends DownloadExcelAction {    
    
    private String filename;
    @Override
    public String execute() throws Exception {        
        super.execute();
                
        ByteArrayInputStream excel;
        try {
            excel = generateExcel();
            this.setFileInputStream(excel);
        } catch (IOException ex) {
            log.error("El reporte esta vacio!");
            log.error(ex);            
            return ERROR;
        }catch(SecurityException ex){
            log.error("No se pudo obtener la lista de archivos, debido a permisos insuficientes en el file system. Por ende no se pudo generar el reporte.");            
            return ERROR;
        } catch (Exception ex){
            log.error("Error inesperado en el reporte detallado de base de datos!");
            log.error(ex);
            return ERROR;
        }
       

        return SUCCESS;
    }

    private ByteArrayInputStream generateExcel() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        createExporter().exportExcel(baos);
        
        return new ByteArrayInputStream(baos.toByteArray());
    }

    private AbstractReportExporter createExporter() {
        DatabaseDetailReportExporter reportExporter = new DatabaseDetailReportExporter();
        reportExporter.setRows(ListService.getDatabaseDetailReportList(filename));
        reportExporter.setRowsEnv(ListService.getDatabaseReportList(filename));
        return reportExporter;
    } 
    
    public void setFilename(String filename){
        this.filename = filename;
        super.setFileName(filename+".xls");
    }
}
