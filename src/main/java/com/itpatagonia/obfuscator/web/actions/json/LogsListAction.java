package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.process.utils.ObfuscatorUtils;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author cd115986
 */
@Log4j2
public class LogsListAction extends ActionSupport implements SessionAware {

    private ArrayList<String> dbItems;
    private String result;
    private String message;

    private Map<String, Object> session;

    /**
     * @return the dbItems
     */
    public ArrayList<String> getDbItems() {
        return dbItems;
    }

    public String execute() {
        if (!validateSession()) {
            return ERROR;
        }
        dbItems = new ArrayList();
        try {
            dbItems = (ArrayList<String>) ObfuscatorUtils.getLogsFiles(null, "gui");
        } catch (SecurityException ex) {
            log.error("No se pudo obtener la lista de archivos logs, debido a permisos insuficientes");
            return ERROR;
        }

        return SUCCESS;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        log.debug("--------------SETEANDO SESSION");
        this.session = session;
    }

    private boolean validateSession() {
        log.debug("-------------VERIFICANDO SESSION");
        String username = "";

        try {
            username = (String) this.session.get("username");
        } catch (NullPointerException ex) {
            log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        
        if (StringUtils.isBlank(username)) {
            log.error("Usuario no logeado ha intentado realizar DatabaseListAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        
        result = "success";
        return true;
    }
}
