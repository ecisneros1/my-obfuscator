package com.itpatagonia.obfuscator.web.actions;

import com.opensymphony.xwork2.ActionSupport;
import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author gortiz
 */
public class DownloadAction extends ActionSupport {

    
    public final static String CONTENT_DISPOSITION_ATTACHMENT = "1";
    public final static String CONTENT_DISPOSITION_INLINE     = "2";
    
    public static final String CONTENT_TYPE_TEXT_PLAIN      = "text/plain";
    public static final String CONTENT_TYPE_TEXT_HTML       = "text/html";
    public static final String CONTENT_TYPE_APPLICATION_PDF = "application/pdf";
    public static final String CONTENT_TYPE_APPLICATION_XLS = "application/excel";
    
    
    private InputStream fileInputStream;

    private String contentType = "application/octet-stream";
    
    private String contentDispositionHeader = CONTENT_DISPOSITION_ATTACHMENT;
    
    private String contentDisposition;
    
    private String fileName;

    
    public String execute() throws Exception {

        if (!StringUtils.isBlank(fileName)) {
            if ( CONTENT_DISPOSITION_INLINE.equals(contentDispositionHeader) ) {
                contentDisposition = "inline; filename=\"" + fileName + "\"";
            }
            else {
                contentDisposition = "attachment; filename=\"" + fileName + "\"";
            }
        }

//        fileInputStream = new FileInputStream(new File("C:\\downloadfile.txt"));
        
        return SUCCESS;
    }

    
    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentDispositionHeader() {
        return contentDispositionHeader;
    }

    public void setContentDispositionHeader(String contentDispositionHeader) {
        this.contentDispositionHeader = contentDispositionHeader;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public void setContentDisposition(String contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
    
}
