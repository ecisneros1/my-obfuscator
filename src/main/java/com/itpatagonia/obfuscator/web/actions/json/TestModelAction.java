package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.process.exceptions.InvalidColumnDefinitionException;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.services.GenericObfuscator;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.SQLException;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 * Action for test the model and operations related.
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class TestModelAction extends ActionSupport implements SessionAware {
    
    private Map<String, Object> session;

    //in
    private String databaseName;
    private String environment;
    //outs
    private String result;
    private String message;
    
    
    @Override
    public String execute(){
        if (!validateSession()) {
            return ERROR;
        }
        
        try {
            GenericObfuscator gObfuscator = new GenericObfuscator();
            gObfuscator.loadXML(databaseName);
            
            IdentityService identityService = (IdentityService) session.get("identityService");
            Credential credential = identityService.getCredential(gObfuscator.getDatabaseIdMSC(environment));
            
            gObfuscator.initializeDBManager(environment, credential);
            
            gObfuscator.validateXML();
            
            if (gObfuscator.isXmlValid) {
                message = "El modelo es válido.";
                result = SUCCESS;
            } else {
                message = "El modelo es invalido.";
                result = ERROR;
            }
            
        } catch (InvalidColumnDefinitionException ex){
            if (ex.getErrorCode() == 2) {
                log.error("El modelo es válido pero contiene columnas PK y FK que solo podrán ser ofuscados con 'Ofuscación Lenta' ");
                message = "El modelo es inválido: " + ex.getMessage();
                //result = SUCCESS;
                result = ERROR;
            } else {
                log.error("El modelo es inválido: " + ex.getMessage());
                message = "El modelo es inválido: " + ex.getMessage();
                result = ERROR;
            }
        } catch (ObfuscatorException ex) {
            log.error("El modelo es inválido: " + ex.getMessage());
            message = "El modelo es inválido: " + ex.getMessage();
            result = ERROR;
        } catch (SQLException ex) {
            log.error("Ocurrió un error al conectar con la BD o al consultar la metadata, exception: " + ex.getMessage());
            message = "Ocurrió un error al conectar con la BD o al consultar la metadata, verifique la conexión y que posee permisos para leer la metadata.";
            result = ERROR;
        } catch (Exception ex) {
            log.error("Ocurrió un error inesperado al conectar con la BD o al consultar la metadata, exception: " + ex.getMessage());
            message = "Ocurrió un error inesperado, verifique la conexión y que posee permisos para leer la metadata.";
            result = ERROR;
        }
        
        return result;
    }
    
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    
    private boolean validateSession() {
        log.debug("-------------VERIFICANDO SESSION");
        String username;
        try {
            username = (String) this.session.get("username");
        } catch (NullPointerException ex) {
            log.error("Usuario no logeado ha intentado realizar TestModelAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        if (StringUtils.isBlank(username)) {
            log.error("Usuario no logeado ha intentado realizar TestModelAction.");
            message = "Se ha caido la session, vuelva a logearse por favor.";
            result = "error-session";
            return false;
        }
        return true;
    }

}
