package com.itpatagonia.obfuscator.web.actions.json;

import com.itpatagonia.obfuscator.process.exceptions.DatabaseConnectionException;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import com.itpatagonia.obfuscator.process.services.DatabaseManager;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

/**
 * Action for test the connection and operations related.
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class TestConnectionAction extends ActionSupport implements SessionAware {

    private Map<String, Object> session;
    private String databaseName;
    private String host;
    private String port;
    private String serviceName;
    private String vendorDataBase;
    private String versionDataBase;
    private String mscID;
    private String result;
    private String message;

    @Override
    public String execute(){
        if (!validateSession()) {
            return ERROR;
        }
        if (StringUtils.isBlank(databaseName)) {
            result = ERROR;
            message = "El nombre de la bd es obligatorio.";
            return ERROR;
            
        }
        
        DatabaseManager dm = DatabaseManager.getInstance(databaseName, host, port, serviceName, DatabasesEnum.getDatabaseEnum(vendorDataBase, versionDataBase));
        Connection conn;

        try {
            // obtiene credential DB
            IdentityService identityService = (IdentityService) this.session.get("identityService");
            Credential credential = identityService.getCredential(mscID);
            
            // obtiene conexion DB
            conn = dm.getConnection(credential);
            
        } catch (DatabaseConnectionException ex) {
            result = ERROR;
            message = "No se pudo conectar debido a: " + ex.getMessage();
            log.error("Error al comprobar la conexión con la BD: " + databaseName + ", No se pudo conectar debido a: " + ex.getMessage());
            return ERROR;
        } catch (ObfuscatorException ex) {
            result = ERROR;
            message = "No se pudo conectar debido a: " + ex.getMessage();
            log.error("Error al obtener la credencial para la BD con ID: " + mscID + ", database: " + databaseName + ", Exception: " + ex.getMessage());
            return ERROR;
        } catch (Exception ex) {
            result = ERROR;
            message = "Error inesperado, revisar los logs para obtener información sobre el error. ";
            log.error("Error inesperado al comprobar la conexión con la BD: " + databaseName + ", Exception: " + ex.getMessage());
            return ERROR;
        }
        
        message = "Se ha conectado con éxito a: " + databaseName;

        result = SUCCESS;

        return SUCCESS;
    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }
   
    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }
    
    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
    
    private boolean validateSession() {
        log.debug("-------------VERIFICANDO SESSION");
        String username;
        try {
            username = (String) this.session.get("username");
        } catch (NullPointerException ex) {
            log.error("Usuario no logueado ha intentado realizar TestConnectionAction.");
            message = "Se ha caido la session, vuelva a loguearse por favor.";
            result = "error-session";

            return false;
        }
        if (StringUtils.isBlank(username)) {
            log.error("Usuario no logueado ha intentado realizar TestConnectionAction.");
            message = "Se ha caido la session, vuelva a loguearse por favor.";
            result = "error-session";
            return false;
        }
        return true;
    }

    /**
     * @param vendorDatabase the vendorDatabase to set
     */
    public void setVendorDataBase(String vendorDatabase) {
        this.vendorDataBase = vendorDatabase;
    }

    /**
     * @param versionDatabase the versionDatabase to set
     */
    public void setVersionDataBase(String versionDatabase) {
        this.versionDataBase = versionDatabase;
    }

    /**
     * @param mscID the mscID to set
     */
    public void setMscID(String mscID) {
        this.mscID = mscID;
    }

}