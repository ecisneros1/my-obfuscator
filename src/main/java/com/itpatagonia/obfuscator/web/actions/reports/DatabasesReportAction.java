package com.itpatagonia.obfuscator.web.actions.reports;

import com.itpatagonia.obfuscator.process.services.ListService;
import com.itpatagonia.obfuscator.web.actions.DownloadExcelAction;
import com.itpatagonia.obfuscator.web.reports.export.AbstractReportExporter;
import com.itpatagonia.obfuscator.web.reports.export.DatabasesReportExporter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author gortiz
 */
@Log4j2
public class DatabasesReportAction extends DownloadExcelAction {

    
    @Override
    public String execute() throws Exception {
        super.setFileName("Databases_Obfuscator.xls");
        super.execute();        
        
        ByteArrayInputStream excel;
        try {
            excel = generateExcel();
            this.setFileInputStream(excel);
        } catch (IOException ex) {
            log.error(getText("global.action.report.empty"));
            log.error(ex.getMessage());
           
            return ERROR;
        }catch(SecurityException ex){
            log.error("No se pudo obtener la lista de archivos, debido a permisos insuficientes en el file system. Por ende no se pudo generar el reporte.");            
            return ERROR;
        }catch (Exception ex){
            log.error(getText("global.action.report.DatabasesReportAction.unexpected.error"));
            log.error(ex.getMessage());
        }
       
        log.info(getText("global.action.report.DatabasesReportAction.info.reportsuccess"));
        return SUCCESS;
    }

    private ByteArrayInputStream generateExcel() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
       createExporter().exportExcel(baos);
        
        return new ByteArrayInputStream(baos.toByteArray());
    }

    private AbstractReportExporter createExporter() {
        DatabasesReportExporter reportExporter = new DatabasesReportExporter();
        try{
            reportExporter.setRows(ListService.getDatabasesReportList());
        }catch(SecurityException ex){
            log.error("No se pudo obtener la lista de archivos, debido a permisos insuficientes. No se pudo generar el reporte debido a ello.");            
        } 
        return reportExporter;
    }

  


}
