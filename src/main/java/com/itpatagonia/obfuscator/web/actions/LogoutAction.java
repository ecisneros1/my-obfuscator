package com.itpatagonia.obfuscator.web.actions;

import com.itpatagonia.obfuscator.process.services.IdentityService;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author cd115986
 */
@Log4j2
public class LogoutAction extends ActionSupport implements SessionAware {
    
    
    private Map<String, Object> session;
    
    @Override
    public String execute() {
        log.info("User logout: " + this.session.get("username"));
        
        try {
            IdentityService identityService = (IdentityService) this.session.get("identityService");
            identityService.finish();

            this.session.clear();
            
        } catch (Exception ex) {
            log.error("Error al intentar cerrar la sessión, Exception : " + ex.getMessage());
            return ERROR;
        }

        return SUCCESS;
    }
    
    @Override
    public void setSession(Map<String, Object> session) {
         this.session = session;
    }
    
}
