package com.itpatagonia.obfuscator.web.reports.export;

import com.itpatagonia.obfuscator.process.model.DatabaseReportItem;
import com.itpatagonia.obfuscator.web.reports.excel.ExcelBuilder;
import com.itpatagonia.obfuscator.web.reports.excel.ExcelDefineColumn;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 *
 * @author cd115986
 */
public class DatabasesReportExporter extends AbstractReportExporter {
    private List<DatabaseReportItem> rows;
    @Override
    public void exportExcel(OutputStream out) throws IOException {
        ExcelBuilder exBuilder = new ExcelBuilder();
        exBuilder.setUseMessagesProperties(false);
        
        ExcelDefineColumn defineCols = createExcelDefineColumnsDatabases();
        exBuilder.createSheet("Bases")
                    .header(defineCols.getHeader())
                    .rows(defineCols.getRows());
        exBuilder.write(out);
    }
    
    private ExcelDefineColumn createExcelDefineColumnsDatabases() {
        
        return new ExcelDefineColumn<DatabaseReportItem>(rows){
            @Override
            protected void define(DatabaseReportItem bd){
//                add("global.report.databases.column.name", bd.getName());
//                add("global.report.databases.column.appowner", bd.getAppOwner());
//                add("global.report.databases.column.vendor", bd.getVendorDataBase());
//                add("global.report.databases.column.version", bd.getVersionDataBase());
//                add("global.report.databases.column.env.name", bd.getEnvironment());
//                add("global.report.databases.column.env.host", bd.getHost());
//                
//                add("global.report.databases.column.env.port", bd.getPort());
//                add("global.report.databases.column.env.servicename", bd.getServicename());
//                add("global.report.databases.column.tables.count", bd.getNumberTables());
                add("Aplicacion", bd.getAppOwner());
                add("Nombre", bd.getName());
                add("Tipo BD", bd.getVendorDataBase());
                add("Version BD", bd.getVersionDataBase());
                add("Id Ambiente", bd.getEnvironment());
                add("Diferente Base Schema", bd.getOverrideSchema());
                add("Host", bd.getHost());
                add("Puerto", bd.getPort());
                add("Servicename", bd.getServicename());
                add("NroTables", bd.getNumberTables());                
                add("NroColumnas", bd.getNumberColumns());                
            }
        };
    }
    public void setRows(List<DatabaseReportItem> rows) {
        this.rows = rows;
    }
}
