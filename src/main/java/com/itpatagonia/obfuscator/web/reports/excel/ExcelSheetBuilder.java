package com.itpatagonia.obfuscator.web.reports.excel;

//import com.itau.mesa.main.utils.Constants;
//import com.itau.mesa.main.utils.MessagesHelper;
import com.opensymphony.xwork2.DefaultTextProvider;
import com.opensymphony.xwork2.TextProvider;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * 
 * @author gortiz
 */
@Log4j2
public class ExcelSheetBuilder {
    
    private List header = Collections.EMPTY_LIST;
    private List<Map> rows = Collections.EMPTY_LIST;
    private final String name;
    private boolean useMessagesProperties = false;

    ExcelSheetBuilder(String name) {
        this.name = name;
    }
    
    protected void setValueDataCell(Cell datacell, Object obj) {
        if (obj == null) {
            datacell.setCellValue("");
        } else if (obj instanceof Date) {
//            SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATETIME_PATTERN);
            SimpleDateFormat sdf = new SimpleDateFormat();
            Date date = (Date)obj;
            datacell.setCellValue(sdf.format(date));
        } else if (obj instanceof Double) {
            Double aDouble = (Double)obj;
            datacell.setCellValue(aDouble);
        } else if (obj instanceof BigDecimal) {
            BigDecimal aBigDecimal = ((BigDecimal)obj).setScale(4, RoundingMode.HALF_UP);            
            datacell.setCellValue(aBigDecimal.doubleValue());
        } else if (obj instanceof Boolean) {
            datacell.setCellValue(BooleanUtils.toString((Boolean)obj, "SI", "NO"));
        } else {            
            datacell.setCellValue(obj.toString());
        }             
    }  
   
    void populateSheet(Sheet sh, Workbook workbook) {
        
        Row headerRow = sh.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
//        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setBold(true);
        headerStyle.setFont(font);
        for (int cellIndex = 0; cellIndex < header.size(); cellIndex++) {
            Cell cell = headerRow.createCell(cellIndex);
            cell.setCellValue(getHeaderName(header.get(cellIndex)));    
            cell.setCellStyle(headerStyle);
        }
        
        for (Map row : rows) {
            Row r = sh.createRow(rows.indexOf(row) + 1);
            for (int cellIndex = 0; cellIndex < header.size(); cellIndex++) {
                Cell datacell = r.createCell(cellIndex);
                Object value = row.get(header.get(cellIndex));
                setValueDataCell(datacell, value);
            }
        }   
        //Por ultimo se ajusta el tamanyo de las columnas al contenido
        for (int i = 0; i < header.size(); i++) {
            sh.autoSizeColumn(i);
        }
    }

    public ExcelSheetBuilder header(List header) {
        this.header = header;
        return this;
    }

    public ExcelSheetBuilder rows(List<Map> rows) {
        this.rows = rows;
        return this;
    }

    public String getName() {
        DefaultTextProvider textProvider = new DefaultTextProvider();
        return useMessagesProperties ? textProvider.getText(name) : name;
    }

    public void setUseMessagesProperties(boolean useMessagesProperties) {
        this.useMessagesProperties = useMessagesProperties;
    }

    private String getHeaderName(Object value) {
        DefaultTextProvider textProvider = new DefaultTextProvider();

        if (useMessagesProperties) {
            try{
                value = (value == null) ? "" : textProvider.getText(value.toString());
            }catch(Exception ex){
                log.error("No se pudo obtener el texto del properties.");
            }
        }
        return ObjectUtils.toString(value);
    }

}
