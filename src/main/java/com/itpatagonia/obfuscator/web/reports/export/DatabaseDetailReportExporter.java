package com.itpatagonia.obfuscator.web.reports.export;

import com.itpatagonia.obfuscator.process.model.DatabaseDetailReportItem;
import com.itpatagonia.obfuscator.process.model.DatabaseReportItem;
import com.itpatagonia.obfuscator.web.reports.excel.ExcelBuilder;
import com.itpatagonia.obfuscator.web.reports.excel.ExcelDefineColumn;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 *
 * @author AR
 */
public class DatabaseDetailReportExporter extends AbstractReportExporter {
    private List<DatabaseDetailReportItem> rows;
    private List<DatabaseReportItem> rowsEnv;
    @Override
    public void exportExcel(OutputStream out) throws IOException {
        ExcelBuilder exBuilder = new ExcelBuilder();
        exBuilder.setUseMessagesProperties(false);
        
        ExcelDefineColumn  defineCols = createExcelDefineColumnsEnv();
        exBuilder.createSheet("Ambientes")
                    .header(defineCols.getHeader())
                    .rows(defineCols.getRows());
        
        defineCols = createExcelDefineColumnsDatabases();       
        exBuilder.createSheet("Tablas")
                    .header(defineCols.getHeader())
                    .rows(defineCols.getRows());
        
        
        exBuilder.write(out);
    }
    
    private ExcelDefineColumn createExcelDefineColumnsEnv() {        
        return new ExcelDefineColumn<DatabaseReportItem>(rowsEnv){
            @Override
            protected void define(DatabaseReportItem bd){
                add("Aplicacion", bd.getAppOwner());
                add("Nombre", bd.getName());
                add("Tipo BD", bd.getVendorDataBase());
                add("Version BD", bd.getVersionDataBase());
                add("Id Ambiente", bd.getEnvironment());
                add("Diferente Base Schema", bd.getOverrideSchema());
                add("Host", bd.getHost());
                add("Puerto", bd.getPort());
                add("Servicename", bd.getServicename());
                add("NroTables", bd.getNumberTables());                
            }
        };
    }
    private ExcelDefineColumn createExcelDefineColumnsDatabases() {
        
        return new ExcelDefineColumn<DatabaseDetailReportItem>(rows){
            @Override
            protected void define(DatabaseDetailReportItem bd){           

                add("Nombre", bd.getName());
                add("Aplicacion", bd.getAppOwner());
                add("Vendor", bd.getVendorDataBase());
                add("Version", bd.getVersionDataBase());
                add("Tabla", bd.getTableName());
                add("Columna", bd.getColumnName());
                add("Metodo", bd.getMethod());
            }
        };
    }
    public void setRows(List<DatabaseDetailReportItem> rows) {
        this.rows = rows;
    }
    public void setRowsEnv(List<DatabaseReportItem> rowsEnv) {
        this.rowsEnv = rowsEnv;
    }
}

