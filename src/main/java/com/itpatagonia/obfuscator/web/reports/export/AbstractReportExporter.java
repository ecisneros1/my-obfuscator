package com.itpatagonia.obfuscator.web.reports.export;

//import com.itau.mesa.main.utils.MessagesHelper;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author GO431220
 */
public abstract class AbstractReportExporter {


    /**
     * 
     * @param out
     * @throws IOException 
     */
    public abstract void exportExcel(OutputStream out) throws IOException;

    
    protected String msg(String key) {
        try {
            return key;
//            return MessagesHelper.get(key);
        } catch (Exception e) {
            return key;
        }
    }
    
}
