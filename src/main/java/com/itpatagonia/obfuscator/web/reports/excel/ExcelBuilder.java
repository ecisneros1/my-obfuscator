package com.itpatagonia.obfuscator.web.reports.excel;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
//import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * 
 * @author gortiz
 */
public class ExcelBuilder {

    private boolean useMessagesProperties = false;

    ArrayList<ExcelSheetBuilder> sheetBuilders = new ArrayList<>();
        
    public ExcelSheetBuilder createSheet(String name) {
        ExcelSheetBuilder excelSheetBuilder = new ExcelSheetBuilder(name);
        excelSheetBuilder.setUseMessagesProperties(useMessagesProperties);
        sheetBuilders.add(excelSheetBuilder);
        return sheetBuilders.get(sheetBuilders.size() - 1);
    }
    
    public void write(OutputStream out) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        for (ExcelSheetBuilder sb : sheetBuilders) {
            Sheet sh = workbook.createSheet(sb.getName());
            sb.populateSheet(sh, workbook);
        }    
        workbook.write(out);
    }

    public void setUseMessagesProperties(boolean useMessagesProperties) {
        this.useMessagesProperties = useMessagesProperties;
    }
    
}
