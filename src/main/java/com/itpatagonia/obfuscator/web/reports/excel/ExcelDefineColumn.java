package com.itpatagonia.obfuscator.web.reports.excel;

import java.util.*;

/**
 * 
 * @author gortiz
 * @param <T> 
 */
public abstract class ExcelDefineColumn<T> {

    protected abstract void define(T obj);
    
    Collection<T> listContent;
    
    List header = new ArrayList();
    List<Map> rows = new ArrayList<>();
    Map row = null;
    
    private void addNoDuplicate(String columnName) {
        if(!header.contains(columnName))
            header.add(columnName);
    }
    
    protected final void add(String columnName, Object value) {
        addNoDuplicate(columnName);
        row.put(columnName, value);
    }
    
    private void createRow(T obj) {
        this.row = new HashMap();
        define(obj);
        rows.add(row);
    }
    
    public List getHeader() {
        return header;
    }

    public List<Map> getRows() {
        return rows;
    }
    
    public ExcelDefineColumn(Collection<T> list) {
        for (T obj : list) {
            createRow(obj);
        }
    }
    
}
