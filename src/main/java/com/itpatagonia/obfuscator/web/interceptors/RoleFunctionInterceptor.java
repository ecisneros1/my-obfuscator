package com.itpatagonia.obfuscator.web.interceptors;

import com.itpatagonia.obfuscator.process.exceptions.NoLoggedUserException;
import com.itpatagonia.obfuscator.process.services.IdentityService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import java.io.IOException;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class RoleFunctionInterceptor extends AbstractInterceptor {

    private List<String> allowedRoleFunctions = new ArrayList<String>();
  
    public void setAllowedRoles(String roles) {
        this.allowedRoleFunctions = stringToList(roles);
    }
   

    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();
        String result = null;
        if (!isAllowed(request, invocation.getAction())) {
            result = handleRejection(invocation, response);
        } else {
            result = invocation.invoke();
        }
        return result;
    }

    /**
     * Splits a string into a List
     * @return List of allowed functions.
     */
    protected List<String> stringToList(String val) {
        if (val != null) {
            String[] list = val.split("[ ]*,[ ]*");
            return Arrays.asList(list);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * Determines if the request should be allowed for the action
     *
     * @param request The request
     * @param action The action object
     * @return True if allowed, false otherwise
     */
    protected boolean isAllowed(HttpServletRequest request, Object action) {
        HttpSession session = request.getSession(false);
        boolean result = false;

        IdentityService identityService = null;
        if (session != null) {
            identityService = (IdentityService) session.getAttribute("identityService");
        }

        if (allowedRoleFunctions.size() > 0) {
            if (session == null || identityService == null) {
                return result;
            }
            for (String role : allowedRoleFunctions) {
                try {
                    result = identityService.hasPermission(role);
                } catch (NoLoggedUserException ex) {
                    log.error("Usuario no logueado intento ejecutar action: " + action);
                } catch (Exception ex) {
                    log.error("Error inesperado al validar permisos para ejecutar action: " + action, ex);
                }
            }
            if (!result) {
                log.error("Usuarios sin permiso para ejecutar el action: " + action);
            }

            return result;
        }
        return true;
    }

    /**
     * Handles a rejection by sending a 403 HTTP error
     *
     * @param invocation The invocation
     * @param response
     * @return The result code
     */
    protected String handleRejection(ActionInvocation invocation, HttpServletResponse response) {
        try {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return "error-function";
        } catch (IOException ex) {
            log.error("No se pudo enviar el aviso de falta de permisos al usuario por un error: ", ex);
            return "error-function";
        }
    }

}
