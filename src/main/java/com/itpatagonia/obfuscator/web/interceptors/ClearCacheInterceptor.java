package com.itpatagonia.obfuscator.web.interceptors;

import org.apache.struts2.StrutsStatics;
 
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import javax.servlet.http.HttpServletResponse;
 
/**
 *
 * @author CD115986
 */
public class ClearCacheInterceptor extends AbstractInterceptor {
    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        ActionContext context=(ActionContext)invocation.getInvocationContext();
         HttpServletResponse response=(HttpServletResponse)context.get(StrutsStatics.HTTP_RESPONSE);
        
        //Instead of the meta tags, this approach it's better because the server's headers
        //have priority over the metatags. So if the server define a cache policy the
        //browser will pay attention to it instead of the meta tags. 
        //In Struts we use interceptors. This will be referenced in struts.xml for every action.
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        String result=invocation.invoke();
        return result;
    }
}
