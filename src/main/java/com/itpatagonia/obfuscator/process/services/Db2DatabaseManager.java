package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.exceptions.DatabaseSQLException;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Gortiz
 */
@Log4j2
public class Db2DatabaseManager extends DatabaseManager {

    /**
     *
     * @param databaseName
     * @param host
     * @param port
     * @param serviceName
     * @param database
     */
    protected Db2DatabaseManager(String databaseName, String host, String port, String serviceName, DatabasesEnum database) {
        super(databaseName, host, port, serviceName, database);
    }

    /**
     *
     * @return
     */
    @Override
    public String buildUrlDatabaseConnection() {
        StringBuilder urlConnection = new StringBuilder(150);
        //jdbc:db2://myhost:5021/mydb
        urlConnection.append("jdbc:db2://");
        urlConnection.append(this.host);
        urlConnection.append(":");
        urlConnection.append(this.port);
        urlConnection.append("/");
        urlConnection.append(this.databaseName);
        return urlConnection.toString();
    }

    @Override
    public List<String> getTablesNames(Connection conn) {
        String[] tableTypes = {"TABLE"};
        String catalog = this.databaseName;
        //TODO try with: getColumnsNames(conn, catalog, "dbo", null, tableTypes);
        return getTablesNames(conn, null, "DBO", null, tableTypes);
    }

    @Override
    public List<String> getColumnsNames(Connection conn, String tablename) {
        String catalog = this.databaseName;
        //TODO try with: getColumnsNames(conn, catalog, "dbo", tablename, null);
        return getColumnsNames(conn, null, "DBO", tablename, null);
    }

    @Override
    public List<String> getPrimaryKeys(Connection conn, String tablename) {
        String catalog = this.databaseName;
        return getPrimaryKeys(conn, null, "DBO", tablename);
    }

    @Override
    public List<String> getImportedKeys(Connection conn, String tablename) {
        String catalog = this.databaseName;
        return getImportedKeys(conn, null, "DBO", tablename);
    }

    @Override
    public String getReferenceNameForDatabaseObject(String tableName) {
        return databaseName + ".dbo." + tableName;
    }

    @Override
    public List<String> getRangeIdForBatch(Connection conn, String tableName, int numberOfRowsPerBatch) {
       try {
            
            Statement stament = conn.createStatement();
            List<String> results = new ArrayList<>();            
                       
            String fieldUnique = getUniqueFieldForBatch(conn, tableName, numberOfRowsPerBatch);
            int numberOfCuts = estimateNumberOfCuts(numberOfRowsPerBatch, conn, tableName);            
           
            String tableNameReference = this.getReferenceNameForDatabaseObject(tableName);
            String initialValue = "";            
            String maxValue = "";            
                        
            //Get the initial value
            String sqlInitialValue = getBottomValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, initialValue);
            ResultSet resultset = stament.executeQuery(sqlInitialValue);
            if(resultset.next()) {
                initialValue = resultset.getString("initialValue"); // This alias is setted in the query created by getBottomValueQuery
                results.add("'"+ initialValue + "'");
                stament.close();  
            } 
                   
            for(int i= 0; i < numberOfCuts; i++){
                log.info("iteracion: "+i);
                if(i % 2 == 0){ // B
                    Statement topStatement    = conn.createStatement();
                    String sqlMaxValue = getTopValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, initialValue);
                    log.info(sqlMaxValue);
                    resultset = topStatement.executeQuery(sqlMaxValue);                    
                    if(resultset.next()){
                        maxValue = resultset.getString("maxValue");
                        results.add("'"+ maxValue + "'");
                        topStatement.close();
                    }
                   
                } else { //A
                    Statement bottomStatement = conn.createStatement();
                    sqlInitialValue = getBottomValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, maxValue);
                    resultset = bottomStatement.executeQuery(sqlInitialValue);                   
                    if(resultset.next()){
                        initialValue = resultset.getString("initialValue");
                        results.add("'"+ initialValue + "'");
                        bottomStatement.close();
                    }
                                                           
                }
            }    
            
            return results;
            
        } catch (SQLException ex) {
            log.error("[DatabaseManager.sendQueryTODB] Error al ejecutar el query de ofuscamiento.");            
            log.error("Error Message"+ ex.getMessage());
            throw new DatabaseSQLException();
        }  
    }

    @Override
    public String getBottomValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String initialValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ")
          .append(uniqueField)
          .append(" as initialValue FROM ")
          .append(tableName);

        if (StringUtils.isNotBlank(initialValue)) {
            sb.append(" WHERE ")
              .append(uniqueField)
              .append(" > '")
              .append(initialValue)
              .append("'");
        }

        sb.append(" ORDER BY ")
          .append(uniqueField)
          .append(" FETCH FIRST 1 ROWS ONLY");

        return sb.toString();
    }

    @Override
    public String getTopValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String bottomValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT MAX(T.")
          .append(uniqueField)
          .append(") as maxValue FROM (SELECT ab.")
          .append(uniqueField)
          .append(" FROM ")
          .append(tableName)
          .append(" AS ab WHERE ab.")
          .append(uniqueField)
          .append(" > '")
          .append(bottomValue)
          .append("' ORDER BY ab.")
          .append(uniqueField)
          .append(" FETCH FIRST ")
          .append(numberOfRowsPerBatch)
          .append(" ROWS ONLY) AS T ");
        return sb.toString();
    }
    
    @Override
    public List<String> getUniqueReferenceIdPerRow(Connection conn, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
    
}
