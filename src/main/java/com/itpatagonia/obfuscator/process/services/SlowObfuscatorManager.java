package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.OfuscationMethod;
import com.itpatagonia.obfuscator.configuration.xml.model.Param;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import com.itpatagonia.obfuscator.process.exceptions.InvalidColumnDefinitionException;
import com.itpatagonia.obfuscator.process.exceptions.InvalidTableDefinitionException;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.utils.ObfuscatorUtils;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public abstract class SlowObfuscatorManager extends ObfuscatorManager implements Ofuscable {

    static private HashMap methods;

    public boolean isXmlValid = false;

    protected List<Column> fixedValuesColumns = null;
    protected List<Column> switchingVowelsColumns = null;
    protected List<Column> copingValuesColumns = null;
    //Batch based updates.
    protected List<Column> fixedValuesColumnsBatch = null;
    protected List<Column> switchingVowelsColumnsBatch = null;
    protected List<Column> copingValuesColumnsBatch = null;

    /**
     * Validate the XML. Obfuscating in slow mode let the user to try to
     * obfuscate pk and fk columns.
     *
     * @throws ObfuscatorException
     * @throws SQLException
     */
    public void validateXML() throws ObfuscatorException, SQLException {
        log.info("... validando la DBD - BD: " + databaseXML.getName());

        log.info("...... comprobando que existen las tablas");
        //Checking if the tables of xml are defined in the database.
        Iterator<Map.Entry<Table, Boolean>> iteratorTable = dbm.checkTablesNames(conn, databaseXML.getTables()).entrySet().iterator();
        while (iteratorTable.hasNext()) {
            Map.Entry<Table, Boolean> entry = iteratorTable.next();
            log.debug("Checking Table:" + entry.getKey() + ": " + entry.getValue());

            if (!entry.getValue()) {
                log.error("[" + this.getClass() + "][error] Error una de las tablas definida no existe en la BD: " + entry.getKey());
                log.info("Compruebe en el xml los nombres de las tablas. ¡Recuerde que los nombres de las tablas son Case Sensitive! ");
                throw new InvalidTableDefinitionException(entry.getKey().toString());
            }
        }

        log.info("...... comprobando que existen las columnas");
        for (Table table : databaseXML.getTables()) {
            isXmlValid = false;
            Iterator<Map.Entry<Column, Boolean>> iteratorColumns = dbm.checkColumnsNames(conn, table).entrySet().iterator();

            while (iteratorColumns.hasNext()) {
                Map.Entry<Column, Boolean> entry = iteratorColumns.next();
                log.debug("Checking Columns:" + entry.getKey() + ": " + entry.getValue());

                if (!entry.getValue()) {
                    log.error("[" + this.getClass() + "][error] Error columna definida en el XML no existe en la BD: " + entry.getKey());
                    log.info("Compruebe en el xml los nombres de las columnas. ¡Recuerde que los nombres de las columnas son Case Sensitive! ");
                    throw new InvalidColumnDefinitionException(entry.getKey().toString());
                }
            }
        }
        isXmlValid = true;
        log.info("...... validacion finalizada exitosamente.");
    }

    @Override
    public void obfuscate() {
        log.info("... iniciando ofuscacion LENTA - BD: " + databaseXML.getName());
        if (isXmlValid) {
            // Normal update lists
            fixedValuesColumns = null;
            switchingVowelsColumns = null;
            copingValuesColumns = null;
            // Batch based updates.
            fixedValuesColumnsBatch = null;
            switchingVowelsColumnsBatch = null;
            copingValuesColumnsBatch = null;

            StringBuilder sqlToObsfuscate = new StringBuilder(databaseXML.getTables().size() * 500);
            List<String> sqlToObsfuscateBatch = new ArrayList<>();
            List<String> tablesProcessList = new ArrayList<>();
            Queue<String> tablesProcessListBatch = new LinkedList<>();

            for (Table table : databaseXML.getTables()) {
                log.info("Table: " + table.getName() + " - BlockSize: " + table.getBlockSize());
                String blockSize = table.getBlockSize();
                //Check if it is set a blocksize, if it is, then process it as a Batch Process. (Exceptional case)
                if (blockSize != null && Integer.parseInt(blockSize) > 0) {
                    populateColumnListByBatchMethod(table);
                } else {
                    populateColumnListByMethod(table); //Common case. 
                }

                if (fixedValuesColumns != null && fixedValuesColumns.size() > 0) {
                    sqlToObsfuscate.append(obfuscateByFixedValue(table, fixedValuesColumns));
                    for (Column colunm : fixedValuesColumns) {
                        tablesProcessList.add("Table: " + table.getName() + " - Method: " + OfuscationMethod.Methods.VALOR_FIJO.name() + " - Column: " + colunm.getName());
                    }
                    fixedValuesColumns.clear();
                }

                if (switchingVowelsColumns != null && switchingVowelsColumns.size() > 0) {
                    sqlToObsfuscate.append(this.obfuscateBySwitchingCharacters(table, switchingVowelsColumns));
                    for (Column colunm : switchingVowelsColumns) {
                        tablesProcessList.add("Table: " + table.getName() + " - Method: " + OfuscationMethod.Methods.INTERCAMBIO_LETRAS.name() + " - Column: " + colunm.getName());
                    }
                    switchingVowelsColumns.clear();
                }

                if (copingValuesColumns != null && copingValuesColumns.size() > 0) {
                    sqlToObsfuscate.append(obfuscateByCopingValues(table, copingValuesColumns));
                    for (Column colunm : copingValuesColumns) {
                        tablesProcessList.add("Table: " + table.getName() + " - Method: " + OfuscationMethod.Methods.COPIAR_COLUMNA.name() + " - Column: " + colunm.getName());
                    }
                    copingValuesColumns.clear();
                }

                if (fixedValuesColumnsBatch != null && fixedValuesColumnsBatch.size() > 0) {
                    sqlToObsfuscateBatch.add(obfuscateByFixedValueBatch(table, fixedValuesColumnsBatch));
                    tablesProcessListBatch.add("Table: " + table.getName() + " - Batch method: " + OfuscationMethod.Methods.VALOR_FIJO.name());
                    fixedValuesColumnsBatch.clear();
                }
                if (switchingVowelsColumnsBatch != null && switchingVowelsColumnsBatch.size() > 0) {
                    sqlToObsfuscateBatch.add(obfuscateBySwitchingCharactersBatch(table, switchingVowelsColumnsBatch));
                    tablesProcessListBatch.add("Table: " + table.getName() + " - Batch method: " + OfuscationMethod.Methods.INTERCAMBIO_LETRAS.name());
                    switchingVowelsColumnsBatch.clear();
                }
                if (copingValuesColumnsBatch != null && copingValuesColumnsBatch.size() > 0) {
                    sqlToObsfuscateBatch.add(obfuscateByCopingValuesBatch(table, copingValuesColumnsBatch));
                    tablesProcessListBatch.add("Table: " + table.getName() + " - Batch method: " + OfuscationMethod.Methods.COPIAR_COLUMNA.name());
                    copingValuesColumnsBatch.clear();
                }
            }
            log.info("....... ejecutando ofuscamiento de datos");
            if (StringUtils.isNotBlank(sqlToObsfuscate.toString())) {
                log.debug("....... " + sqlToObsfuscate.toString());
                int[] results = dbm.sendSimplesQueryTODB(conn, sqlToObsfuscate.toString());
                for (int i = 0; i < results.length; i++) {
                    log.info("....... " + tablesProcessList.get(i) + " - registros actualizados: " + results[i]);
                }
            } else {
                log.info("....... No hay tablas a ofuscar con el proceso único. Continuará con el proceso en lotes.");
            }

            log.info("....... ejecutando ofuscamiento de datos en lotes");
            if (CollectionUtils.isNotEmpty(sqlToObsfuscateBatch)) {
                sqlToObsfuscateBatch.stream().forEach((s) -> {
                    int[] resultsBatch = dbm.sendSimplesQueryTODB(conn, s);
                    int totalUpdatesForTable = sumAll(resultsBatch);
                    log.info("....... " + tablesProcessListBatch.poll() + " - registros actualizados: " + totalUpdatesForTable);
                });
            } else {
                log.info("....... No hay tablas a ofuscar con el proceso en lotes.");
            }

        }
        log.info("....... ofuscación finalizada exitosamente.");
    }

    @Override
    public String obfuscateByFixedValue(Table table, List<Column> columns) {

        StringBuilder sqlToObfuscate = new StringBuilder(columns.size() * 100);
        Param paramFixedValue;
        sqlToObfuscate.append("UPDATE ");
        sqlToObfuscate.append(this.dbm.getReferenceNameForDatabaseObject(table.getName()));
        sqlToObfuscate.append(" SET ");
        Iterator iColumn = columns.iterator();
        Column column;

        while (iColumn.hasNext()) {
            column = (Column) iColumn.next();
            sqlToObfuscate.append(this.dbm.getReferenceNameForColumn(column.getName()));
            sqlToObfuscate.append(" = ");
            sqlToObfuscate.append("'");
            paramFixedValue = (Param) column.getOfuscationMethod().getParams().get(0);
            sqlToObfuscate.append(paramFixedValue.getValue());
            sqlToObfuscate.append("'");
            if (iColumn.hasNext()) {
                sqlToObfuscate.append(",");
            }
        }

        sqlToObfuscate.append(";");

        return sqlToObfuscate.toString();
    }

    @Override
    public String obfuscateByCopingValues(Table table, List<Column> columns) {

        StringBuilder sqlToObfuscate = new StringBuilder(columns.size() * 100);
        Param paramCopyFromValue; // nombre de la columna a copiar
        sqlToObfuscate.append("UPDATE ");
        sqlToObfuscate.append(this.dbm.getReferenceNameForDatabaseObject(table.getName()));
        sqlToObfuscate.append(" SET ");
        Iterator iColumn = columns.iterator();
        Column column;

        while (iColumn.hasNext()) {
            column = (Column) iColumn.next();
            sqlToObfuscate.append(this.dbm.getReferenceNameForColumn(column.getName()));
            sqlToObfuscate.append(" = ");
            paramCopyFromValue = (Param) column.getOfuscationMethod().getParams().get(0);
            String nameColumnCopied = paramCopyFromValue.getValue();
            sqlToObfuscate.append(this.dbm.getReferenceNameForColumn(nameColumnCopied));
            if (iColumn.hasNext()) {
                sqlToObfuscate.append(",");
            }
        }

        sqlToObfuscate.append(";");

        return sqlToObfuscate.toString();
    }

    @Override
    public String obfuscateBySwitchingCharacters(Table table, List<Column> columns) {
        StringBuilder sqlToObfuscate = new StringBuilder();

        sqlToObfuscate.append("UPDATE ");
        sqlToObfuscate.append(this.dbm.getReferenceNameForDatabaseObject(table.getName()));
        sqlToObfuscate.append(" SET ");
        Iterator iColumn = columns.iterator();
        Column column;

        while (iColumn.hasNext()) {
            column = (Column) iColumn.next();
            String nameColumn = this.dbm.getReferenceNameForColumn(column.getName());
            sqlToObfuscate.append(nameColumn);
            sqlToObfuscate.append(" = ");
            sqlToObfuscate.append(
                    ObfuscatorUtils.generateRecursiveReplace(
                            ObfuscatorUtils.generateRecursiveReplace(nameColumn, this.lowerCase),
                            this.upperCase
                    )
            );
            if (iColumn.hasNext()) {
                sqlToObfuscate.append(",");
            }
        }
        sqlToObfuscate.append(";");

        return sqlToObfuscate.toString();
    }

    @Override
    public String obfuscateInBatch(Table table, String sqlFromMethod) {
        int blockSize = Integer.parseInt(table.getBlockSize());
        List<String> rowID = this.dbm.getRangeIdForBatch(conn, table.getName(), blockSize);
        String uniqueField = this.dbm.getUniqueFieldForBatch(conn, table.getName(), blockSize);
        StringBuilder result = new StringBuilder(1000);

        for (int i = 0; i < rowID.size(); i++) {
            // Create StringBuilder with enough space for the query.
            StringBuilder sqlToObfuscate = new StringBuilder(sqlFromMethod.length() + 250);

            sqlToObfuscate.append(sqlFromMethod);
            sqlToObfuscate.append(" WHERE ")
                    .append(uniqueField)
                    .append(" BETWEEN ")
                    .append(rowID.get(i++))
                    .append(" AND ")
                    .append(rowID.get(i))
                    .append(";");
            result.append(sqlToObfuscate.toString());
        }
        return result.toString();

    }

    @Override
    public String obfuscateByCopingValuesBatch(Table table, List<Column> columns) {
        String query = obfuscateByCopingValues(table, columns);
        query = query.substring(0, query.length() - 1);
        return obfuscateInBatch(table, query);
    }

    @Override
    public String obfuscateBySwitchingCharactersBatch(Table table, List<Column> columns) {
        String query = obfuscateBySwitchingCharacters(table, columns);
        query = query.substring(0, query.length() - 1);
        return obfuscateInBatch(table, query);
    }

    @Override
    public String obfuscateByFixedValueBatch(Table table, List<Column> columns) {
        String query = obfuscateByFixedValue(table, columns);
        query = query.substring(0, query.length() - 1);
        return obfuscateInBatch(table, query);
    }

    @Override
    protected void populateColumnListByMethod(Table table) {
        for (Column column : table.getColumns()) {
            switch (column.getOfuscationMethod().getMethod()) {
                case VALOR_FIJO:
                    if (fixedValuesColumns == null) {
                        fixedValuesColumns = new ArrayList();
                    }
                    fixedValuesColumns.add(column);
                    break;
                case INTERCAMBIO_LETRAS:
                    if (switchingVowelsColumns == null) {
                        switchingVowelsColumns = new ArrayList();
                    }
                    switchingVowelsColumns.add(column);
                    break;
                case COPIAR_COLUMNA:
                    if (copingValuesColumns == null) {
                        copingValuesColumns = new ArrayList();
                    }
                    copingValuesColumns.add(column);
                    break;
            }
        }
    }

    @Override
    protected void populateColumnListByBatchMethod(Table table) {
        for (Column column : table.getColumns()) {
            switch (column.getOfuscationMethod().getMethod()) {
                case VALOR_FIJO:
                    if (fixedValuesColumnsBatch == null) {
                        fixedValuesColumnsBatch = new ArrayList();
                    }
                    fixedValuesColumnsBatch.add(column);
                    break;
                case INTERCAMBIO_LETRAS:
                    if (switchingVowelsColumnsBatch == null) {
                        switchingVowelsColumnsBatch = new ArrayList();
                    }
                    switchingVowelsColumnsBatch.add(column);
                    break;
                case COPIAR_COLUMNA:
                    if (copingValuesColumnsBatch == null) {
                        copingValuesColumnsBatch = new ArrayList();
                    }
                    copingValuesColumnsBatch.add(column);
                    break;
            }
        }
    }

    public static Map<String, String> getMethods() {
        //if(methods == null){

        methods = new HashMap();
        for (OfuscationMethod.Methods method : OfuscationMethod.Methods.values()) {

            methods.put(method.name(), method.getDisplayName());
        }
        //}            

        return methods;
    }

    protected final char[] upperCase = {
        'E', '.',
        'A', 'E',
        '.', 'A',
        'V', '.',
        'B', 'V',
        '.', 'B',
        'D', '.',
        'C', 'D',
        '.', 'C',
        'H', '.',
        'F', 'H',
        '.', 'F',
        'J', '.',
        'G', 'J',
        '.', 'G',
        'Y', '.',
        'I', 'Y',
        '.', 'I',
        'Q', '.',
        'K', 'Q',
        '.', 'K',
        'R', '.',
        'L', 'R',
        '.', 'L',
        'N', '.',
        'M', 'N',
        '.', 'M',
        'U', '.',
        'O', 'U',
        '.', 'O',
        'T', '.',
        'P', 'T',
        '.', 'P',
        'Z', '.',
        'S', 'Z',
        '.', 'S',
        'X', '.',
        'W', 'X',
        '.', 'W',
        '0', '3',
        '1', '6',
        '2', '9',
        '3', '7',
        '4', '8',
        '5', '1',
        '6', '0',
        '7', '5',
        '8', '2',
        '9', '4'
    };
    protected final char[] lowerCase = {
        'e', '.',
        'a', 'e',
        '.', 'a',
        'v', '.',
        'b', 'v',
        '.', 'b',
        'd', '.',
        'c', 'd',
        '.', 'c',
        'h', '.',
        'f', 'h',
        '.', 'f',
        'j', '.',
        'g', 'j',
        '.', 'g',
        'y', '.',
        'i', 'y',
        '.', 'i',
        'q', '.',
        'k', 'q',
        '.', 'k',
        'r', '.',
        'l', 'r',
        '.', 'l',
        'n', '.',
        'm', 'n',
        '.', 'm',
        'u', '.',
        'o', 'u',
        '.', 'o',
        't', '.',
        'p', 't',
        '.', 'p',
        'z', '.',
        's', 'z',
        '.', 's',
        'x', '.',
        'w', 'x',
        '.', 'w'
    };

}
