package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.exceptions.DatabaseSQLException;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Gortiz
 */
@Log4j2
public class OracleDatabaseManager extends DatabaseManager {

    /**
     *
     * @param databaseName
     * @param host
     * @param port
     * @param serviceName
     * @param database
     */
    protected OracleDatabaseManager(String databaseName, String host, String port, String serviceName, DatabasesEnum database) {
        super(databaseName, host, port, serviceName, database);
    }

    /**
     *
     * @return
     */
    @Override
    public String buildUrlDatabaseConnection() {
        StringBuilder urlConnection = new StringBuilder(150);
        //urlConnection.append(":thin:@//");
        urlConnection.append("jdbc:oracle:thin:@(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=");
        urlConnection.append(this.host);
        //urlConnection.append(":");
        urlConnection.append(")(PORT=");
        urlConnection.append(this.port);
        urlConnection.append(")))(CONNECT_DATA=(UR=A)(SERVICE_NAME=");
        urlConnection.append(this.serviceName);
        urlConnection.append(")(SERVER=DEDICATED)))");
        //urlConnection.append("/");
        return urlConnection.toString();
    }

    /**
     *
     * @param conn
     * @return
     */
    @Override
    public List<String> getTablesNames(Connection conn) {
        String[] tableTypes = {"TABLE"};
        String schema = this.databaseName;
        return getTablesNames(conn, null, schema, null, tableTypes);
    }

    /**
     *
     * @param conn
     * @param tablename
     * @return
     */
    @Override
    public List<String> getColumnsNames(Connection conn, String tablename) {
        String schema = this.databaseName;
        return getColumnsNames(conn, null, schema, tablename, null);
    }

    /**
     *
     * @param conn
     * @param tablename
     * @return
     */
    @Override
    public List<String> getPrimaryKeys(Connection conn, String tablename) {
        String schema = this.databaseName;
        return getPrimaryKeys(conn, null, schema, tablename);
    }

    /**
     *
     * @param conn
     * @param tablename
     * @return
     */
    @Override
    public List<String> getImportedKeys(Connection conn, String tablename) {
        String schema = this.databaseName;
        return getImportedKeys(conn, null, schema, tablename);
    }

    /**
     *
     * @param tableName
     * @return The reference name for a table object, 
     *         i.e: "myBD.mySchema.tableName"
     * 
     */
    @Override
    public String getReferenceNameForDatabaseObject(String tableName) {
        String schema = this.databaseName;
        String identifierQuote = StringUtils.defaultIfBlank(this.identifierQuoteString, ""); // IdentifierQuoteString: "
        return schema + "." + identifierQuote + tableName + identifierQuote;
//        return schema + ".\"" + tableName + "\"";
//        return schema + "." + tableName;
    }

    /**
     * 
     * @param columnName
     * @return 
     */
    @Override
    public String getReferenceNameForColumn(String columnName) {
        String identifierQuote = StringUtils.defaultIfBlank(this.identifierQuoteString, ""); // IdentifierQuoteString: "
        return identifierQuote + columnName + identifierQuote;
//        return "\"" + columnName + "\"";
    }

    /**
     *
     * @param tableName
     * @return The reference name for a table object, i.e:
     * "myBD.mySchema.tableName"
     */
    @Override
    public List<String> getUniqueReferenceIdPerRow(Connection conn, String tableName) {
        try {
            Statement stament = conn.createStatement();
            ResultSet resultset = stament.executeQuery("SELECT ROWIDTOCHAR(ROWID) as idRow FROM " + this.getReferenceNameForDatabaseObject(tableName));
            List<String> results = new ArrayList<>();
            while (resultset.next()) {
                results.add("'" + resultset.getString("idRow") + "'");
            }

            return results;

        } catch (SQLException ex) {
            log.error("[DatabaseManager.sendQueryTODB] Error al ejecutar el query de ofuscamiento.");
            log.error("Error Message" + ex.getMessage());
            throw new DatabaseSQLException();
        }
    }

    /**
     * Get the total number of rows in the table passed.
     *
     * @param conn
     * @param tableName
     * @return
     */
    @Override
    public int getNumberOfRows(Connection conn, String tableName) {
        int result = 0;
        try {
            Statement stament = conn.createStatement();
            ResultSet resultset = stament.executeQuery("SELECT COUNT(*) totalRows FROM " + this.getReferenceNameForDatabaseObject(tableName));
            if (resultset.next()) {
                result = resultset.getInt("totalRows");
            }
        } catch (SQLException ex) {
            log.error("Error al intentar obtener el numero de rows en la tabla: " + tableName, ex);
        }
        return result;
    }

    /**
     * Gets the total number of rows in the table passed that have a different
     * value in the field specified.
     *
     * @param conn
     * @param tableName
     * @return
     */
    @Override
    public int getNumberOfRowsDistinctByField(Connection conn, String tableName, String uniqueField) {
        int result = 0;
        try {
            Statement stament = conn.createStatement();
            ResultSet resultset = stament.executeQuery("SELECT COUNT(DISTINCT " + uniqueField + ") totalRows FROM " + this.getReferenceNameForDatabaseObject(tableName));
            if (resultset.next()) {
                result = resultset.getInt("totalRows");
            }
        } catch (SQLException ex) {
            log.error("Error al intentar obtener el numero de rows en la tabla", ex);
        }
        return result;
    }

    @Override
    public List<String> getRangeIdForBatch(Connection conn, String tableName, int numberOfRowsPerBatch) {
        try {

            Statement stament = conn.createStatement();
            List<String> results = new ArrayList<>();

            String fieldUnique = "ROWID";
            int numberOfCuts = estimateNumberOfCuts(numberOfRowsPerBatch, conn, tableName);

            String tableNameReference = this.getReferenceNameForDatabaseObject(tableName);
            String initialValue = "";
            String maxValue = "";

            //Get the initial value
            String sqlInitialValue = getBottomValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, initialValue);
            ResultSet resultset = stament.executeQuery(sqlInitialValue);
            if (resultset.next()) {
                initialValue = resultset.getString("initialValue"); // This alias is setted in the query created by getBottomValueQuery
                results.add("'" + initialValue + "'");
                stament.close();
            }

            for (int i = 0; i < numberOfCuts; i++) {
                log.info("iteracion: " + i);
                if (i % 2 == 0) { // B
                    Statement topStatement = conn.createStatement();
                    String sqlMaxValue = getTopValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, initialValue);
                    log.info(sqlMaxValue);
                    resultset = topStatement.executeQuery(sqlMaxValue);
                    if (resultset.next()) {
                        maxValue = resultset.getString("maxValue");
                        results.add("'" + maxValue + "'");
                        topStatement.close();
                    }

                } else { //A
                    Statement bottomStatement = conn.createStatement();
                    sqlInitialValue = getBottomValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, maxValue);
                    log.info(sqlInitialValue);
                    resultset = bottomStatement.executeQuery(sqlInitialValue);
                    if (resultset.next()) {
                        initialValue = resultset.getString("initialValue");
                        results.add("'" + initialValue + "'");
                        bottomStatement.close();
                    }

                }
            }

            return results;

        } catch (SQLException ex) {
            log.error("[OracleDatabaseManager.getRangeIdForBatch] Error al generar los limites para los lotes.");
            log.error("Error Message" + ex.getMessage());
            throw new DatabaseSQLException();
        }
    }

    @Override
    public String getBottomValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String initialValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ")
          .append(uniqueField)
          .append(" initialValue FROM ( SELECT ")
          .append(uniqueField)
          .append(" FROM ")
          .append(tableName)
          .append(" ORDER BY ")
          .append(uniqueField)
          .append(")");

        if (StringUtils.isNotBlank(initialValue)) {
            sb.append(" WHERE ")
              .append(uniqueField)
              .append(" > '")
              .append(initialValue)
              .append("'")
              .append(" AND ROWNUM <= 1 ");
        } else {
            sb.append(" WHERE ROWNUM <= 1 ");
        }

        return sb.toString();
    }

    @Override
    public String getTopValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String bottomValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT MAX(T.rowkeytouse) maxValue FROM (SELECT ab.")
          .append(uniqueField)
          .append(" rowkeytouse FROM ")
          .append(tableName)
          .append(" ab WHERE ab.")
          .append(uniqueField)
          .append(" > '")
          .append(bottomValue)
          .append("' AND ROWNUM <= ")
          .append(numberOfRowsPerBatch)
          .append(" ORDER BY rowkeytouse ")
          .append(" ) T ");

        return sb.toString();
    }

    @Override
    public String getUniqueFieldForBatch(Connection conn, String tableName, int blocksize) {
        return "ROWID";
    }

}
