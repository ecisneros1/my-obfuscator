package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.AppContext;
import com.itpatagonia.obfuscator.process.exceptions.InvalidCredentialException;
import com.itpatagonia.obfuscator.process.exceptions.MSCCertificateException;
import com.itpatagonia.obfuscator.process.exceptions.NoLoggedUserException;
import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.utils.GenericAESCryptUtil;
import java.io.File;
import java.util.List;


import lombok.extern.log4j.Log4j2;
import stdbnk.msc.api.MSCException;
import stdbnk.msc.api.MSCSessionRestClient;
import stdbnk.msc.api.UsuarioNoLogueadoException;
import stdbnk.msc.ldap.Credencial;

/**
 * Service to talk with MSC API.
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class MSCIdentityService extends IdentityService {
    
    private MSCSessionRestClient mscClient;
    
    private static final String host;
    private static final int port;    
    private static final File certificate;
    private static final char[] keystorePassword;
    private static final char[] keyPassword ;
    private static final String applicationName;
    private static final String moduleName;
    private static final String domain;
    
    private String user;
    
    // IIB - Getting properties from config.xml
    static {
        host = AppContext.identityConfig.getHostMSC();
        port = AppContext.identityConfig.getPortMSC();    
        certificate = new File(AppContext.identityConfig.getCertificateFileMSC());
        keystorePassword = AppContext.identityConfig.getKeystorePasswordMSC().toCharArray();
        keyPassword = AppContext.identityConfig.getKeyPasswordMSC().toCharArray();
        applicationName = AppContext.identityConfig.getApplicationName();
        moduleName = AppContext.identityConfig.getModuleName();
        domain = AppContext.identityConfig.getDomainMSC();        
    }
    
    // IIB - The user to be used in the connection to MSC. This can be changed on Runtime.
    {        
        user = AppContext.identityConfig.getUserOfuMSC();
    }
    
    /**
     * 
     */
    public MSCIdentityService() {
    }
    
    
    /**
     * Instance the MSC Session and log in the user.
     */
    private void initializeMSCSession(){
        boolean isLogged = false;
        try {
            log.debug("Host:" + host);
            log.debug("port:" + port);
            log.debug("Certificate:"+ certificate.getPath() + ", exists:"+certificate.exists()+  ", canRead: "+certificate.canRead());            
            log.debug("applicationName:" +applicationName);
            log.debug("moduleName:" +moduleName);
            log.debug("domain:" +domain);
            log.debug("user:" +user);
            
            this.mscClient = new MSCSessionRestClient(host, port, certificate, decrypt(keystorePassword), decrypt(keyPassword), applicationName,moduleName);
            log.info("Iniciando session MSC.");
            isLogged = this.mscClient.login(domain+"\\"+user);
            log.info("Resultado del login msc: "+isLogged);
        } catch (MSCException ex) {
            log.error("MSCException - Verifique el certificado y las claves definidas.", ex);
            if (ex.getMessage().contains("exception decrypting")) {
                log.error("El certificado de MSC esta vencido, es invalido. JCE podría no estar instalado.");
            }
            throw new MSCCertificateException();
        } catch (Exception ex) {
            log.error("Error al logearse en MSC: ", ex);
        } catch (Error x) {
            log.error("Fatal Error: " + x.getMessage(), x);
        }
        if (!isLogged) {
            log.info("El usuario no logró logearse en MSC.");
            throw new NoLoggedUserException();
        }
    }
    
    
    /**
     * Get the MSCSession.
     * @return the MSC Session.
     */
    public MSCSessionRestClient getMscClient() {
        return mscClient;
    }

    @Override
    protected void initialize(String username) {
        this.user = username;
        initializeMSCSession();
    }

    @Override
    public Credential getCredential() {
        String idMSC = AppContext.identityConfig.getDefaultIdMSC();
        return getCredential(idMSC);
    }

    @Override
    public Credential getCredential(String idMSC) {
        if (this.getMscClient() != null) {
            try {
                Credencial c = this.getMscClient().consultarCredencial(idMSC);
                return new Credential(c.getUsuario(), c.getPassword());
                
            } catch (MSCException ex) {
                log.error("No existe la credencial: " + idMSC + " - " + ex.getMessage());
                throw new InvalidCredentialException(7, "No existe el MSC Id o es inválido.");
            } catch (UsuarioNoLogueadoException ex) {
                log.error("Usuario no logueado, MSC Exception - " + ex.getMessage());
                throw new NoLoggedUserException(4, "Usuario no logueado correctamente en MSC.");
            } catch (Exception ex) {
                log.error("Error desconocido: " + ex.getMessage(), ex);
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public List<String> getPermissions() {
        try {
            return getMscClient().consultarFunciones();
        } catch (UsuarioNoLogueadoException e) {
            log.error("El usuario no esta logueado en MSC.");
            throw new NoLoggedUserException();
        }
    }

    @Override
    public boolean hasPermission(String permission) {
        try {
            return getMscClient().consultarFuncion(permission);
        } catch (UsuarioNoLogueadoException e) {
            log.error("El usuario no esta logueado en MSC.");
            throw new NoLoggedUserException();
        }
    }

    @Override
    public void finish() {
        getMscClient().logout();
    }
    
    private char[] decrypt(char[] toDecrypt){   
        return GenericAESCryptUtil.decrypt(toDecrypt);            
    }
    private char[] encrypt(char[] toDecrypt){   
        return GenericAESCryptUtil.crypt(toDecrypt);            
    }

}
