package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.model.Credential;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author cd115986
 */
public class FakeIdentityService extends IdentityService {
    

    private String dbUser;
    private String dbPassword;
    private List<String> permissions;

    
    @Override
    protected void initialize(String username) {
        // NOTE: setear los valores a usar...
        this.dbUser = "root";
        this.dbPassword = "atenas";
        this.permissions = Arrays.asList("Leer", "Editar", "Auditar", "Obfuscar", "Reportar"); // TODO: poner en un enum...
    }

    @Override
    public void finish() {
        // Do nothing...
    }

    @Override
    public Credential getCredential() {
        return new Credential(this.dbUser, this.dbPassword);
    }

    @Override
    public Credential getCredential(String id) {
        return getCredential();
    }

    @Override
    public List<String> getPermissions() {
        return this.permissions;
    }

    @Override
    public boolean hasPermission(String permission) {
        return this.permissions.contains(permission);
    }

}
