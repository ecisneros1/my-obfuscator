package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import com.itpatagonia.obfuscator.process.utils.ObfuscatorUtils;
import java.sql.Connection;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class SQLServerObfuscator extends ObfuscatorManager {

    protected SQLServerObfuscator() {
    }

    protected SQLServerObfuscator(Database databaseXML, DatabaseManager dbm, Connection conn, boolean isXmlValid) {
        this.databaseXML = databaseXML;
        this.dbm = dbm;
        this.isXmlValid = isXmlValid;
        this.conn = conn;
    }

    @Override
    public String obfuscateByFixedValue(Table table, List<Column> columns) {
        return super.obfuscateByFixedValue(table, columns);
    }

    /**
     *
     * @param table
     * @param columns
     * @return
     */
    @Override
    public String obfuscateBySwitchingCharacters(Table table, List<Column> columns) {
        // 
        StringBuilder sqlToObfuscate = new StringBuilder();

        // replace para letras minusculas, letras mayusculas y numeros, usando COLLATE Latin1_General_CS_AI
        sqlToObfuscate.append("UPDATE ");
        sqlToObfuscate.append(this.dbm.getReferenceNameForDatabaseObject(table.getName()));
        sqlToObfuscate.append(" SET ");

        Iterator iColumn = columns.iterator();
        while (iColumn.hasNext()) {
            Column column = (Column) iColumn.next();
            String nameColumn = this.dbm.getReferenceNameForColumn(column.getName());
            sqlToObfuscate.append(nameColumn);
            sqlToObfuscate.append(" = ");

            String nameColumnCOLLATE = nameColumn + " COLLATE Latin1_General_CS_AI"; // case sensitive
            // replace para letras minusculas
            String replaces = ObfuscatorUtils.generateRecursiveReplace(nameColumnCOLLATE, this.lettersLowerCase);
            // replace para letras mayusculas
            replaces = ObfuscatorUtils.generateRecursiveReplace(replaces, this.lettersUpperCase);
            // replace para numeros
            replaces = ObfuscatorUtils.generateRecursiveReplace(replaces, this.digits);

            sqlToObfuscate.append(replaces);
            
            if (iColumn.hasNext()) {
                sqlToObfuscate.append(",");
            }
        }
        sqlToObfuscate.append(";");
        
//        // replaces particionado en 3 updates, para reducir los niveles de anidamiento de replace
//        // replace para letras minusculas
//        sqlToObfuscate.append(obfuscateBySwitchingCharacters(table, columns, this.lettersLowerCase));
//        // replace para letras mayusculas
//        sqlToObfuscate.append(obfuscateBySwitchingCharacters(table, columns, this.lettersUpperCase));
//        // replace para numeros
//        sqlToObfuscate.append(obfuscateBySwitchingCharacters(table, columns, this.digits));


//        return super.obfuscateBySwitchingCharacters(table, columns);
        return sqlToObfuscate.toString();
    }

    private String obfuscateBySwitchingCharacters(Table table, List<Column> columns, char[] characters) {

        StringBuilder sqlToObfuscate = new StringBuilder();
        sqlToObfuscate.append("UPDATE ");
        sqlToObfuscate.append(this.dbm.getReferenceNameForDatabaseObject(table.getName()));
        sqlToObfuscate.append(" SET ");

        Iterator iColumn = columns.iterator();
        while (iColumn.hasNext()) {
            Column column = (Column) iColumn.next();
            sqlToObfuscate.append(column.getName());
            sqlToObfuscate.append(" = ");
            sqlToObfuscate.append(ObfuscatorUtils.generateRecursiveReplace(column.getName(), characters));
            if (iColumn.hasNext()) {
                sqlToObfuscate.append(",");
            }
        }
        sqlToObfuscate.append(";");

        return sqlToObfuscate.toString();
    }

    protected final char[] lettersUpperCase = {
        'E', '.',
        'A', 'E',
        '.', 'A',
        'V', '.',
        'B', 'V',
        '.', 'B',
        'D', '.',
        'C', 'D',
        '.', 'C',
        'H', '.',
        'F', 'H',
        '.', 'F',
        'J', '.',
        'G', 'J',
        '.', 'G',
        'Y', '.',
        'I', 'Y',
        '.', 'I',
        'Q', '.',
        'K', 'Q',
        '.', 'K',
        'R', '.',
        'L', 'R',
        '.', 'L',
        'N', '.',
        'M', 'N',
        '.', 'M',
        'U', '.',
        'O', 'U',
        '.', 'O',
        'T', '.',
        'P', 'T',
        '.', 'P',
        'Z', '.',
        'S', 'Z',
        '.', 'S',
        'X', '.',
        'W', 'X',
        '.', 'W',};

    protected final char[] lettersLowerCase = {
        'e', '.',
        'a', 'e',
        '.', 'a',
        'v', '.',
        'b', 'v',
        '.', 'b',
        'd', '.',
        'c', 'd',
        '.', 'c',
        'h', '.',
        'f', 'h',
        '.', 'f',
        'j', '.',
        'g', 'j',
        '.', 'g',
        'y', '.',
        'i', 'y',
        '.', 'i',
        'q', '.',
        'k', 'q',
        '.', 'k',
        'r', '.',
        'l', 'r',
        '.', 'l',
        'n', '.',
        'm', 'n',
        '.', 'm',
        'u', '.',
        'o', 'u',
        '.', 'o',
        't', '.',
        'p', 't',
        '.', 'p',
        'z', '.',
        's', 'z',
        '.', 's',
        'x', '.',
        'w', 'x',
        '.', 'w'
    };

    protected final char[] digits = {
        '0', '3',
        '1', '6',
        '2', '9',
        '3', '7',
        '4', '8',
        '5', '1',
        '6', '0',
        '7', '5',
        '8', '2',
        '9', '4'
    };

}
