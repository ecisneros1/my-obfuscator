package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import java.sql.Connection;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Gortiz
 */
@Log4j2
public class SqlServerDatabaseManager extends DatabaseManager {
    
    
    /**
     * 
     * @param databaseName
     * @param host
     * @param port
     * @param serviceName
     * @param database 
     */
    protected SqlServerDatabaseManager(String databaseName, String host, String port, String serviceName, DatabasesEnum database) {
        super(databaseName, host, port, serviceName, database);
    }

    
    /**
     * 
     * @return 
     */
    @Override
    public String buildUrlDatabaseConnection() {
        StringBuilder urlConnection = new StringBuilder(150);
        // jdbc:jtds:sqlserver://MYPC:1433/Blog;instance=SQLEXPRESS
        urlConnection.append("jdbc:jtds:sqlserver://");
        urlConnection.append(this.host);
        urlConnection.append(":");
        urlConnection.append(this.port);
        urlConnection.append("/");
        urlConnection.append(this.databaseName);
        if (StringUtils.isNotBlank(this.serviceName)) {
            urlConnection.append(";instance=");
            urlConnection.append(this.serviceName); // instance
        }
        return urlConnection.toString();
    }

    
    /**
     * 
     * @param conn
     * @return 
     */
    @Override
    public List<String> getTablesNames(Connection conn) {
        String[] tableTypes = {"TABLE"};
        String catalog = this.databaseName;
        return getTablesNames(conn, catalog, null, null, tableTypes);
    }

    
    /**
     * 
     * @param conn
     * @param tablename
     * @return 
     */
    @Override
    public List<String> getColumnsNames(Connection conn, String tablename) {
        String catalog = this.databaseName;
        return getColumnsNames(conn, catalog, null, tablename, null);
    }
    

    /**
     * 
     * @param conn
     * @param tablename
     * @return 
     */
    @Override
    public List<String> getPrimaryKeys(Connection conn, String tablename) {
        String catalog = this.databaseName;
        return getPrimaryKeys(conn, catalog, null, tablename);
    }


    /**
     * 
     * @param conn
     * @param tablename
     * @return 
     */
    @Override
    public List<String> getImportedKeys(Connection conn, String tablename) {
        String catalog = this.databaseName;
        return getImportedKeys(conn, catalog, null, tablename);
    }

    /**
     * 
     * @param tableName
     * @return The reference name for a table object, i.e: "myBD.mySchema.tableName" 
     */
    @Override
    public String getReferenceNameForDatabaseObject(String tableName){
        return databaseName + ".." + tableName;
    }

    @Override
    public List<String> getUniqueReferenceIdPerRow(Connection conn, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getBottomValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String initialValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT TOP 1 ")
            .append(uniqueField)
            .append(" initialValue FROM ")
            .append(tableName);

        if (StringUtils.isNotBlank(initialValue)) {
            sb.append(" WHERE ")
                .append(uniqueField)
                .append(" > '")
                .append(initialValue)
                .append("'");
        }
        sb.append(" ORDER BY ")
            .append(uniqueField);

        return sb.toString();
    }

    @Override
    public String getTopValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String bottomValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT MAX(T.rowkeytouse) maxValue FROM (SELECT TOP  ")
            .append(numberOfRowsPerBatch)
            .append(" ab.")
            .append(uniqueField)
            .append(" rowkeytouse FROM ")
            .append(tableName)
            .append(" ab WHERE ab.")
            .append(uniqueField)
            .append(" > '")
            .append(bottomValue)
            .append(" ) T ")
            .append(" ORDER BY ab.")
            .append(uniqueField);

        return sb.toString();
    }
    
}
