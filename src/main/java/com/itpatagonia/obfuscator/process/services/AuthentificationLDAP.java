package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.AppContext;
import com.unboundid.ldap.sdk.Attribute;
import com.unboundid.ldap.sdk.ExtendedRequest;
import lombok.extern.log4j.Log4j2;

import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.LDAPSearchException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchResultEntry;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.extensions.StartTLSExtendedRequest;
import com.unboundid.util.ssl.SSLUtil;
import com.unboundid.util.ssl.TrustAllTrustManager;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.SSLContext;

/**
 *
 * @author cd115986
 */
@Log4j2
public class AuthentificationLDAP {

    public static boolean authenticate(String username, String password) throws LDAPException {
        try {
            if (AppContext.mainConfig.getIdentityProvider().contains(FakeIdentityService.class.getSimpleName())) {
                // no autentica usuario
                return true;
            }
            
            //TrustStoreTrustManager trust = new TrustStoreTrustManager("", "", "", false);
            //LDAPConnectionOptions options = new LDAPConnectionOptions();
            //options.setSSLSocketVerifier(new HostNameSSLSocketVerifier(false));
            SSLUtil sslUtil = new SSLUtil(new TrustAllTrustManager());
            sslUtil.setDefaultSSLProtocol("TLSv1");
            sslUtil.setEnabledSSLProtocols(Arrays.asList("TLSv1"));
            SSLContext sslContext = sslUtil.createSSLContext();
            ExtendedRequest extRequest = new StartTLSExtendedRequest(sslContext);

            // Establish a non-secure connection to the server.
            LDAPConnection ldap = new LDAPConnection(AppContext.mainConfig.getLdapHost(), Integer.valueOf(AppContext.mainConfig.getLdapPort()));
            ldap.processExtendedOperation(extRequest);

            //TODO check if we have to use a generic user.
            //LDAPConnection ldap = new LDAPConnection(AppContext.mainConfig.getLdapHost(), 389);
            //SearchResult sr = ldap.search(AppContext.mainConfig.getLdapBaseDN(), SearchScope.SUB, "(uid=" + username + ")");
            //ldap.connect(AppContext.mainConfig.getLdapHost(), 636);
            //Generic user getted from config file.
            //ldap.bind(AppContext.mainConfig.getLdapUser(), AppContext.mainConfig.getLdapPass());
            ldap.bind(username + "@" + AppContext.mainConfig.getLdapHost(), password);

            SearchResult sr = ldap.search(AppContext.mainConfig.getLdapBaseDN(), SearchScope.SUB, "(CN=" + username.toUpperCase() + ")");
            if (sr.getEntryCount() == 0) {
                return false;
            }
            String dn = sr.getSearchEntries().get(0).getDN();
            //String dn = AppContext.mainConfig.getLdapBaseDN();
            try {
                ldap.bind(dn, password);
                return true;
            } catch (LDAPException e) {
                if (e.getResultCode() == ResultCode.INVALID_CREDENTIALS) {
                    return false;
                }
                throw e;
            }
        } catch (GeneralSecurityException ex) {
            log.error("Credenciales Invalidas");
            return false;
        }
    }

    private String getNombre(LDAPConnection connection, String userId) {
        String filter = "distinguishedName=CN=" + userId + "," + AppContext.mainConfig.getLdapBaseDN();
        Attribute searchResult = getValue(connection, AppContext.mainConfig.getLdapBaseDN(), filter, "displayName");
        String nombre = searchResult != null ? searchResult.getValue() : null;
        return nombre;
    }

    private Attribute getValue(LDAPConnection connection, String baseDN, String filter, String attribute) {
        Attribute retValue = null;
        if (connection.isConnected()) {
            try {
                SearchResult searchResult = connection.search(baseDN, SearchScope.ONE, filter);
                List<SearchResultEntry> results = searchResult.getSearchEntries();
                for (SearchResultEntry e : results) {
                    Attribute attr = e.getAttribute(attribute);
                    if (attr != null) {
                        retValue = attr;
                        break;
                    }
                }
            } catch (LDAPSearchException e) {

            } finally {
                connection.close();
            }
        }

        return retValue;
    }

}
