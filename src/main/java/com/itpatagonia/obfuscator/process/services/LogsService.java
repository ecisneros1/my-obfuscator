
package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.AppContext;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class LogsService {
    
    public static List<String> readLog(String name){
        ArrayList<String> logLines = new ArrayList();
        String path = AppContext.mainConfig.getLogsDir();
        File logFile = new File(path, name);
        
        BufferedReader b;
        try {
            b = new BufferedReader(new FileReader(logFile));
            String readLine = "";
            while ((readLine = b.readLine()) != null) {
                logLines.add(readLine);
            }       
        } catch (FileNotFoundException ex) {
            Logger.getLogger(LogsService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogsService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return logLines;
    }
    
    
    
}
