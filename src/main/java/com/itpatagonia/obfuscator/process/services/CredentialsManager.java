package com.itpatagonia.obfuscator.process.services;

import java.util.List;

/**
 * Interface to get Credentials to connect to the database.
 * 
 * @author cd115986
 * @param <T>
 */
public interface CredentialsManager<T> {

    
    public T getCredential();
    
    public T getCredential(String id);
    
    public List<String> getPermissions();
    
    public boolean hasPermission(String permission);
    
}
