package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.AppContext;
import com.itpatagonia.obfuscator.process.exceptions.IdentityServiceException;
import com.itpatagonia.obfuscator.process.model.Credential;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author Gortiz
 */
@Log4j2
public abstract class IdentityService implements CredentialsManager<Credential> {

    
    /**
     * 
     * @param username
     * @return 
     */
    public static IdentityService getInstance(String username) {
        try {
            String identityProviderClass = AppContext.mainConfig.getIdentityProvider();
            IdentityService identityService = (IdentityService) Class.forName(identityProviderClass).newInstance();
            identityService.initialize(username);
            return identityService;
        } catch (Exception ex) {
            log.error("Error al instanciar IdentityService - " + ex.getMessage(), ex);
            throw new IdentityServiceException();  
        }
    }


    protected abstract void initialize(String username);
    
    public abstract void finish();

    
    @Override
    public abstract Credential getCredential();
    
    @Override
    public abstract Credential getCredential(String id);

    
}
