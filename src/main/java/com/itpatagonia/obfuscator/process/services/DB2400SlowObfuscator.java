package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.model.OfuscationMethod;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author gortiz
 */
@Log4j2
public class DB2400SlowObfuscator extends ObfuscatorManager {

    protected DB2400SlowObfuscator() {
    }

    public DB2400SlowObfuscator(Database databaseXML, DatabaseManager dbm, Connection conn, boolean isXmlValid) {
        this.databaseXML = databaseXML;
        this.dbm = dbm;
        this.isXmlValid = isXmlValid;
        this.conn = conn;
    }

    @Override
    public void obfuscate() {
        log.info("... iniciando DB2 ofuscacion LENTA - BD: " + databaseXML.getName());
        if (isXmlValid) {
            // Normal update lists
            fixedValuesColumns = null;
            switchingVowelsColumns = null;
            copingValuesColumns = null;
            // Batch based updates.
            fixedValuesColumnsBatch = null;
            switchingVowelsColumnsBatch = null;
            copingValuesColumnsBatch = null;

            StringBuilder sqlToObsfuscate = new StringBuilder(databaseXML.getTables().size() * 500);
            List<String> sqlToObsfuscateBatch = new ArrayList<>();
            List<String> tablesProcessList = new ArrayList<>();
            Queue<String> tablesProcessListBatch = new LinkedList<>();

            for (Table table : databaseXML.getTables()) {
                log.info("Table: " + table.getName() + " - BlockSize: " + table.getBlockSize());
                String blockSize = table.getBlockSize();
                //Check if it is set a blocksize, if it is, then process it as a Batch Process. (Exceptional case)
                if (blockSize != null && StringUtils.isNotBlank(blockSize) && Integer.parseInt(blockSize) > 0) {
                    //sqlToObsfuscateBatch.add("ALTER TABLE "+this.dbm.getReferenceNameForDatabaseObject(table.getName()) + " ACTIVATE NOT LOGGED INITIALLY;");
                    populateColumnListByBatchMethod(table);
                } else {
                    //sqlToObsfuscate.append("ALTER TABLE "+this.dbm.getReferenceNameForDatabaseObject(table.getName()) + " ACTIVATE NOT LOGGED INITIALLY;");
                    populateColumnListByMethod(table); //Common case. 
                }

                if (fixedValuesColumns != null && fixedValuesColumns.size() > 0) {
                    sqlToObsfuscate.append(obfuscateByFixedValue(table, fixedValuesColumns));
                    for (Column colunm : fixedValuesColumns) {
                        tablesProcessList.add("Table: " + table.getName() + " - Method: " + OfuscationMethod.Methods.VALOR_FIJO.name() + " - Column: " + colunm.getName());
                    }
                    fixedValuesColumns.clear();
                }

                if (switchingVowelsColumns != null && switchingVowelsColumns.size() > 0) {
                    sqlToObsfuscate.append(this.obfuscateBySwitchingCharacters(table, switchingVowelsColumns));
                    for (Column colunm : switchingVowelsColumns) {
                        tablesProcessList.add("Table: " + table.getName() + " - Method: " + OfuscationMethod.Methods.INTERCAMBIO_LETRAS.name() + " - Column: " + colunm.getName());
                    }
                    switchingVowelsColumns.clear();
                }

                if (copingValuesColumns != null && copingValuesColumns.size() > 0) {
                    sqlToObsfuscate.append(obfuscateByCopingValues(table, copingValuesColumns));
                    for (Column colunm : copingValuesColumns) {
                        tablesProcessList.add("Table: " + table.getName() + " - Method: " + OfuscationMethod.Methods.COPIAR_COLUMNA.name() + " - Column: " + colunm.getName());
                    }
                    copingValuesColumns.clear();
                }

                if (fixedValuesColumnsBatch != null && fixedValuesColumnsBatch.size() > 0) {
                    sqlToObsfuscateBatch.add(obfuscateByFixedValueBatch(table, fixedValuesColumnsBatch));
                    tablesProcessListBatch.add("Table: " + table.getName() + " - Batch method: " + OfuscationMethod.Methods.VALOR_FIJO.name());
                    fixedValuesColumnsBatch.clear();
                }
                if (switchingVowelsColumnsBatch != null && switchingVowelsColumnsBatch.size() > 0) {
                    sqlToObsfuscateBatch.add(obfuscateBySwitchingCharactersBatch(table, switchingVowelsColumnsBatch));
                    tablesProcessListBatch.add("Table: " + table.getName() + " - Batch method: " + OfuscationMethod.Methods.INTERCAMBIO_LETRAS.name());
                    switchingVowelsColumnsBatch.clear();
                }
                if (copingValuesColumnsBatch != null && copingValuesColumnsBatch.size() > 0) {
                    sqlToObsfuscateBatch.add(obfuscateByCopingValuesBatch(table, copingValuesColumnsBatch));
                    tablesProcessListBatch.add("Table: " + table.getName() + " - Batch method: " + OfuscationMethod.Methods.COPIAR_COLUMNA.name());
                    copingValuesColumnsBatch.clear();
                }
            }

            log.info("....... ejecutando ofuscamiento de datos");
            if (StringUtils.isNotBlank(sqlToObsfuscate.toString())) {
                log.debug("....... " + sqlToObsfuscate.toString());
                int[] results = dbm.sendSimplesQueryTODB(conn, sqlToObsfuscate.toString());
                for (int i = 0; i < results.length; i++) {
                    log.info("....... " + tablesProcessList.get(i) + " - registros actualizados: " + results[i]);
                }
            } else {
                log.info("....... No hay tablas a ofuscar con el proceso único. Continuará con el proceso en lotes.");
            }

            log.info("....... ejecutando ofuscamiento de datos en lotes");
            if (CollectionUtils.isNotEmpty(sqlToObsfuscateBatch)) {
                sqlToObsfuscateBatch.stream().forEach((s) -> {
                    int[] resultsBatch = dbm.sendSimplesQueryTODB(conn, s);
                    int totalUpdatesForTable = sumAll(resultsBatch);
                    log.info("....... " + tablesProcessListBatch.poll() + " - registros actualizados: " + totalUpdatesForTable);
                });
            } else {
                log.info("....... No hay tablas a ofuscar con el proceso en lotes.");
            }

        }
        log.info("....... ofuscacion finalizada exitosamente.");
    }

    @Override
    public String obfuscateByFixedValue(Table table, List<Column> columns) {
        return super.obfuscateByFixedValue(table, columns);
    }

    @Override
    public String obfuscateBySwitchingCharacters(Table table, List<Column> columns) {
        return super.obfuscateBySwitchingCharacters(table, columns);
    }

    @Override
    public String obfuscateInBatch(Table table, String sqlFromMethod) {
        int blockSize = Integer.parseInt(table.getBlockSize());
        List<String> rowID = this.dbm.getRangeIdForBatch(conn, table.getName(), blockSize);
        String uniqueField = this.dbm.getUniqueFieldForBatch(conn, table.getName(), blockSize);
        StringBuilder result = new StringBuilder(1000);

        for (int i = 0; i < rowID.size(); i++) {
            // Create StringBuilder with enough space for the query.
            StringBuilder sqlToObfuscate = new StringBuilder(sqlFromMethod.length() + 250);

            sqlToObfuscate.append(sqlFromMethod);
            sqlToObfuscate.append(" WHERE ")
                .append(uniqueField)
                .append(" BETWEEN ")
                .append(rowID.get(i++))
                .append(" AND ")
                .append(rowID.get(i))
                .append(";");
            result.append(sqlToObfuscate.toString());
        }

        return result.toString();
    }

}
