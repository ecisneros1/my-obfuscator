package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import java.sql.Connection;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author gortiz
 */
@Log4j2
public class Db2400DatabaseManager extends DatabaseManager {

    /**
     * 
     * @param databaseName
     * @param host
     * @param port
     * @param serviceName
     * @param database 
     */
    protected Db2400DatabaseManager(String databaseName, String host, String port, String serviceName, DatabasesEnum database) {
        super(databaseName, host, port, serviceName, database);
    }

    /**
     * 
     * @return 
     */
    @Override
    public String buildUrlDatabaseConnection() {
        StringBuilder urlConnection = new StringBuilder(150);
        //datasource.db2400.url=jdbc:as400://hostame;prompt=false
        //datasource.db2400.url=jdbc:as400://192.168.21.231;prompt=false;
        // jdbc:as400://{host};[libraries={database};]
        // jdbc:as400://192.168.21.231;libraries=GOMLIB;
        urlConnection.append("jdbc:as400://");
        urlConnection.append(this.host);
//        urlConnection.append(":");
//        urlConnection.append(this.port);
//        urlConnection.append("/");
//        urlConnection.append(this.databaseName);
        urlConnection.append(";libraries=");
        urlConnection.append(this.databaseName);
//        urlConnection.append(";prompt=false;");
        return urlConnection.toString();
    }

    @Override
    public List<String> getTablesNames(Connection conn) {
        String[] tableTypes = {"TABLE"};
        String schema = this.databaseName;
        return getTablesNames(conn, null, schema, null, tableTypes);
    }

    @Override
    public List<String> getColumnsNames(Connection conn, String tablename) {
        String schema = this.databaseName;
        return getColumnsNames(conn, null, schema, tablename, null);
    }

    @Override
    public List<String> getPrimaryKeys(Connection conn, String tablename) {
        String schema = this.databaseName;
        return getPrimaryKeys(conn, null, schema, tablename);
    }

    @Override
    public List<String> getImportedKeys(Connection conn, String tablename) {
        String schema = this.databaseName;
        return getImportedKeys(conn, null, schema, tablename);
    }

    @Override
    public String getReferenceNameForDatabaseObject(String tableName) {
        return databaseName + "." + tableName;
    }

    @Override
    public List<String> getUniqueReferenceIdPerRow(Connection conn, String tableName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTopValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String bottomValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT MAX(T.")
          .append(uniqueField)
          .append(") as maxValue FROM (SELECT ab.")
          .append(uniqueField)
          .append(" FROM ")
          .append(tableName)
          .append(" AS ab WHERE ab.")
          .append(uniqueField)
          .append(" > '")
          .append(bottomValue)
          .append("' ORDER BY ab.")
          .append(uniqueField)
          .append(" FETCH FIRST ")
          .append(numberOfRowsPerBatch)
          .append(" ROWS ONLY) AS T ");
        return sb.toString();
    }

    @Override
    public String getBottomValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String initialValue) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ")
          .append(uniqueField)
          .append(" as initialValue FROM ")
          .append(tableName);

        if (StringUtils.isNotBlank(initialValue)) {
            sb.append(" WHERE ")
              .append(uniqueField)
              .append(" > '")
              .append(initialValue)
              .append("'");
        }

        sb.append(" ORDER BY ")
          .append(uniqueField)
          .append(" FETCH FIRST 1 ROWS ONLY");

        return sb.toString();
    }
    
}
