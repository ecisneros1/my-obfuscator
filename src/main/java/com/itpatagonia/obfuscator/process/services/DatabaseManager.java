package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.model.Environment;
import com.itpatagonia.obfuscator.configuration.xml.model.OfuscationMethod;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import com.itpatagonia.obfuscator.process.exceptions.CanNotFindACandidateColumnException;
import com.itpatagonia.obfuscator.process.exceptions.CheckMetadataException;
import com.itpatagonia.obfuscator.process.exceptions.DatabaseConnectionException;
import com.itpatagonia.obfuscator.process.exceptions.DatabaseSQLException;
import com.itpatagonia.obfuscator.process.model.Credential;
import com.itpatagonia.obfuscator.process.model.DatabasesEnum;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 * @author Gortiz
 */
@Log4j2
public abstract class DatabaseManager implements DatabaseConnection, DatabaseMetadata {

    private String user;
    private String pass;

    protected String databaseName = "";
    protected String host = "";
    protected String port = "";
    protected String serviceName = "";
    protected DatabasesEnum database;
    
    protected String identifierQuoteString = "";

    /**
     *
     * @param databaseName
     * @param host
     * @param port
     * @param serviceName
     * @param database
     */
    protected DatabaseManager(String databaseName, String host, String port, String serviceName, DatabasesEnum database) {
        this.database = database;
        this.databaseName = databaseName;
        this.serviceName = serviceName;
        this.host = host;
        this.port = port;
    }

    /**
     *
     * @param databaseName
     * @param host
     * @param port
     * @param serviceName
     * @param database
     * @return
     */
    public static DatabaseManager getInstance(String databaseName, String host, String port, String serviceName, DatabasesEnum database) {
        switch (database) {
            case ORACLE10g:
            case ORACLE11g:
            case ORACLE12c:
            case ORACLE18c:
                return new OracleDatabaseManager(databaseName, host, port, serviceName, database);
            case DB2:
                return new Db2DatabaseManager(databaseName, host, port, serviceName, database);
            case DB2_400:
                return new Db2400DatabaseManager(databaseName, host, port, serviceName, database);
            case SQLSERVER2008:
            case SQLSERVER2012:
            case SQLSERVER2014:
            case SQLSERVER2016:
            case SQLSERVER2017:
                return new SqlServerDatabaseManager(databaseName, host, port, serviceName, database);
            case MYSQL:
                return new MySqlDatabaseManager(databaseName, host, port, serviceName, database);
            default:
                throw new UnsupportedOperationException("Not supported yet - " + database);
        }
    }

    /**
     *
     * @param databaseXML
     * @param ambiente
     * @return
     */
    public static DatabaseManager getInstance(Database databaseXML, String ambiente) {
        Environment environment = databaseXML.getEnvironment(ambiente);
        String dbName = (StringUtils.isBlank(environment.getOverrideName())) ? databaseXML.getName() : environment.getOverrideName();
        return DatabaseManager.getInstance(dbName, environment.getHost(), environment.getPort(), environment.getServiceName(), DatabasesEnum.getDatabaseEnum(databaseXML.getVendorDataBase(), databaseXML.getVersionDataBase()));
    }

    /**
     * Return the Driver class name.
     *
     * @return
     */
    @Override
    public final String getDriverClass() {
        return database.getDriverClass();
    }

    /**
     * Builds the url needed to connect to the database.
     *
     * @return
     */
    @Override
    public abstract String buildUrlDatabaseConnection();

    /**
     * Get the connection to the database.
     *
     * @param credential
     * @return
     */
    @Override
    public final Connection getConnection(Credential credential) {
        Connection conn = null;
        String urlConnection = buildUrlDatabaseConnection();
        try {
            this.user = credential.getUser();
            this.pass = credential.getPwd();
            Class.forName(getDriverClass()).newInstance();
            conn = DriverManager.getConnection(urlConnection, this.user, this.pass);

        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            log.error("La conexion a la base de datos no fue posible debido a: " + ex.getMessage() + " - " + urlConnection, ex);
            if (ex instanceof SQLException) {
                SQLException sqle = (SQLException) ex;
                log.error("Error: " + sqle.getErrorCode() + " - " + sqle.getSQLState());
            }
            throw new DatabaseConnectionException();
        }

        return conn;
    }

    /**
     * Validates that the tables passed exist in the database.
     *
     * @param conn
     * @param xmlTables
     * @return
     */
    @Override
    public final Map<Table, Boolean> checkTablesNames(Connection conn, List<Table> xmlTables) {

        // obtiene nombres de las tablas de la metadata
        List<String> tablesNames = getTablesNames(conn);

        // validar las tablas
        Map<Table, Boolean> result = validTablesNames(xmlTables, tablesNames);

        return result;
    }

    /**
     * Get all the tables names in the Database in the default schema.
     *
     * @param conn
     * @return
     */
    @Override
    public abstract List<String> getTablesNames(Connection conn);

    /**
     * Get all the tables names in the Database and specific schema.
     *
     * @param conn
     * @param catalog
     * @param schemaPattern
     * @param tableNamePattern
     * @param types
     * @return
     */
    protected List<String> getTablesNames(Connection conn, String catalog, String schemaPattern, String tableNamePattern, String[] types) {
        try {
            List<String> tables = new ArrayList();
            DatabaseMetaData dmd = conn.getMetaData();
            log.debug("Database product name: " + dmd.getDatabaseProductName());
            log.debug("Database product version: " + dmd.getDatabaseProductVersion());
            log.debug("Database version: " + dmd.getDatabaseMajorVersion());
            log.debug("." + dmd.getDatabaseMinorVersion());
            log.debug("." + dmd.getDriverName());
            log.debug("." + dmd.getDriverVersion());
            
            log.debug("IdentifierQuoteString: " + dmd.getIdentifierQuoteString());
            
            ResultSet rs = dmd.getTables(catalog, schemaPattern, tableNamePattern, types);
            while (rs.next()) {
                tables.add(rs.getString("TABLE_NAME"));
            }
            rs.close();
            return tables;
        } catch (SQLException ex) {
            log.error("Ocurrió un error al obtener la metadata de las tablas -  Exception: " + ex.getMessage());
            throw new CheckMetadataException();
        } catch (Exception ex) {
            log.error("Unexpected exception: " + ex.getMessage(), ex);
            throw new CheckMetadataException();
        }
    }

    /**
     * Compare if the tableNames in the first list exist in the other list.
     *
     * @param xmlTables
     * @param tablesNames
     * @return
     */
    protected Map<Table, Boolean> validTablesNames(List<Table> xmlTables, List<String> tablesNames) {
        Map<Table, Boolean> results = new HashMap<>();
        for (Table table : xmlTables) {
            boolean wasFound = tablesNames.contains(table.getName());
            results.put(table, wasFound);
        }

        return results;
    }

    /**
     * Validates that the definition of columns passed exist in the metadata.
     *
     * @param conn
     * @param table
     * @return
     */
    @Override
    public final Map<Column, Boolean> checkColumnsNames(Connection conn, Table table) {

        // obtiene nombres de las columnas de la metadata
        List<String> columnsNames = getColumnsNames(conn, table.getName());

        // validar las columnas de la tabla
        List<Column> xmlColumns = table.getColumns();
        Map<Column, Boolean> result = validColumnsNames(xmlColumns, columnsNames);

        return result;
    }

    /**
     * Get a list of columns names in this table.
     *
     * @param conn
     * @param tablename
     * @return
     */
    @Override
    public abstract List<String> getColumnsNames(Connection conn, String tablename);

    /**
     *
     * @param conn
     * @param catalog
     * @param schemaPattern
     * @param tableNamePattern
     * @param columnNamePattern
     * @return
     */
    protected List<String> getColumnsNames(Connection conn, String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern) {
        try {
            log.debug("Buscando la metadata de columnas para la tabla: " + tableNamePattern);
            List<String> columnsNames = new ArrayList();
            DatabaseMetaData dmd = conn.getMetaData();
            ResultSet rs = dmd.getColumns(catalog, schemaPattern, tableNamePattern, columnNamePattern);
            log.debug("------La Metadata de las columnas------");
            log.debug("Metadata de las columnas de la Tabla: " + tableNamePattern);
            while (rs.next()) {
                columnsNames.add(rs.getString("COLUMN_NAME"));
                log.debug("--" + rs.getString("COLUMN_NAME"));
                //rs.getString("COLUMN_SIZE");
            }
            rs.close();
            return columnsNames;
        } catch (SQLException ex) {
            log.error("Ocurrió un error durante la comprobación de las columnas, para la tabla: " + tableNamePattern + ", Exception: " + ex.getMessage());
            throw new CheckMetadataException();
        } catch (Exception ex) {
            log.error("Unexpected exception: " + ex.getMessage(), ex);
            throw new CheckMetadataException();
        }
    }

    /**
     *
     * @param xmlColumns
     * @param columnsNames
     * @return
     */
    protected Map<Column, Boolean> validColumnsNames(List<Column> xmlColumns, List<String> columnsNames) {
        Map<Column, Boolean> results = new HashMap();
        for (Column column : xmlColumns) {
            boolean wasFound = columnsNames.contains(column.getName());
            results.put(column, wasFound);
        }
        return results;
    }

    /**
     *
     * @param conn
     * @param table
     * @return
     */
    @Override
    public final Map<Column, Boolean> checkPrimaryKeys(Connection conn, Table table) {

        // obtiene nombres de las columnas PK de la metadata
        List<String> pkColumnsNames = getPrimaryKeys(conn, table.getName());

        // validar las columnas PK de la tabla
        List<Column> xmlColumns = table.getColumns();
        Map<Column, Boolean> result = validPrimaryKeys(xmlColumns, pkColumnsNames);

        return result;
    }

    /**
     *
     * @param conn
     * @param tablename
     * @return
     */
    @Override
    public abstract List<String> getPrimaryKeys(Connection conn, String tablename);

    /**
     *
     * @param conn
     * @param catalog
     * @param schema
     * @param table
     * @return
     */
    protected List<String> getPrimaryKeys(Connection conn, String catalog, String schema, String table) {
        try {
            List<String> pkColumns = new ArrayList();
            DatabaseMetaData dmd = conn.getMetaData();
            ResultSet rs = dmd.getPrimaryKeys(catalog, schema, table);
            log.debug("PK Columns' Metadata from the Table: " + table);
            while (rs.next()) {
                pkColumns.add(rs.getString("COLUMN_NAME"));
                log.debug("--" + rs.getString("COLUMN_NAME"));
            }
            rs.close();
            return pkColumns;
        } catch (SQLException ex) {
            log.error("Ocurrió un error durante la comprobación de las columnas PK, para la tabla: " + table + ", Exception: " + ex.getMessage());
            throw new CheckMetadataException();
        } catch (Exception ex) {
            log.error("Unexpected exception: " + ex.getMessage(), ex);
            throw new CheckMetadataException();
        }
    }

    /**
     *
     * @param xmlColumns
     * @param pkColumnsNames
     * @return
     */
    protected Map<Column, Boolean> validPrimaryKeys(List<Column> xmlColumns, List<String> pkColumnsNames) {
        Map<Column, Boolean> results = new HashMap();
        for (Column column : xmlColumns) {
            boolean isPK = pkColumnsNames.contains(column.getName());
            results.put(column, isPK);
        }
        return results;
    }

    /**
     *
     * @param conn
     * @param table
     * @return
     */
    @Override
    public final Map<Column, Boolean> checkImportedKeys(Connection conn, Table table) {

        // obtiene nombres de las columnas FK de la metadata
        List<String> fkColumnsNames = getImportedKeys(conn, table.getName());

        // validar las columnas FK de la tabla
        List<Column> xmlColumns = table.getColumns();
        Map<Column, Boolean> result = validImportedKeys(xmlColumns, fkColumnsNames);

        return result;
    }

    /**
     *
     * @param conn
     * @param tablename
     * @return
     */
    @Override
    public abstract List<String> getImportedKeys(Connection conn, String tablename);

    /**
     *
     * @param conn
     * @param catalog
     * @param schema
     * @param table
     * @return
     */
    protected List<String> getImportedKeys(Connection conn, String catalog, String schema, String table) {
        try {
            List<String> fkColumns = new ArrayList();
            DatabaseMetaData dmd = conn.getMetaData();
            ResultSet rs = dmd.getImportedKeys(catalog, schema, table);
            log.debug("FK Columns' Metadata from the Table: " + table);
            while (rs.next()) {
                fkColumns.add(rs.getString("FKCOLUMN_NAME"));
                log.debug("--" + rs.getString("FKCOLUMN_NAME"));
            }
            rs.close();
            return fkColumns;

        } catch (SQLException ex) {
            log.error("Ocurrió un error durante la comprobación de las columnas, para la tabla: " + table + ", Exception: " + ex.getMessage());
            throw new CheckMetadataException();
        } catch (Exception ex) {
            log.error("Unexpected exception: " + ex.getMessage(), ex);
            throw new CheckMetadataException();
        }
    }

    /**
     *
     * @param xmlColumns
     * @param fkColumnsNames
     * @return
     */
    protected Map<Column, Boolean> validImportedKeys(List<Column> xmlColumns, List<String> fkColumnsNames) {
        Map<Column, Boolean> results = new HashMap();
        for (Column column : xmlColumns) {
            boolean isFK = fkColumnsNames.contains(column.getName());
            results.put(column, isFK);
        }
        return results;
    }

    /**
     * Sends the queries to the databases using batch statements.
     *
     * @see JDBC Statement.addBatch()
     * @param conn
     * @param query
     * @return
     */
    public int[] sendQueryTODB(Connection conn, String query) {
        try {
            Statement stament = conn.createStatement();
            String[] queries = query.split(";");
            for (String singleQuery : queries) {
                stament.addBatch(singleQuery);
            }
            int[] results = stament.executeBatch();

            return results;

        } catch (SQLException ex) {
            log.error("[DatabaseManager.sendQueryTODB] Error al ejecutar el query de ofuscamiento.");
            log.error("Error Message " + ex.getMessage());
            log.error("Error Message nextException: " + ex.getNextException().getMessage());
            throw new DatabaseSQLException();
        }
    }

    /**
     *
     * @param conn
     * @param query
     * @return
     */
    public int[] sendSimplesQueryTODB(Connection conn, String query) {
        List<Integer> results = new ArrayList();
        try {
            Statement stament = conn.createStatement();
            String[] queries = query.split(";");
            log.debug(".....Numero de queries a ejecutar: " + queries.length);
            for (String singleQuery : queries) {
                log.debug("......" + singleQuery);
                results.add(stament.executeUpdate(singleQuery));
            }
            return results.stream().mapToInt(i -> i).toArray();

        } catch (SQLException ex) {
            log.error("[DatabaseManager.sendSimplesQueryTODB] Error al ejecutar el query de ofuscamiento lento. Nro Queries Ejecutados: " + results.size());
            log.error("Error Message " + ex.getMessage());
            throw new DatabaseSQLException();
        }
    }

    /**
     *
     * @param conn
     * @return
     */
    public List<Table> getModelFromMetadata(Connection conn) {
        List<Table> tables = new ArrayList();
        try {
            List<String> tablesNames = getTablesNames(conn);
            Iterator iteratorTables = tablesNames.iterator();

            Table tableObj;
            Column columnObj;

            OfuscationMethod ofuMethod = new OfuscationMethod();
            ofuMethod.setName(OfuscationMethod.Methods.INTERCAMBIO_LETRAS.name());

            List<Column> columns;
            List<String> columnsName;
            List<String> pkList;
            List<String> fkList;

            while (iteratorTables.hasNext()) {

                tableObj = new Table();
                tableObj.setName((String) iteratorTables.next());

                columns = new ArrayList();
                columnsName = getColumnsNames(conn, tableObj.getName());

                pkList = getPrimaryKeys(conn, tableObj.getName());
                fkList = getImportedKeys(conn, tableObj.getName());

                Iterator iteratorColumnsNames = columnsName.iterator();
                while (iteratorColumnsNames.hasNext()) {
                    columnObj = new Column();
                    String name = (String) iteratorColumnsNames.next();

                    columnObj.setName(name);
                    columnObj.setOfuscationMethod(ofuMethod);

                    columnObj.setPK(pkList.contains(name));
                    columnObj.setFK(fkList.contains(name));

                    columns.add(columnObj);

                }
                tableObj.setColumns(columns);
                tables.add(tableObj);
            }

        } catch (Exception ex) {
            log.error("Unexpected exception: " + ex);
            throw ex;
        }

        return tables;
    }

    /**
     * Get the full name for a table to be used in a sql statement. The full
     * name of the table usually includes the database name, schema, and table
     * name separated by dots.
     *
     * @param tableName
     * @return
     */
    public abstract String getReferenceNameForDatabaseObject(String tableName);

    /**
     * 
     * @param columnName
     * @return 
     */
    public String getReferenceNameForColumn(String columnName) {
        return columnName;
    }
    
    /**
     * Gets a list of id for each row in the table.
     *
     * @param conn
     * @param tableName
     * @return
     */
    public abstract List<String> getUniqueReferenceIdPerRow(Connection conn, String tableName);

    /**
     * Gets an candidate column/field that is enough unique to be used for batch
     * process.
     *
     * @param conn
     * @param tableName
     * @param blocksize
     * @return
     */
    public String getUniqueFieldForBatch(Connection conn, String tableName, int blocksize) {

        List<String> pkList = getPrimaryKeys(conn, tableName);
        try {
            for (String field : pkList) {
                if (validateColumnAsCandidate(tableName, field, conn, blocksize)) {
                    return field;
                }
            }
            pkList = getColumnsNames(conn, tableName);
            for (String field : pkList) {
                if (validateColumnAsCandidate(tableName, field, conn, blocksize)) {
                    return field;
                }
            }
            // TODO Change for an specific exception. 
            throw new CanNotFindACandidateColumnException();
        } catch (SQLException ex) {
            throw new CanNotFindACandidateColumnException();
        }

    }

    /**
     * Get a list with a even number of items that represents the lowest and
     * highest id value for each batch. Example: From a table with 200 rows and
     * batchs of 20 rows, will return 20 ids.
     *
     * @param conn
     * @param tableName
     * @param numberOfRowsPerBatch
     * @return
     */
    public List<String> getRangeIdForBatch(Connection conn, String tableName, int numberOfRowsPerBatch) {
        try {
            Statement stament = conn.createStatement();
            List<String> results = new ArrayList<>();

            String fieldUnique = getUniqueFieldForBatch(conn, tableName, numberOfRowsPerBatch);
            int numberOfCuts = estimateNumberOfCuts(numberOfRowsPerBatch, conn, tableName);

            String tableNameReference = this.getReferenceNameForDatabaseObject(tableName);
            String initialValue = "";
            String maxValue = "";

            //Get the initial value
            String sqlInitialValue = getBottomValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, initialValue);
            ResultSet resultset = stament.executeQuery(sqlInitialValue);
            if (resultset.next()) {
                initialValue = resultset.getString("initialValue"); // This alias is setted in the query created by getBottomValueQuery
                results.add("'" + initialValue + "'");
                stament.close();
            }

            for (int i = 0; i <= numberOfCuts; i++) {
                log.info("iteracion: " + i);
                if (i % 2 == 0) {
                    Statement topStatement = conn.createStatement();
                    String sqlMaxValue = getTopValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, initialValue);
                    resultset = topStatement.executeQuery(sqlMaxValue);
                    if (resultset.next()) {
                        maxValue = resultset.getString("maxValue");
                        results.add("'" + maxValue + "'");
                        topStatement.close();
                    }

                } else {
                    Statement bottomStatement = conn.createStatement();
                    sqlInitialValue = getBottomValueQuery(tableNameReference, fieldUnique, numberOfRowsPerBatch, maxValue);
                    resultset = bottomStatement.executeQuery(sqlInitialValue);
                    if (resultset.next()) {
                        initialValue = resultset.getString("initialValue");
                        results.add("'" + initialValue + "'");
                        bottomStatement.close();
                    }

                }
            }

            return results;

        } catch (SQLException ex) {
            log.error("[DatabaseManager.sendQueryTODB] Error al ejecutar el query de ofuscamiento.");
            log.error("Error Message" + ex.getMessage());
            throw new DatabaseSQLException();
        }
    }

    /**
     * Get the total number of rows in the table passed.
     *
     * @param conn
     * @param tableName
     * @return
     */
    public int getNumberOfRows(Connection conn, String tableName) {
        int result = 0;
        try {
            Statement stament = conn.createStatement();
            ResultSet resultset = stament.executeQuery("SELECT COUNT(*) AS totalRows FROM " + this.getReferenceNameForDatabaseObject(tableName));
            if (resultset.next()) {
                result = resultset.getInt("totalRows");
            }
        } catch (SQLException ex) {
            log.error("Error al intentar obtener el numero de rows en la tabla: " + tableName, ex);
        }
        return result;
    }

    /**
     * Gets the total number of rows in the table passed that have a different
     * value in the field specified.
     *
     * @param conn
     * @param tableName
     * @param uniqueField
     * @return
     */
    public int getNumberOfRowsDistinctByField(Connection conn, String tableName, String uniqueField) {
        int result = 0;
        try {
            Statement stament = conn.createStatement();
            ResultSet resultset = stament.executeQuery("SELECT COUNT(DISTINCT " + uniqueField + ") AS totalRows FROM " + this.getReferenceNameForDatabaseObject(tableName));
            if (resultset.next()) {
                result = resultset.getInt("totalRows");
            }
        } catch (SQLException ex) {
            log.error("Error al intentar obtener el numero de rows en la tabla", ex);
        }
        return result;
    }

    /**
     * Build a query to get Top id value of a subset of the total rows of the
     * table (specified by the numberOfRowsPerBatch and bottom value).
     *
     * @param tableName
     * @param uniqueField
     * @param numberOfRowsPerBatch
     * @param bottomValue
     * @return
     */
    public abstract String getTopValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String bottomValue);

    /**
     * Build a query to get Bottom id value of a subset of the total rows of the
     * table (specified by the numberOfRowsPerBatch and initialValue). If the
     * initial value passed is blank or null, it will get the first value of the
     * table ordered by uniqueField.
     *
     * @param tableName
     * @param uniqueField
     * @param numberOfRowsPerBatch
     * @param initialValue
     * @return
     */
    public abstract String getBottomValueQuery(String tableName, String uniqueField, int numberOfRowsPerBatch, String initialValue);

    /**
     * Validates if the field passed has values enough different (DISTINCT
     * between them) to be used as "id" for the batch proccess.
     *
     * @param tableName
     * @param columnName
     * @param conn
     * @param blocksize
     * @return
     * @throws SQLException
     */
    private boolean validateColumnAsCandidate(String tableName, String columnName, Connection conn, int blocksize) throws SQLException {

        boolean result = true;
        //Get the total number of rows in the table.
        double totalRows = getNumberOfRows(conn, tableName);
        if (totalRows > blocksize) {
            // Get the total number of rows that have a distint value in the choosen field.
            double totalDistintRows = getNumberOfRowsDistinctByField(conn, tableName, columnName);
            //Calculate the tolerance that we can have related to the variable number of rows that 
            //we could have inside a batch of rows for this table with this blocksize.
            double tolerance = 2 - (1 / (totalRows / blocksize));
            result = totalRows - totalDistintRows <= (blocksize * tolerance);
        }

        return result;
    }

    /**
     * Given the number of total rows in the table and the blocksize or number
     * of Rows per batch, return the number of iteration or "cuts" needed to
     * process the whole table.
     *
     * @param numberOfRowsPerBatch
     * @param conn
     * @param tableName
     * @return
     */
    protected int estimateNumberOfCuts(int numberOfRowsPerBatch, Connection conn, String tableName) {
        int numberOfRowsInTable = getNumberOfRows(conn, tableName);
        int numberOfCuts;
        if (numberOfRowsPerBatch > 0 && numberOfRowsPerBatch < numberOfRowsInTable) {
            numberOfCuts = (numberOfRowsInTable / numberOfRowsPerBatch);
            numberOfCuts = (++numberOfCuts) * 2;
        } else {
            numberOfCuts = 1;
        }
        return numberOfCuts;
    }

    /**
     *
     * @param conn
     * @param tableNames
     * @return
     */
    public Map<String, Integer> getNumberOfRowsPerTable(Connection conn, List<String> tableNames) {
        log.info("INI DatabaseManager.numberOfRows ******************************");
        log.info("... contando filas - BD: " + databaseName + " - " + host + " - " + serviceName);
        Map<String, Integer> listOfTablesAndRows = new HashMap<>();
        for (String tableName : tableNames) {
            Integer numberOfRows = getNumberOfRows(conn, tableName);
            listOfTablesAndRows.put(tableName, numberOfRows);
            log.info("...... tabla: " + tableName + " - registros: " + numberOfRows);
        }
        log.info("...... conteo filas finalizado exitosamente.");
        log.info("END DatabaseManager.numberOfRows ******************************");
        return listOfTablesAndRows;
    }

    /**
     * Obtener IdentifierQuoteString de DatabaseMetaData
     * 
     * @param conn 
     */
    public void setIdentifierQuoteString(Connection conn) {
        try {
            // set IdentifierQuoteString...
            DatabaseMetaData dmd = conn.getMetaData();
            this.identifierQuoteString = dmd.getIdentifierQuoteString();
            log.debug("IdentifierQuoteString: " + this.identifierQuoteString);
            
        } catch (SQLException ex) {
            log.error("Ocurrió un error al obtener la metadata -  Exception: " + ex.getMessage());
            throw new CheckMetadataException();
        } catch (Exception ex) {
            log.error("Unexpected exception: " + ex.getMessage(), ex);
            throw new CheckMetadataException();
        }
    }

    @Override
    public String toString() {
        return "DatabaseManager{" + "databaseName=" + databaseName + ", host=" + host + ", port=" + port + ", serviceName=" + serviceName + ", database=" + database + '}';
    }

}
