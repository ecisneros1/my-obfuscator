package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.model.Environment;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import com.itpatagonia.obfuscator.configuration.xml.services.XstreamService;
import com.itpatagonia.obfuscator.process.model.DatabaseDetailReportItem;
import com.itpatagonia.obfuscator.process.model.DatabaseReportItem;
import com.itpatagonia.obfuscator.process.utils.ObfuscatorUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author cd115986
 */
@Log4j2
public class ListService {
    
    public static List<DatabaseReportItem> getDatabasesReportList(){
        List<DatabaseReportItem> resultList = new ArrayList();
        DatabaseReportItem item;
        List<String> listBD = ObfuscatorUtils.getXMLFiles();
        XstreamService xsService = new XstreamService();
        
        for(String databaseName : listBD){
            Database database;
            try {
                database = (Database) xsService.loadXmlFile(databaseName+".xml");                
            } catch(Exception ex){
                log.error("No se pudo cargar el archivo XML");
                continue;
            }
            if(database == null) {
                log.error("XStreamService retorno null! No se encontró o no se pudo cargar el xml.");
                continue;
            }
            
            

            List<Environment> environments = database.getEnvironments();
            
            if(environments != null && environments.size() > 0){                
                for(Environment env : environments){
                    item = new DatabaseReportItem();
                    item.setName(database.getName());
                    item.setAppOwner(database.getAppOwner());
                    item.setVendorDataBase(database.getVendorDataBase());
                    item.setVersionDataBase(database.getVersionDataBase());
                    item.setNumberTables(database.getTables().size());
                    item.setEnvironment(env.getName());
                    item.setOverrideSchema(env.getOverrideName());
                    item.setHost(env.getHost());
                    item.setPort(env.getPort());                   
                    item.setServicename(env.getServiceName());               
                    int numberOfColumns= database
                            .getTables()
                            .stream()
                            .collect(Collectors.summingInt(table -> table.getColumns().size()));
                    item.setNumberColumns(numberOfColumns);
                    resultList.add(item);                    
                }
            }else{
                item = new DatabaseReportItem();
                item.setName(database.getName());
                item.setAppOwner(database.getAppOwner());
                item.setVendorDataBase(database.getVendorDataBase());
                item.setVersionDataBase(database.getVersionDataBase());
                item.setNumberTables(database.getTables().size());
               resultList.add(item);                                     
            }
        }
        return resultList;
    }
    /**
     * 
     * @param filename name of the database which will be consulted
     * @return a list with the data for the specific database
     */
    public static List<DatabaseReportItem> getDatabaseReportList(String filename){
        List<DatabaseReportItem> resultList = new ArrayList();
        DatabaseReportItem item;
        List<String> listBD = ObfuscatorUtils.getXMLFiles();
        XstreamService xsService = new XstreamService();
        
        Database database = null;
        if(listBD.contains(filename)){
            try {
                database = (Database) xsService.loadXmlFile(filename+".xml");                
            } catch(Exception ex){
                log.error("No se pudo cargar el archivo XML");
                return null;                
            }            
        }else{
            log.error("No se encontró la Base de datos");
            return null;
        }         

        List<Environment> environments = database.getEnvironments();
            
        if(environments != null && environments.size() > 0){                
            for(Environment env : environments){
                item = new DatabaseReportItem();
                item.setName(database.getName());
                item.setAppOwner(database.getAppOwner());
                item.setVendorDataBase(database.getVendorDataBase());
                item.setVersionDataBase(database.getVersionDataBase());
                item.setNumberTables(database.getTables().size());
                item.setEnvironment(env.getName());
                item.setHost(env.getHost());
                item.setPort(env.getPort());                   
                item.setServicename(env.getServiceName());
                int numberOfColumns= database
                        .getTables()
                        .stream()
                        .collect(Collectors.summingInt(table -> table.getColumns().size()));
                item.setNumberColumns(numberOfColumns);
                resultList.add(item);                    
            }
        }else{
            item = new DatabaseReportItem();
            item.setName(database.getName());
            item.setAppOwner(database.getAppOwner());
            item.setVendorDataBase(database.getVendorDataBase());
            item.setVersionDataBase(database.getVersionDataBase());
            item.setNumberTables(database.getTables().size());
            resultList.add(item);                                     
        }
        
        return resultList;
    }
    /**
     * Get the details for a specific database.
     * @param filename name of the database that would
     * @see DatabaseDetailReportItem
     * @return A list of DatabaseDetailReportItem.
     */
    public static List<DatabaseDetailReportItem> getDatabaseDetailReportList(String filename){
        List<DatabaseDetailReportItem> resultList = new ArrayList();
        DatabaseDetailReportItem item;
        List<String> listBD = ObfuscatorUtils.getXMLFiles();
        XstreamService xsService = new XstreamService();
        
        Database database = null;
        if(listBD.contains(filename)){
            try {
                database = (Database) xsService.loadXmlFile(filename+".xml");                
            } catch(Exception ex){
                log.error("No se pudo cargar el archivo XML");
                return null;                
            }            
        }else{
            log.error("No se encontró la Base de datos");
            return null;
        }         

        List<Table> tables = database.getTables();

        if(tables != null && tables.size() > 0){                
            for(Table table : tables){
                 List<Column> columns = table.getColumns();
                for(Column column : columns){
                    item = new DatabaseDetailReportItem();
                    item.setName(database.getName());
                    item.setAppOwner(database.getAppOwner());
                    item.setVendorDataBase(database.getVendorDataBase());
                    item.setVersionDataBase(database.getVersionDataBase());
                    item.setTableName(table.getName());
                    item.setColumnName(column.getName());
                    item.setMethod(column.getOfuscationMethod().getName());
                    resultList.add(item);   
                }

            }
        }else{
            item = new DatabaseDetailReportItem();
            item.setName(database.getName());
            item.setAppOwner(database.getAppOwner());
            item.setVendorDataBase(database.getVendorDataBase());
            item.setVersionDataBase(database.getVersionDataBase());               
            resultList.add(item);                                     
        }
        
        return resultList;
    }
    
    private int countColumns(Table table){
        return table.getColumns().size();
    }
    
}
