package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Database;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;

import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class MySqlObfuscator extends ObfuscatorManager {

    protected MySqlObfuscator() {
        super();
    }

    protected MySqlObfuscator(Database databaseXML, DatabaseManager dbm, Connection conn, boolean isXmlValid) {
        this.databaseXML = databaseXML;
        this.dbm = dbm;
        this.isXmlValid = isXmlValid;
        this.conn = conn;
    }

    @Override
    public String obfuscateByFixedValue(Table table, List<Column> columns) {
        return super.obfuscateByFixedValue(table, columns);
    }

    @Override
    public String obfuscateBySwitchingCharacters(Table table, List<Column> columns) {
        return super.obfuscateBySwitchingCharacters(table, columns);
    }

}
