package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import java.util.List;

/**
 *
 * @author cd115986
 */
interface Ofuscable {

    public void obfuscate();

    String obfuscateByFixedValue(Table table, List<Column> columns);

    String obfuscateByDictionary(Table table, List<Column> columns);

    String obfuscateBySet(Table table, List<Column> columns);

    String obfuscateBySwitchingRows(Table table, List<Column> columns);

    String obfuscateBySwitchingDigits(Table table, List<Column> columns);

    String obfuscateBySwitchingCharacters(Table table, List<Column> columns);

    String obfuscateByCopingValues(Table table, List<Column> columns);

}
