package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.exceptions.EnvironmentNotFoundException;
import com.itpatagonia.obfuscator.process.exceptions.InvalidColumnDefinitionException;
import com.itpatagonia.obfuscator.process.exceptions.InvalidTableDefinitionException;
import com.itpatagonia.obfuscator.process.exceptions.ObfuscatorException;
import com.itpatagonia.obfuscator.process.model.Credential;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.time.StopWatch;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class GenericObfuscator extends ObfuscatorManager {

    private ObfuscatorManager OM;

    private static boolean isSlowObfuscation = false;

    /**
     *
     * @param xmlFileName
     * @param ambiente
     * @param credential
     * @return
     */
    public int process(String xmlFileName, String ambiente, Credential credential) {
        log.info("{BR}");
        StopWatch timer = new StopWatch();
        isSlowObfuscation = false;
        try {
            loadXML(xmlFileName);

            log.info("INI Obfuscator.process ****************************************");
            timer.start();

            initializeDBManager(ambiente, credential);

            validateXML();

            obfuscate();

        } catch (InvalidTableDefinitionException | InvalidColumnDefinitionException | EnvironmentNotFoundException ex) {
            log.error(ex);
            return ex.getErrorCode();
        } catch (ObfuscatorException oex) {
            log.error("Error code: " + oex.getErrorCode() + "," + oex);
            return oex.getErrorCode();
        } catch (Exception ex) {
            log.error(ex);
            return 100;
        } finally {
            timer.stop();
            log.info("... execution time: " + timer.getTime() + " ms (" + timer.toString() + ")");
            log.info("END Obfuscator.process ****************************************");
            log.info("{BR}");
        }

        return 0;
    }

    /**
     *
     * @param xmlFileName
     * @param ambiente
     * @param credential
     * @return
     */
    public int slowProcess(String xmlFileName, String ambiente, Credential credential) {
        log.info("{HR}");
        StopWatch timer = new StopWatch();
        isSlowObfuscation = true;
        try {
            loadXML(xmlFileName);

            log.info("INI Obfuscator.slowProcess ****************************************");
            timer.start();

            initializeDBManager(ambiente, credential);

            validateXML();

            obfuscate();

        } catch (InvalidTableDefinitionException | InvalidColumnDefinitionException | EnvironmentNotFoundException ex) {
            log.error(ex);
            return ex.getErrorCode();
        } catch (ObfuscatorException oex) {
            log.error("Error code: " + oex.getErrorCode() + "," + oex);
            return oex.getErrorCode();
        } catch (Exception ex) {
            log.error(ex);
            return 100;
        } finally {
            timer.stop();
            log.info("... execution time: " + timer.getTime() + " ms (" + timer.toString() + ")");
            log.info("END Obfuscator.slowProcess ****************************************");
            log.info("{HR}");
        }

        return 0;
    }

    @Override
    public void obfuscate() {
        if (OM == null) {
            //NOTE  should this check if it is already setted and/or if it has changed before instance a new one?
            OM = getInstanceForDatabase();
        }
        OM.obfuscate();
    }

    private ObfuscatorManager getInstanceForDatabase() {
        switch (this.dbm.database) {
            case ORACLE11g:
            case ORACLE12c:
            case ORACLE18c:
                if (isSlowObfuscation) {
                    return new OracleSlowObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                } else {
                    return new OracleObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                }
            case DB2:
                if (isSlowObfuscation) {
                    return new DB2SlowObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                } else {
                    return new DB2Obfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                }
            case DB2_400:
                if (isSlowObfuscation) {
                    return new DB2400SlowObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                } else {
                    return new DB2400Obfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                }
            case SQLSERVER2008:
            case SQLSERVER2012:
            case SQLSERVER2014:
            case SQLSERVER2016:
            case SQLSERVER2017:
                if (isSlowObfuscation) {
                    return new SQLServerSlowObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                } else {
                    return new SQLServerObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                }
            case MYSQL:
                if (isSlowObfuscation) {
                    return new MySqlSlowObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                } else {
                    return new MySqlObfuscator(this.databaseXML, this.dbm, this.conn, this.isXmlValid);
                }
            default:
                throw new UnsupportedOperationException("Not supported yet - " + this.dbm.database);
        }
    }

}
