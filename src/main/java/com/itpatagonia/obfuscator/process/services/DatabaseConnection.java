package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.process.model.Credential;
import java.sql.Connection;

/**
 *
 * @author Gortiz
 */
public interface DatabaseConnection {


    public String getDriverClass();
    
    public String buildUrlDatabaseConnection();
        
    public Connection getConnection(Credential credential);
        
}
