package com.itpatagonia.obfuscator.process.services;

import com.itpatagonia.obfuscator.configuration.xml.model.Column;
import com.itpatagonia.obfuscator.configuration.xml.model.Table;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Gortiz
 */
public interface DatabaseMetadata {


    public Map<Table, Boolean> checkTablesNames(Connection conn, List<Table> xmlTables);
    
    public List<String> getTablesNames(Connection conn);


    public Map<Column, Boolean> checkColumnsNames(Connection conn, Table table);
        
    public List<String> getColumnsNames(Connection conn, String tablename);

    
    public Map<Column, Boolean> checkPrimaryKeys(Connection conn, Table table);
    
    public List<String> getPrimaryKeys(Connection conn, String tablename);

    
    public Map<Column, Boolean> checkImportedKeys(Connection conn, Table table);
    
    public List<String> getImportedKeys(Connection conn, String tablename);

    
    
}
