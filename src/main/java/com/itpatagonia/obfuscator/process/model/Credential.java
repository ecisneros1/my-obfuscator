package com.itpatagonia.obfuscator.process.model;

/**
 *
 * @author Gortiz
 */
public class Credential {
    
    
    private String user;
    
    private String pwd;

    /**
     * 
     * @param user
     * @param pwd 
     */
    public Credential(String user, String pwd) {
        this.user = user;
        this.pwd = pwd;
    }
    
    
    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
}
