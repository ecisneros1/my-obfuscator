package com.itpatagonia.obfuscator.process.model;

import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Gortiz
 */
public enum DatabasesEnum {

    ORACLE10g("oracle", "10g"),
    ORACLE11g("oracle", "11g"),
    ORACLE12c("oracle", "12c"),
    ORACLE18c("oracle", "18c"),

    DB2("DB2", "10"),

    DB2_400("DB2/400", "AS400"), // DB2 iSeries/AS 400
    
    SQLSERVER2008("sqlserver", "10"),
    SQLSERVER2012("sqlserver", "11"),
    SQLSERVER2014("sqlserver", "12"),
    SQLSERVER2016("sqlserver", "13"),
    SQLSERVER2017("sqlserver", "14"),

    MYSQL("mysql", "8.0.30"),
    
    ;


    public String database;
    public String version;

    private static Map<String, String> vendors;


    private DatabasesEnum(String database, String version) {
        this.database = database;
        this.version = version;
    }

    public static DatabasesEnum getDatabaseEnum(String vendor, String version) {
        for (DatabasesEnum database : DatabasesEnum.values()) {
            if (database.database.equals(vendor) && database.version.equals(version)) {
                return database;
            }
        }
        return null;
    }

    public String getDriverClass() {
        switch (this) {
            case ORACLE10g:
            case ORACLE11g:
            case ORACLE12c:
            case ORACLE18c:
                return "oracle.jdbc.driver.OracleDriver";
            case DB2:
                return "com.ibm.db2.jcc.DB2Driver";
            case DB2_400:
                return "com.ibm.as400.access.AS400JDBCDriver";
            case SQLSERVER2008:
            case SQLSERVER2012:
            case SQLSERVER2014:
            case SQLSERVER2016:
            case SQLSERVER2017:
                return "net.sourceforge.jtds.jdbc.Driver";
                   //return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
            case MYSQL:
                return "com.mysql.cj.jdbc.Driver";
            }
        return null;
    }

    public static Map<String, String> getVendors() {
        if (vendors == null) {
            vendors = new TreeMap<>();
            for (DatabasesEnum database : DatabasesEnum.values()) {
                vendors.put(database.database + "-" + database.version, database.database + " - " + database.version);
            }
        }
        return vendors;
    }

    public boolean isOracleDatabase() {
        return this.database.equalsIgnoreCase("oracle");
    }

    public boolean isDB2Database() {
        return this.database.equalsIgnoreCase("db2");
    }

    public boolean isDB2400Database() {
        return this.database.equalsIgnoreCase("db2/400");
    }

    public boolean isSqlServerDatabase() {
        return this.database.equalsIgnoreCase("sqlserver");
    }

    public boolean isMySqlDatabase() {
        return this.database.equalsIgnoreCase("mysql");
    }

}
