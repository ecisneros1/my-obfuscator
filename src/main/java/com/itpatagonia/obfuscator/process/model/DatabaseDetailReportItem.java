package com.itpatagonia.obfuscator.process.model;

/**
 *
 * @author cd115986
 */
public class DatabaseDetailReportItem {
    
    private String name;
    private String appOwner;
    private String vendorDataBase;
    private String versionDataBase;
    private String  tableName;
    private String  columnName;
    private String  method;
    
    

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the appOwner
     */
    public String getAppOwner() {
        return appOwner;
    }

    /**
     * @param appOwner the appOwner to set
     */
    public void setAppOwner(String appOwner) {
        this.appOwner = appOwner;
    }

    /**
     * @return the vendorDataBase
     */
    public String getVendorDataBase() {
        return vendorDataBase;
    }

    /**
     * @param vendorDataBase the vendorDataBase to set
     */
    public void setVendorDataBase(String vendorDataBase) {
        this.vendorDataBase = vendorDataBase;
    }

    /**
     * @return the versionDataBase
     */
    public String getVersionDataBase() {
        return versionDataBase;
    }

    /**
     * @param versionDataBase the versionDataBase to set
     */
    public void setVersionDataBase(String versionDataBase) {
        this.versionDataBase = versionDataBase;
    }

    /**
     * @return the tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * @return the columnName
     */
    public String getColumnName() {
        return columnName;
    }

    /**
     * @param columnName the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @param method the method to set
     */
    public void setMethod(String method) {
        this.method = method;
    }

   
    
    
}
