package com.itpatagonia.obfuscator.process.model;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author cd115986
 */
@Getter @Setter
public class DatabaseReportItem {
    private String name;
    private String appOwner;
    private String vendorDataBase;
    private String versionDataBase;
    private String  environment;
    private String overrideSchema;
    private String  host;
    private String  port;
    private String  servicename;
    private int numberTables;
    private int numberColumns;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the appOwner
     */
    public String getAppOwner() {
        return appOwner;
    }

    /**
     * @param appOwner the appOwner to set
     */
    public void setAppOwner(String appOwner) {
        this.appOwner = appOwner;
    }

    /**
     * @return the vendorDataBase
     */
    public String getVendorDataBase() {
        return vendorDataBase;
    }

    /**
     * @param vendorDataBase the vendorDataBase to set
     */
    public void setVendorDataBase(String vendorDataBase) {
        this.vendorDataBase = vendorDataBase;
    }

    /**
     * @return the versionDataBase
     */
    public String getVersionDataBase() {
        return versionDataBase;
    }

    /**
     * @param versionDataBase the versionDataBase to set
     */
    public void setVersionDataBase(String versionDataBase) {
        this.versionDataBase = versionDataBase;
    }

    /**
     * @return the environment
     */
    public String getEnvironment() {
        return environment;
    }

    /**
     * @param environment the environment to set
     */
    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @param host the host to set
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return the port
     */
    public String getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    public void setPort(String port) {
        this.port = port;
    }

    /**
     * @return the servicename
     */
    public String getServicename() {
        return servicename;
    }

    /**
     * @param servicename the servicename to set
     */
    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    /**
     * @return the numberTables
     */
    public int getNumberTables() {
        return numberTables;
    }

    /**
     * @param numberTables the numberTables to set
     */
    public void setNumberTables(int numberTables) {
        this.numberTables = numberTables;
    }

    /**
     * @return the numberColumns
     */
    public int getNumberColumns() {
        return numberColumns;
    }

    /**
     * @param numberColumns the numberColumns to set
     */
    public void setNumberColumns(int numberColumns) {
        this.numberColumns = numberColumns;
    }
    
    
}
