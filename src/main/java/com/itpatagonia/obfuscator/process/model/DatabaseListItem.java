package com.itpatagonia.obfuscator.process.model;

/**
 *
 * @author AR
 */
public class DatabaseListItem {

    /**
     * @return the databaseName
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * @param databaseName the databaseName to set
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * @return the appOwner
     */
    public String getAppOwner() {
        return appOwner;
    }

    /**
     * @param appOwner the appOwner to set
     */
    public void setAppOwner(String appOwner) {
        this.appOwner = appOwner;
    }

    public DatabaseListItem(String databaseName, String appOwner) {
        this.databaseName = databaseName;
        this.appOwner = appOwner;
    }

    public DatabaseListItem() {
    }
    
    
    private String databaseName;
    private String appOwner;
    
    
}
