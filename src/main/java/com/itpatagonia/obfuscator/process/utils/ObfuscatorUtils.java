package com.itpatagonia.obfuscator.process.utils;

import com.itpatagonia.obfuscator.configuration.AppContext;
import java.io.File;
import java.io.FilenameFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author cd115986
 */
@Log4j2
public class ObfuscatorUtils {

    public enum FileOperation {
        MODIFIED("modified"),
        DELETED("deleted");

        String operation;

        FileOperation(String operation) {
            this.operation = operation;
        }
    }

    /**
     * Search for all the logs defined on the logs directory.
     *
     * @return List of all log existed on ../logs/
     */
    public static List<String> getLogsFiles() {
        File directory = new File(AppContext.mainConfig.getLogsDir());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File[] logFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".log");
            }
        });
        // order files by date modified descendent
        Arrays.sort(logFiles, Comparator.comparingLong(File::lastModified).reversed());
        List<String> lista = extractFileNames(logFiles);
        return lista;
    }

    /**
     * Search for logs defined on the logs directory.
     *
     * @param includePattern
     * @param excludePattern
     * @return List of all log existed on ../logs/
     */
    public static List<String> getLogsFiles(String includePattern, String excludePattern) {
        File directory = new File(AppContext.mainConfig.getLogsDir());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File[] logFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                boolean include = (StringUtils.isNotEmpty(includePattern) ? name.toLowerCase().contains(includePattern) : true);
                boolean exclude = (StringUtils.isNotEmpty(excludePattern) && name.toLowerCase().contains(excludePattern));
                boolean isLogFile = name.toLowerCase().endsWith(".log");
                return isLogFile && include && !exclude;
            }
        });
        // order files by date modified descendent
        Arrays.sort(logFiles, Comparator.comparingLong(File::lastModified).reversed());
        List<String> lista = extractFileNames(logFiles);
        return lista;
    }

    private static List<String> extractFileNames(File[] logFiles) {
        List<String> lista = new ArrayList();
        for (File file : logFiles) {
            if (!file.isDirectory()) {
                lista.add(file.getName().replace(".log", " ").trim());
            }
        }
        return lista;
    }

    /**
     * Search for all the xml defined on the xml directory.
     *
     * @return List of all xml existed on ../xml/database
     */
    public static List<String> getXMLFiles() {
        log.debug("On GetXMLFILES()");
        File directory = new File(AppContext.mainConfig.getXmlDir());
        if (!directory.exists()) {
            directory.mkdirs();
        }
        File[] xmlFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".xml");
            }
        });
        List<String> lista = new ArrayList();
        log.debug("Lista de base de datos existentes: ");
        for (File file : xmlFiles) {
            log.debug("Database: " + file.getName().replaceAll(".xml", " "));
            if (!file.isDirectory()) {
                lista.add(file.getName().replace(".xml", " ").trim());
            }
        }
        return lista;
    }

    /**
     * Search for all the xml defined on the xml directory and return just the
     * ones that contains the filter value.
     *
     * @return List of all xml existed on ../xml/database
     */
    public static List<String> getXMLFiles(String filter) {
        log.debug("On GetXMLFILES(" + filter + ")");
        File directory = new File(AppContext.mainConfig.getXmlDir());

        File[] xmlFiles = directory.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".xml");
            }
        });
        List<String> lista = new ArrayList();
        for (File file : xmlFiles) {
            log.debug("File: " + file.getName().replaceAll(".xml", " "));
            if (!file.isDirectory()) {
                if (file.getName().contains(filter)) {
                    lista.add(file.getName().replace(".xml", " ").trim());
                }
            }
        }
        return lista;
    }

    /**
     * Moves files to a history directory and rename them with the date and
     * reason of the operation (status).
     *
     * @param filename name of the file that would be renamed.
     * @param status reason of the file renamed (MODIFIED or DELETED).
     * @return true if was successful or false if occurs an exception.
     */
    public static boolean operateFile(String filename, FileOperation status) {
        boolean result = false;
        log.debug("on operateFile()- filename" + filename);
        File historyDir = null;
        try {
            historyDir = new File(AppContext.mainConfig.getXmlHistoryDir());            
            log.info("..Se INICIO la operacion: " + status + " sobre el archivo " + filename);
            logFileSystemInformation(historyDir);
            
            if (!historyDir.exists()) {
                boolean resultCreation = historyDir.mkdirs();
                log.info("Se creo el directorio historico: " + resultCreation);
            }

            File oldFile = new File(AppContext.mainConfig.getXmlDir(), filename);
            DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd HH_mm_ss");
            Date date = new Date();

            String nowDate = dateFormat.format(date);
            File renamedFile = new File(historyDir, filename + nowDate + "." + status.name());
            result = oldFile.renameTo(renamedFile);
            if(result){
                log.info("..Se REALIZO la operacion: " + status + " sobre el archivo " + filename );
            }
            else {
                log.info("..NO SE REALIZO la operacion: " + status + " sobre el archivo " + filename );
            }
            return result;

        } catch (SecurityException e) {
            log.error("..Permisos insuficientes para renombrar o mover el archivo: " + filename);
            log.error("..No se pudo renombrar o mover el archivo debido a permisos insuficientes, expection: " + e.getMessage());
            return result;
        } catch (Exception e) {
            log.error("..No se pudo renombrar o mover el archivo: " + filename);
            log.error("..No se pudo renombrar o mover el archivo debido a la expection: " + e.getMessage());
            return result;
        }
    }

    private static void logFileSystemInformation(File file){
        if (file != null && file.isDirectory()) {
            log.debug("...HistoryDir path: " + file.getPath());
            log.debug("...Es historyDir un directorio: " + file.isDirectory());
            log.debug("...Permiso Escritura en history dir: " + file.canWrite());
            log.debug("...Permiso Lectura en history dir: " + file.canRead());
            log.debug("...Permiso Execution en history dir: " + file.canExecute());
            log.debug("...Free space: " + file.getFreeSpace() + " of " + file.getTotalSpace());
            log.debug("...Usable space: " + file.getUsableSpace() + " of " + file.getTotalSpace());
        } else {
            log.info("...No se pudo imprimir la informacion del directorio: El file es null o no es un directorio.");
        }
    }

    /**
     * Generate SQL with REPLACE where replace a value with it consecutive value
     * on the array (on pairs) = {value, ReplacementOfValue, value2,
     * ReplacementOfValue2}
     *
     * @param value
     * @param characters
     * @return
     */
    public static String generateRecursiveReplace(String value, char[] characters) {
        String result = value;
        int index = 0;
        if (characters.length - 1 < index || characters.length % 2 != 0) {
            return result;
        } else {
            result = "REPLACE(" + result + ",'" + characters[index++] + "','" + characters[index++] + "')";
            return generateRecursiveReplace(result, characters, index);
        }
    }
    
    private static String generateRecursiveReplace(String value, char[] characters, int index) {
        String result = value;
        if (characters.length - 1 < index) {
            return result;
        } else {
            result = "REPLACE(" + result + ",'" + characters[index++] + "','" + characters[index++] + "')";
            return generateRecursiveReplace(result, characters, index);
        }
    }

}
