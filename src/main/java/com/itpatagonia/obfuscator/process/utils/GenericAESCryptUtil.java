package com.itpatagonia.obfuscator.process.utils;

import java.security.Key;

import javax.crypto.SecretKey;
import javax.security.auth.DestroyFailedException;

import ar.com.wd.WDCrypt;
import lombok.extern.log4j.Log4j2;
/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2 
public class GenericAESCryptUtil {

    private static String seed = "HGBR55AA";   

	public static final String GENERIC_PROVIDER_CLASS_NAME = "org.bouncycastle.jce.provider.BouncyCastleProvider";
	public static final String IBM_PROVIDER_CLASS_NAME = "com.ibm.crypto.provider.IBMJCE";

	public static String PROVIDER_CLASS_NAME;

	static {
		try {
			PROVIDER_CLASS_NAME = Class.forName(IBM_PROVIDER_CLASS_NAME).getName();
		} catch (ClassNotFoundException e) {
			try {
				PROVIDER_CLASS_NAME = Class.forName(GENERIC_PROVIDER_CLASS_NAME).getName();
			} catch (ClassNotFoundException e1) {
				log.error("No se encontró ningún crypto provider");
			}
		}
	}

	private static String getSeed() {
		return (seed + seed.toLowerCase());
		//return (seed);
	}

	public final static String crypt(String data) {
		WDCrypt wdCrypt = new WDCrypt();
		wdCrypt.algoritmo = "AES";
		wdCrypt.key = getSeed();
		wdCrypt.providerClass = PROVIDER_CLASS_NAME;
		wdCrypt.initKey();
		wdCrypt.theKey = new SecretKeyWrapper(wdCrypt.theKey);
		String result = wdCrypt.encrypt(data);
		return (result);
	}

	private static class SecretKeyWrapper implements SecretKey {

		private static final long serialVersionUID = -2735489868874051226L;

		private Key innerKey;

		SecretKeyWrapper(Key key) {
			this.innerKey = key;
		}

		public void destroy() throws DestroyFailedException {
			innerKey = null;
		}

		public boolean isDestroyed() {
			return innerKey == null;
		}

		@Override
		public boolean equals(Object o) {
			return innerKey.equals(o);
		}

		@Override
		public int hashCode() {
			return innerKey.hashCode();
		}

		@Override
		public String getAlgorithm() {
			return innerKey.getAlgorithm();
		}

		@Override
		public String getFormat() {
			return innerKey.getFormat();
		}

		@Override
		public byte[] getEncoded() {
			return innerKey.getEncoded();
		}

	}

	public final static String decrypt(String data) {
		if (data.startsWith("{AES}")) {
			WDCrypt wdCrypt = new WDCrypt();
			wdCrypt.algoritmo = "AES";
			wdCrypt.key = getSeed();
			wdCrypt.providerClass = PROVIDER_CLASS_NAME;
			wdCrypt.initKey();
			wdCrypt.theKey = new SecretKeyWrapper(wdCrypt.theKey);
			String result = wdCrypt.decrypt(data.substring(5));
			return (result);
		}
		return (data);
	}
	public final static char[] decrypt(char[] data) {
            char[] decryptedChars = decrypt(new String(data)).toCharArray();
            return decryptedChars;
	}
	public final static char[] crypt(char[] data) {
            char[] cryptedChars = crypt(new String(data)).toCharArray();
            return cryptedChars;
	}

}
