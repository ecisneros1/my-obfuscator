
package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class MSCCertificateException extends ObfuscatorException {

    public MSCCertificateException() {
        super(7, "MSCException - Verifique el certificado y las claves definidas. JCE debe estar instalado en el servidor.");
    }
    public MSCCertificateException(int errorCode, String message) {
        super(errorCode, message);
    }

}
