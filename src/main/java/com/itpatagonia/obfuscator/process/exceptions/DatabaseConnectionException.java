package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author cd115986
 */
public class DatabaseConnectionException extends ObfuscatorException {
    
    public DatabaseConnectionException(int errorCode, String message) {
        super(errorCode, message);
    }
    public DatabaseConnectionException() {
        super(4, "No se pudo conectar con la base de datos.");
    }
    
}
