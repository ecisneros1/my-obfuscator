package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author cd115986
 */
public class InvalidTableDefinitionException extends ObfuscatorException{
    
    private String tableName;
      
    public InvalidTableDefinitionException(int errorCode, String message, String tableName) {
        super(errorCode, message);
        this.tableName = tableName;
    }
    public InvalidTableDefinitionException(String tableName) {
        super(2, "Se ha definido una Tabla que no existe en la base de datos: " + tableName);
        this.tableName = tableName;
    }

    public InvalidTableDefinitionException() {
        super(2, "Se ha definido una Tabla que no existe en la base de datos.");
    }

    /**
     * @return the tableName
     */
    public String getTableName() {
        return tableName;
    }
    
    
}
