package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author cd115986
 */
public class CheckMetadataException extends ObfuscatorException {

    public CheckMetadataException() {
        super(6, "No se pudo comprobar la metadata.");
    }
    
}
