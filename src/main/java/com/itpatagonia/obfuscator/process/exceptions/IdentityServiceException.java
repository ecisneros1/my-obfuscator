
package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class IdentityServiceException extends ObfuscatorException{

    public IdentityServiceException() {
        super(9,"No se pudo instanciar el Identity provider u ocurrió un error al inicializarlo..");
    }
    
    public IdentityServiceException(int errorCode, String message) {
        super(errorCode, message);
    }
    
}
