package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author Gortiz
 */
public class InvalidCredentialException extends ObfuscatorException {
    
    
    public InvalidCredentialException(int errorCode, String message) {
        super(errorCode, message);
    }

    public InvalidCredentialException() {
        super(8, "No se pudo obtener la credencial o es inválida.");
    }
    
}
