
package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class NoLoadedXmlException extends ObfuscatorException {

    public NoLoadedXmlException() {
        super(11, "No se ha cargado el archivo XML.");
    }

}
