package com.itpatagonia.obfuscator.process.exceptions;

/**
 * Invalid Column Def Exception.
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class InvalidColumnDefinitionException extends ObfuscatorException {
    
    private String columnName;    
    
    public InvalidColumnDefinitionException(int errorCode, String message, String columnName) {
        super(errorCode, message);
        this.columnName = columnName;
    }

    public InvalidColumnDefinitionException(String columnName) {
        super(3, "Se ha definido una columna que no existe en la base de datos: " + columnName);
        this.columnName = columnName;
    }

    public InvalidColumnDefinitionException() {
        super(3, "Se ha definido una columna que no existe en la base de datos.");
    }
    
}
