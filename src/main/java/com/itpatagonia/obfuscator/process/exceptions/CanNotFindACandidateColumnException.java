package com.itpatagonia.obfuscator.process.exceptions;

/**
 * Invalid Column Def Exception.
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class CanNotFindACandidateColumnException extends ObfuscatorException {
    
    private String tableName;    
    
    public CanNotFindACandidateColumnException(int errorCode, String message, String tableName) {
        super(errorCode, message);
        this.tableName = tableName;
    }

    public CanNotFindACandidateColumnException(String tableName) {
        super(3, "No se ha podido encontrar una columna candidata para el procesamiento en lotes de la tabla: "+ tableName);
        this.tableName = tableName;
    }
    public CanNotFindACandidateColumnException() {
        super(3, "No se ha podido encontrar una columna candidata para el procesamiento en lotes de la tabla.");
    }
    
    
}
