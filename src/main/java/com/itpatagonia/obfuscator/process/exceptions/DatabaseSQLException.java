package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class DatabaseSQLException extends ObfuscatorException {
    
    public DatabaseSQLException(int errorCode, String message) {
        super(errorCode, message);
    }
    public DatabaseSQLException() {
        super(10, "No se pudo ejecutar el batch de queries.");
    }
    
}
