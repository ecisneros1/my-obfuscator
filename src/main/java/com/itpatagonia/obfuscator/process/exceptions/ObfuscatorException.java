package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author cd115986
 */
public abstract class ObfuscatorException extends RuntimeException {

    private int errorCode;

    public ObfuscatorException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * @return the errorCode
     */
    public int getErrorCode() {
        return errorCode;
    }

}
