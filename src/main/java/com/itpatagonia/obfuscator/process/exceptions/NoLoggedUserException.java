package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
public class NoLoggedUserException extends ObfuscatorException {
    
    public NoLoggedUserException(int errorCode, String message) {
        super(errorCode, message);
    }

    public NoLoggedUserException() {
        super(5, "El usuario no se ha logueado correctamente.");
    }
    
    
}
