package com.itpatagonia.obfuscator.process.exceptions;

/**
 *
 * @author cd115986
 */
public class EnvironmentNotFoundException extends ObfuscatorException {
    
    public EnvironmentNotFoundException(int errorCode, String message) {
        super(errorCode, message);
    }

    public EnvironmentNotFoundException() {
        super(1,"No se ha encontrado el Ambiente solicitado en el XML a Obfuscar.");
    }
    
}
