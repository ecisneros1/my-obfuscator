

import lombok.extern.log4j.Log4j2;

/*
 *

/**
 *
 * @author Angel Rodrigues <arodrigues@itpatagonia.com>
 */
@Log4j2
public class test {
    public static void main(String[] args){
        MiTest obj1 = new MiTest();
        final MiTest obj2 = new MiTest();
        
        testingFinalParameter(obj1);
        testingFinalParameter(obj2);
        System.out.println("After method");
        System.out.println(obj1);
        System.out.println(obj2);
    }
    
    public static void testingFinalParameter(final MiTest obj){
        System.out.println("Final as parameter, start value: ");
        System.out.println(obj);
        obj.setHola("chao");
        obj.setMundo("world");
        obj.setNumero(3);
        obj.setB('c');
        System.out.println("Final value: ");
        System.out.println(obj);
        System.out.println("\n");
    }   
   
}

class MiTest {
    
    String hola;
    String mundo;
    
    int numero;
    char b;

    public MiTest() {
        this.hola = "";
        this.mundo = "";
        this.numero = 1;
        this.b = 'a';
    }

    public MiTest(String hola, String mundo, int numero, char b) {
        this.hola = hola;
        this.mundo = mundo;
        this.numero = numero;
        this.b = b;
        
    }

    public String getHola() {
        return hola;
    }

    public void setHola(String hola) {
        this.hola = hola;
    }

    public String getMundo() {
        return mundo;
    }

    public void setMundo(String mundo) {
        this.mundo = mundo;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public char getB() {
        return b;
    }

    public void setB(char b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "MiTest{" + "hola=" + hola + ", mundo=" + mundo + ", numero=" + numero + ", b=" + b + '}';
    }
    
    
    
}