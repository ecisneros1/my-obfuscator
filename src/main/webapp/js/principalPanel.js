/**
 * 
 *  Document   : navbar
 *  Created on : Aug 13, 2018, 11:54:04 AM
 *  Author     : arodrigues
 * 
 */
//Ajax loading animation:

$(document).on({
    ajaxStart: function() { $(".ajaxBackground").addClass("loading"); $(".ajaxLoadingAnimation").css("display","inline"); },
    ajaxStop: function() { $(".ajaxBackground").removeClass("loading"); $(".ajaxLoadingAnimation").css("display","none"); }    
});

//Globals
var databaseObject = {};
var envEditIndex = -1;
var tableEditIndex = -1;
var columnEditIndex = -1;
var activeDB;
var isDataModified= false;
var databaseList = [];
var connectionTested = false;
var tablesImported = {};
//Functions
var ableToEdit= false;
var ableToReport= false;
var ableToAudit= false;
var ableToObfuscate= false;
PNotify.prototype.options.styling = "fontawesome";
var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};

$(document).ready(function(){  
    checkFunctions();
    $('[data-toggle="tooltip"]').tooltip();
    // VALIDATIONS -----------------------------------------------------------
    // VALIDATIONS -----------------------------------------------------------
    $("#inputDatabaseSearch").on({
        keyup: function(e) {           
          filterDatabaseList($("#inputDatabaseSearch").val());
        }
    });
    $("input.no-specialcharacter-allowed").on({
        keydown: function(e) {
          this.value = this.value.replace(/[!@#\$%\^\&\*\)\(\+\=\]<>\[¿?\|\¬~¡"*]/g, "");
          this.value = this.value.replace(/\s*\n*/g, "");
          //Si intenta colocar un espacio return;
          if (e.which === 32)
            return false;
        },
        focus: function (){
            this.value = this.value.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            this.value = this.value.replace(/\s*\n*/g, "");
        },
        blur: function (){
            this.value = this.value.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            this.value = this.value.replace(/\s*\n*/g, "");
        },
        change: function() {
          this.value = this.value.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
          this.value = this.value.replace(/\s*\n*/g, "");
        },
        paste: function(event){            
            let paste = (event.originalEvent.clipboardData || window.clipboardData).getData('text');            
            paste = paste.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>\.¿?¡"\|\¬~]*/g, "");
            paste = paste.replace(/\s*\n*/g, "");            
            event.originalEvent.clipboardData.text = paste;            
            event.target.value += paste;
            return false;
        }
    });
    $("input.only-some-specialcharacter-allowed").on({
        keydown: function(e) {
          this.value = this.value.replace(/[!#\$%\^\&\*\)\(\+\=\]<>\[¿?\|\¬~¡"*]/g, "");
          this.value = this.value.replace(/\s*\n*/g, "");
          //Si intenta colocar un espacio return;
          if (e.which === 32)
            return false;
        },
        focus: function (){
            this.value = this.value.replace(/[!#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            this.value = this.value.replace(/\s*\n*/g, "");
        },
        blur: function (){
            this.value = this.value.replace(/[!#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            this.value = this.value.replace(/\s*\n*/g, "");
        },
        change: function() {
          this.value = this.value.replace(/[!#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
          this.value = this.value.replace(/\s*\n*/g, "");
        },
        paste: function(event){            
            let paste = (event.originalEvent.clipboardData || window.clipboardData).getData('text');            
            paste = paste.replace(/[!#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            paste = paste.replace(/\s*\n*/g, "");            
            event.originalEvent.clipboardData.text = paste;            
            event.target.value += paste;
            return false;
        }
    });
    $(document).on("keydown",".requiredInput", function(){        
        $(this).removeClass("requiredInput");
    });
    //Some data was edited!
    $(".data-modified-control").on("change", function(){
        isDataModified = true;          
        
    });
    $("#btnRefreshDatabases").on("click",function(){
       getDatabaseList(); 
    });
    
    //Database Name Changed 
    $("#inputDatabaseName").on("change", function(){        
        $("#labelDatabaseOnForm").text("Editando: "+ $(this).val());
        var chipBinded = $(this).attr("data-bindedChip");        
        $("#"+chipBinded + " .chipText").text($(this).val());
    });
    
    // ADD BUTTONS -----------------------------------------------------------
    // ADD BUTTONS -----------------------------------------------------------
    
    $("#btnAddDatabase").click(function(){
        var canCreateNewDB = isDataModified ? confirm("Ha modificado la información de la BD, ¿Desea crear una nueva? Perderá la información que ha modificado") : true;
        if(canCreateNewDB){
           createNewDB();           
        }        
    });
    
    $("#btnAddEnviroment").click(function(){
        envEditIndex = -1;
        var inputValue = $("#inputEnviromentName").val();
        $("#inputEnviromentNameEdit").val(inputValue);
        $("#inputOverrideNameEdit").val("");
        $("#host").val("");
        $("#port").val("");
        $("#serviceName").val("");
        $("#mscID").val(ID_MSC_DEFAULT);
        if(inputValue){
            var isCreated = $("#enviromentListContainer").find("#"+inputValue);   
            if(isCreated.length > 0){
                customConfirm({'message':"Ya existe un ambiente con ese nombre."});
            }else{
                $("#enviromentModalLabel").text("Ambiente: "+ $("#inputEnviromentName").val());
                $('#environmentModal').modal('show');
            }
        }        
        
    });
    
    $("#btnAddTable").click(function(e){
        e.preventDefault();
        if($(".database-chip").is(".active")) $("#tableModal").modal("show");
    });
    
    $("#btnAddColumn").click(function(e){
        e.preventDefault();
        if($(".database-chip").is(".active")){
            if($(".table-chip").is(".active")){
                showParamsInputs($("#selectObfuscationMethod").val());
                $("#columnModal").modal("show");            
            }else{
               customConfirm({'message':"Debe seleccionar una tabla primero para agregar una nueva columna."});                       
            }
        }        
    });    
    
    // CHIPS EVENTS  -------------------------------------------------------
    // CHIPS EVENTS  -------------------------------------------------------
    
    //General chips activation/deactivation
    $(document).on("click",".chip-fluid, .chip ",function(){  
        if($(this).hasClass("disabled")) return;
        if(isDataModified && $(this).hasClass("database-chip") ) return;
        var isAlreadyActive = $(this).hasClass("active");
        var isNotMultiselectList = $(this).hasClass("multiselect");
        if(!isAlreadyActive && !isNotMultiselectList){            
            $(this).parent().children().each(function(index, element){
                $(this).removeClass("active");
            }); 
        }
        $(this).toggleClass("active");
    });
    
    //Chips Controls --------------------------------------------
    
    //delete Database
    $(document).on("click",".database-chip > .controls-container > .btnClose", function(e){
        e.stopPropagation();
        var thisObject = $(this);
        var options = {
            'message':'¿Deseas eliminar esta Base de datos? No se podrá recuperar.' ,
            'yesButton': function(){                
                var databaseId= thisObject.parent().parent()[0].id;
                console.log("Deleting database: "+databaseId);
                deleteDatabase(databaseId);
            }
        };
        customConfirm(options);
    });
    //delete environment
    $(document).on("click","#enviromentListContainer > .chip > .controls-container > .btnClose", function(e){
        e.stopPropagation();
        var thisObject = $(this);
        var options = {
            'message':'¿Deseas eliminar este Ambiente? No se podrá recuperar una vez guardado.' ,
            'yesButton': function(){
                //var i= $(this).parent().parent()[0].dataset.index;
                var i= thisObject.parent().parent()[0].dataset.index;                
                databaseObject.database.environments.splice(i, 1);
                updateEnvironments();
                //Clean de columns chips.
                 $("#columnsListContainer").empty();
            }
        };
        customConfirm(options);
    });
    //delete table
    $(document).on("click",".table-chip > .controls-container > .btnClose", function(e){
        e.stopPropagation();
        var thisObject = $(this);
        var options = {
            'message':'¿Deseas eliminar esta tabla? No se podrá recuperar una vez guardado.' ,
            'yesButton': function(){
                //var i= $(this).parent().parent()[0].dataset.index;
                var i= thisObject.parent().parent()[0].dataset.index;
                console.log("deleting table");
                databaseObject.database.tables.splice(i, 1);
                updateTables();
                //Clean de columns chips.
                $("#columnsListContainer").empty();
            }
        };
        customConfirm(options);
    });
    //edit table
    $(document).on("click",".table-chip > .controls-container > .btnEdit", function(e){
        e.stopPropagation();
        tableEditIndex = $(this).parent().parent()[0].dataset.index;
        $("#inputTableName").val(databaseObject.database.tables[tableEditIndex].name);
        $("#inputBlockSize").val(databaseObject.database.tables[tableEditIndex].blockSize);
        $("#tableModal").modal('show');
        
    });
    //delete column
    $(document).on("click"," #columnsListContainer > .chip-fluid > .controls-container > .btnClose", function(e){
        e.stopPropagation();
        var thisObject = $(this);
        var options = {
            'message':'¿Deseas eliminar esta Columna? No se podrá recuperar una vez guardado.' ,
            'yesButton': function(){
                var iTable = thisObject.parent().parent()[0].dataset.indexTable;
                var i = thisObject.parent().parent()[0].dataset.index;
                console.log("deleting column");
                databaseObject.database.tables[iTable].columns.splice(i, 1);
                updateColumns(iTable); 
                columnEditIndex = -1;
            }
        };
        customConfirm(options);
    });
     //edit column
    $(document).on("click","#columnsListContainer > .chip-fluid > .controls-container > .btnEdit", function(e){
        e.stopPropagation();
        
        tableEditIndex = $(this).parent().parent()[0].dataset.indexTable;
        columnEditIndex = $(this).parent().parent()[0].dataset.index;        
        var columnObject = databaseObject.database.tables[tableEditIndex].columns[columnEditIndex];
        $("#inputColumnName").val(columnObject.name);
        $("#inputColumnDataType").val(columnObject.dataType);
        $("#selectObfuscationMethod").val(columnObject.ofuscationMethod.name);
        fillInputParam(columnObject);
        showParamsInputs($("#selectObfuscationMethod").val());
        $("#columnModal").modal('show');
    });
    $('[data-dismiss="modal"]').on("click",function(){
        envEditIndex = -1;
        tableEditIndex = -1;
        columnEditIndex = -1;
    });
    //Database selected
    $(document).on("click","#databaseListContainer > .chip-fluid",function(){
       
        var scopeThis = $(this);
        if(isDataModified){
            
            var options = {
                'message':'No se ha guardado la información que has modificado, se perderan los cambios ¿Deseas continuar sin guardar?' ,
                'yesButton': function(){
                    isDataModified = false;
                    $("#databaseListContainer > .new-database").remove();
                    showSuccess("No se han guardo los cambios en el servidor, selecciona otra BD.");
                    cleanAllInputs();
                    disableUIForm();
                    //requestBD(scopeThis);
                }
            };        
            customConfirm(options);
        }else{
            requestBD(scopeThis);
        }
        $("#btnExport").attr("href", "dbdetailreport?filename="+scopeThis.prop('id'));
        
    });
    
    // Table selected
    $(document).on("click",".table-chip",function(){
        if($(this).is('.active')){
            var index = $(this).attr("data-index");        
            updateColumns(index);            
        }else{
             $("#columnsListContainer").empty();
        }
    });
     // Column IMPORTED selected
    $(document).on("click","#modelListContainer > .chip-fluid",function(){
        // Return if it is disabled (meaning it is PK or FK).
        if($(this).is('.disabled')) return;
        
        var index = $(this).attr("data-index");        
        var indexTable = $(this).attr("data-index-imported-table");        
        if($(this).is('.active')){
            tablesImported[indexTable].columns[index].isSelected = true;            
        }else{
            tablesImported[indexTable].columns[index].isSelected = false;                         
        }
        //Get the number of selected columns.
        var numberOfColumnsSelected = tablesImported[indexTable].columns.filter(function(columnLooking){
                return columnLooking.isSelected;
            }).length;
        var originalText  = $("#tableSelectList option:selected").text().split(">")[0];           
        if(numberOfColumnsSelected > 0){
            $("#tableSelectList option:selected").css("color","red");
            $("#tableSelectList option:selected").text(originalText+" > " + numberOfColumnsSelected + " columnas seleccionadas" );
        }else{
            $("#tableSelectList option:selected").css("color","black");
            $("#tableSelectList option:selected").text(originalText.trim()); 
        }
        //Add color red to the table that has any table imported.        
    });
    // Edit Environment 
    $(document).on("click","#enviromentListContainer > .chip",function(){
        
        toggleDisableButton("#btnTestModel", false);   
        toggleDisableButton("#btnImportModelEnv", false);
        envEditIndex = this.dataset.index;        
        $("#inputEnviromentNameEdit").val(databaseObject.database.environments[envEditIndex].name);
        $("#inputOverrideNameEdit").val(databaseObject.database.environments[envEditIndex].overrideName);
        $("#host").val(databaseObject.database.environments[envEditIndex].host);
        $("#port").val(databaseObject.database.environments[envEditIndex].port);
        $("#serviceName").val(databaseObject.database.environments[envEditIndex].serviceName);
        var mscID = databaseObject.database.environments[envEditIndex].idMSC;
        if(mscID){
            $("#mscID").val(mscID);
        }else{
            $("#mscID").val(ID_MSC_DEFAULT);
        }
        connectionTested = false;
        $("#environmentModal").modal('show');
    });    
    
    //TEST CONNECTION BUTTON ------------------------------------------------------
    //TEST CONNECTION BUTTON ------------------------------------------------------
    $("#btnTestConnection").on("click",function(){        
        var host = $("#host").val();
        var port = $("#port").val();
        var serviceName = $("#serviceName").val();
        var mscID = $("#mscID").val();
         
        if(!host || !(port || serviceName)){
            showError("Debe definir el host, el port y/o el service name.");
            return;
        }
        var overrideName= $("#inputOverrideNameEdit").val();
        var dbName = (overrideName !== undefined && overrideName !== null && overrideName.length > 0) ? overrideName :  $("#inputDatabaseName").val();
        var data = {
            databaseName: dbName, 
            host: host,
            port: port,
            serviceName: serviceName,            
            mscID: mscID,
            vendorDataBase: $("#venderSelect").val().split("-")[0],
            versionDataBase: $("#venderSelect").val().split("-")[1]
            
        };
        data = JSON.stringify(data);
        $.ajax({
            url: "testconection.action",
            data: data,
            dataType: 'json',
            contentType: 'application/json',
            type: 'POST',
            cache: false,
            async: true,
            statusCode:{
                401: function(request, status, error) {
                    showError("No posees los permisos suficientes para realizar esta acción.");
                },
                403: function(request, status, error) {
                    showError("No posees los permisos suficientes para realizar esta acción.");
                },
                404: function(request, status, error) {
                    showError("No se pudo conectar con el servidor - Error 404.");
                }
            },
            success: function (res) {                
                if(res.result==="success"){
                    showSuccess(res.message);
                    connectionTested = true;
                    toggleDisableButton("#btnTestModel", false);
                    toggleDisableButton("#btnImportModelEnv", false);
                    
                }else{
                    connectionTested = false;
                    toggleDisableButton("#btnTestModel", true);
		            toggleDisableButton("#btnImportModelEnv", true);
                    validateSession(res.result);
                    showError(res.message);
                }
            },
            error: function(){                        
                showError("No se ha podido probar la conexión de la BD. El servidor no responde o no tienes los permisos necesarios.");
            }

        });
    });
    //TEST MODEL BUTTON ------------------------------------------------------
    //TEST MODEL BUTTON ------------------------------------------------------
    $("#btnTestModel").on("click",function(){        
        if(envEditIndex > -1){            
            var needToSaveDatabaseList = $("#databaseListContainer").find(".new-database");
            if(needToSaveDatabaseList.length < 1 && !isDataModified){                
                var data = {databaseName: databaseObject.database.name + ".xml", environment: databaseObject.database.environments[envEditIndex].name};
                data = JSON.stringify(data);
                $.ajax({
                    url: "testmodel.action",
                    data: data,
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    cache: false,
                    async: true,
                    statusCode:{
                        403: function(request, status, error) {
                            showError("No posees los permisos suficientes para realizar esta acción.");
                        }
                    },
                    success: function (res) {                
                        if(res.result==="success"){
                            showSuccess(res.message);                            
                            
                        }else{
                            toggleDisableButton("#btnTestModel", true);
                            toggleDisableButton("#btnImportModelEnv", true);
                            validateSession(res.result);
                            showError(res.message);
                        }
                    },
                    error: function(){                        
                        showError("No se ha podido probar el modelo de la BD");
                    }

                });
            }else{
                showError("Debes guardar los cambios antes de probar el modelo! ");
                var options = {
                    'message':'Debes guardar las bases de datos modificadas antes de probar el modelo! ¿Deseas guardar ahora?' ,
                    'yesButton': function(){
                        isDataModified = false;
                        var actualDatabase = $("#databaseListContainer").find(".active")[0];
                        $("#btnGuardarXML").trigger("click");
                        $("#environmentModal").modal('hide');
                        
                    }
                };        
                customConfirm(options);                
                
            }
        }else{
            showError("Debes guardar los cambios antes de probar el modelo! ");
            var options = {
                'message':'Debes guardar los cambios antes de probar el modelo! ¿Deseas guardar ahora?' ,
                'yesButton': function(){
                    isDataModified = false;
                    $("#btnGuardarXML").trigger("click");

                }
            };   
        }
    });
    //IMPORT MODEL BUTTON
    //IMPORT MODEL BUTTON
    $("#btnImportModelEnv").on("click", function () {
        var host = $("#host").val();
        var port = $("#port").val();
        var serviceName = $("#serviceName").val();
        var mscID = $("#mscID").val();
        var dbName = $("#inputDatabaseName").val();
        var data = {
            databaseName: dbName,
            host: host,
            port: port,
            serviceName: serviceName,
            mscID: mscID,
            vendorDataBase: $("#venderSelect").val().split("-")[0],
            versionDataBase: $("#venderSelect").val().split("-")[1]
        };
        data = JSON.stringify(data);
        $.ajax({
            url: "importmodel.action",
            data: data,
            dataType: "json",
            contentType: "application/json",
            type: "POST",
            cache: false,
            async: true,
            success: function (res) {
                if (res.result === "success") {
                    //showSuccess(res.message);
                    connectionTested = true;
                    console.log(res.modelImported);
                    tablesImported = res.modelImported;
                    updateImportedTables();
                    $("#btnSaveEnviroment").trigger("click");
                    //$("#environmentModal").modal('hide');
                    $("#modelModal").modal("show");
                    

                    setTimeout(function () {
                        $("#explainModelImport").tooltip("show");
                    }, 1000);
                } else {
                    connectionTested = false;
                    toggleDisableButton("#btnTestModel", true);
		    toggleDisableButton("#btnImportModelEnv", true);
                    validateSession(res.result);
                    showError(res.message);
                }
            },
            error: function () {
                showError(
                        "No se puede importar el modelo. Compruebe la conexion a la BD."
                        );
            }
        });
    });
    //OBFUSCATE BUTTON ------------------------------------------------------
    //OBFUSCATE BUTTON ------------------------------------------------------
    
    $("#btnObfuscate").on("click",function(){        
        if(envEditIndex > -1){            
            var needToSaveDatabaseList = $("#databaseListContainer").find(".new-database");
            if(needToSaveDatabaseList.length < 1 && !isDataModified){
                
                var data = {databaseName: databaseObject.database.name + ".xml", environment: databaseObject.database.environments[envEditIndex].name};
                data = JSON.stringify(data);
                $.ajax({
                    url: "obfuscate.action",
                    data: data,
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    cache: false,
                    async: true,
                    statusCode:{
                        403: function(request, status, error) {
                            showError("No posees los permisos suficientes para realizar esta acción.");
                        }
                    },
                    success: function (res) {                
                        if(res.result==="success"){
                            showSuccess("Se obfuscó la BD con éxito.");
                        }else{
                            validateSession(res.result);
                            showError("No se ha podido Obfuscar la BD. Verifique la definición y la información de conexión. ErrorCode: "+res.errorCode);
                        }
                    },
                    error: function(){                        
                        showError("No se ha podido Obfuscar la BD");
                    }

                });
            }else{
                console.log("Debes guardar las bases de datos modificadas antes de obfuscar! ");
                showError("Debes guardar las bases de datos y ambientes modificados antes de obfuscar! ");
            }
        }else{
            console.log("ELSE envEditIndex < 0");
            showError("El ambiente seleccionado no existe o no se ha guardado. Guarde el ambiente y el XML para poder obfuscar.");
        }
    });
    $("#btnSlowObfuscate").on("click",function(){        
        if(envEditIndex > -1){            
            var needToSaveDatabaseList = $("#databaseListContainer").find(".new-database");
            if(needToSaveDatabaseList.length < 1 && !isDataModified){
                
                var data = {databaseName: databaseObject.database.name + ".xml", environment: databaseObject.database.environments[envEditIndex].name};
                data = JSON.stringify(data);
                $.ajax({
                    url: "slowobfuscate.action",
                    data: data,
                    dataType: 'json',
                    contentType: 'application/json',
                    type: 'POST',
                    cache: false,
                    async: true,
                    statusCode:{
                        403: function(request, status, error) {
                            showError("No posees los permisos suficientes para realizar esta acción.");
                        }
                    },
                    success: function (res) {                
                        if(res.result==="success"){
                            showSuccess("Se obfuscó la BD con éxito.");
                        }else{
                            validateSession(res.result);
                            showError("No se ha podido Obfuscar la BD. Verifique la definición y la información de conexión. ErrorCode: "+res.errorCode);
                        }
                    },
                    error: function(){                        
                        showError("No se ha podido Obfuscar la BD");
                    }

                });
            }else{
                console.log("Debes guardar las bases de datos modificadas antes de obfuscar! ");
                showError("Debes guardar las bases de datos y ambientes modificados antes de obfuscar! ");
            }
        }else{
            console.log("ELSE envEditIndex < 0");
            showError("El ambiente seleccionado no existe o no se ha guardado. Guarde el ambiente y el XML para poder obfuscar.");
        }
    });
    
    $("#btnImportModel").on("click",function(){      
        importModel();
    });
    //BUTTONS SAVE EVENTS ----------------------------------------------------
    //BUTTONS SAVE EVENTS ----------------------------------------------------
    
    $("#btnGuardarXML").click(function(e){
        e.preventDefault();
        isDataModified = false;              
        
        databaseObject.database.name = $("#inputDatabaseName").val();
        databaseObject.database.appOwner = $("#inputAppOwner").val();
        databaseObject.database.vendorDataBase = $("#venderSelect").val().split("-")[0];
        databaseObject.database.versionDataBase = $("#venderSelect").val().split("-")[1];
        if(validateInputs()){           
            var data = JSON.stringify(databaseObject);
            
            $.ajax({
                url: "savedatabase.action",
                data: data,
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                async: true,
                statusCode:{
                    403: function(request, status, error) {
                        showError("No posees los permisos suficientes para realizar esta acción.");
                    }
                },
                success: function (res) {                
                    if(res.result==="success"){
                        showSuccess(databaseObject.database.name + ", se ha guardado con éxito!");
                        cleanAllInputs();
                        envEditIndex = -1;
                        tableEditIndex = -1;
                        columnEditIndex = -1;
                        activeDB="";
                        databaseObject = {'database':{
                            'name':'',
                            'appOwner': '',
                            'dateCreation': "",
                            'dateModification':"",
                            'environments': [],
                            'tables':[],
                            'vendorDataBase':'',
                            'versionDataBase':"10g"
                       }};
                        getDatabaseList();
                        $("#labelDatabaseOnForm").text("Seleccione una BD");    
                        disableUIForm();
                    }else{
                        showError(res.message);
                        validateSession(res.result);
                    }                 
                }
            });  
        }
        
    });
    //Guardar Ambiente y agregar a la lista
    $("#btnSaveEnviroment").click(function(){
        var name = $("#inputEnviromentNameEdit").val();
        var overrideName = $("#inputOverrideNameEdit").val();
        var host = $("#host").val();
        var port = $("#port").val();
        var serviceName = $("#serviceName").val();
        var mscID = $("#mscID").val();
        
        if(envEditIndex < 0){            
            var env = {'name': name, 'host': host , 'port': port , 'serviceName': serviceName, 'idMSC': mscID, 'overrideName': overrideName };
            var alreadyExist = $("#enviromentListContainer").find("#"+name);
            if(!alreadyExist.length > 0){
                createNewEnviroment(env);                
            }else{
                customConfirm({'message':"Ya existe un ambiente con ese nombre."});                    
                return;
            }
        }else{
            //check if the user renamed to a environment already defined. 
            //Use the envEditIndex and find() to check if the editing chip is the same that
            //the one that has the new name! Also this can be done when the onChange method of the input!
            var alreadyExist = $("#enviromentListContainer").find("#"+name);
            if(alreadyExist.length > 0 && !$(alreadyExist[0]).hasClass("active")){
                customConfirm({'message':"Ya existe un ambiente con ese nombre."});                    
                return;            
            }else{
                databaseObject.database.environments[envEditIndex].name = name;
                databaseObject.database.environments[envEditIndex].host = host;
                databaseObject.database.environments[envEditIndex].port = port;
                databaseObject.database.environments[envEditIndex].serviceName = serviceName;                     
                databaseObject.database.environments[envEditIndex].idMSC = mscID;                     
                databaseObject.database.environments[envEditIndex].overrideName = overrideName;                     
                envEditIndex= -1; 
                
            }
            //editing an env already created
           
        }
        updateEnvironments();
       $("#inputEnviromentName").val("");
       $("#inputEnviromentNameEdit").val("");
       $("#host").val("");
       $("#port").val("");
       $("#serviceName").val("");       
       $('#environmentModal').modal('hide');
    });
    //Guardar Tabla y agregar a la lista de chips
    $("#btnSaveTable").click(function(){       
        var name = $("#inputTableName").val();
        var blockSize = $("#inputBlockSize").val();
        if(name.length < 1) {
            showError("El nombre de la tabla es requerido");
            return;
        }
        
        if(tableEditIndex < 0){
            var table = {'columns': [], 'name': name, 'blockSize': blockSize};
            var alreadyExist = $("#tablesListContainer").find("#"+name);
            if(!alreadyExist.length > 0){                
                createNewTable(table);             
            }else{
                customConfirm({'message':"Ya existe una tabla con ese nombre."});                    
                return;
            }
                
        }else{
            databaseObject.database.tables[tableEditIndex].name = name;
            databaseObject.database.tables[tableEditIndex].blockSize = blockSize;
        }
        updateTables();
        tableEditIndex = -1;
        $("#inputTableName").val("");
        $('#tableModal').modal('hide');
    });
    //Guardar Columna y agregar a la lista de chips
    $("#btnSaveColumn").click(function(){
        var index = getActiveTableIndex();       
        var name = $("#inputColumnName").val();
        if(name.length < 1) {
            showError("El nombre de la columna es requerido");
            return;
        }
        var areParamsSetted = true;
        $("input.methodsParam.active").each(function(){            
            var value = $(this).val();            
            if(!value || value.lenght < 1){
                showError("El parametro es requerido.");
                areParamsSetted = false;
                $(this).focus();
                return;
            }            
        });
        if(!areParamsSetted){
            return;
        }
        var dataType = $("#inputColumnDataType").val();       
        var OfuscationMethod = getOfuscationMethod();
       
       if(columnEditIndex < 0){
            var column = {'name': name, 'dataType': dataType, 'ofuscationMethod': OfuscationMethod };
            var alreadyExist = $("#columnsListContainer").find("#"+name);
            if(!alreadyExist.length > 0){                                
                createNewColumn(index,column);
            }else{
                customConfirm({'message':"Ya existe una columna con ese nombre."});                    
                return;
            }
       }else{
           databaseObject.database.tables[tableEditIndex].columns[columnEditIndex].name = name;
           databaseObject.database.tables[tableEditIndex].columns[columnEditIndex].dataType = dataType;
           databaseObject.database.tables[tableEditIndex].columns[columnEditIndex].ofuscationMethod = OfuscationMethod;
       }
       updateColumns(index);
       $("#inputColumnName").val("");
       $('#columnModal').modal('hide');
        columnEditIndex = -1;
    });
    
    $("#btnExport").on("click",function(){
       
       if(activeDB==="" || activeDB=== undefined){
           showError("Debe seleccionar una base de datos a exportar.");
           e.preventDefault();           
       }else{
           showSuccess("Generando reporte de " + activeDB);
       } 
    });
    //$("#btnSaveData")
    // CUSTOM EVENTS ---------------------------------------------------------
    // CUSTOM EVENTS ---------------------------------------------------------
    //Activación o desactivación de un Chips de Database.
    $(".data-modified-control").on("database:toggle", function(){        
        
        if($(".database-chip").is(".active")){ 
            $(".data-modified-control").prop('disabled', false);
            $("button.data-modified-control").removeClass("disabled btn-outline-secondary");
            $("button.data-modified-control").addClass("btn-outline-danger");
            toggleDisableButton("#btnOpenRowCount", false);
            if(ableToReport) toggleDisableButton("#btnExport", false);  
            
            if(ableToEdit){
                $("#btnGuardarXML").removeClass("disabled btn-outline-secondary");
                $("#btnGuardarXML").addClass("btn-danger");
                $("#btnGuardarXML").prop('disabled', false);
            }
            if(!ableToObfuscate){
                toggleDisableButton("#btnObfuscate", true);
            }
            
        }else{
            disableUIForm();
        }
       
    });
    
    //On selecting an obfuscation method:
    $("#selectObfuscationMethod").change(function(){        
        showParamsInputs($("#selectObfuscationMethod").val());
    });
    //On selecting an obfuscation method:
    $("#tableSelectList").change(function(){      
        $('#explainModelImport').tooltip('hide');
        updateImportedColumns($("#tableSelectList").val());
    });
    
    
    //TRIGGER EVENTS ---------------------------------------------------------
    //TRIGGER EVENTS ---------------------------------------------------------
    
    $(".data-modified-control").trigger("database:toggle");    
    //Fill the list of databases.
    getDatabaseList();
});

function disableUIForm(){
    toggleDisableButton("button.data-modified-control", true);
    toggleDisableButton("#btnExport", true);
    toggleDisableButton("#btnOpenRowCount", true);
    //inputs
    $(".data-modified-control").prop('disabled', true); 
    $("#btnGuardarXML").addClass("disabled btn-outline-secondary");
    $("#btnGuardarXML").removeClass("btn-danger");
    $("#btnGuardarXML").prop('disabled', true);
}
//FUNCTIONS ---------------------------------------------------------
//FUNCTIONS ---------------------------------------------------------
function getActiveTableIndex(){
    var index = -1;
    $("#tablesListContainer > .table-chip").each(function(){
        if($(this).is(".active")){
            index =  this.dataset.index;
        }
    });
    return index;
}
function getOfuscationMethod(){
    var params = getParamsOfuscation();
    var ofuscationMethod = {'name': $("#selectObfuscationMethod").val(), 'method': $("#selectObfuscationMethod").val(), 'params': params };       
    return ofuscationMethod;
}
function getParamsOfuscation(){
    var paramsResult = [];
    $(".methodsParam.active").each(function(){
        var name = $(this).attr('id');
        var value = $(this).val();
        paramsResult.push({'name':name, 'value':value});
    });
    return paramsResult;
}
function updateEnvironments(){
    $("#enviromentListContainer").empty();
    
    var environments = databaseObject.database.environments;
    for(var i = 0; i < environments.length; i++){
        var element = document.createElement('div');
        var span = document.createElement('span');        
        element.className = "chip";
        span.className = "chipText";
        span.innerHTML = environments[i].name;
        element.id= environments[i].name;
        element.title= "Url: "+environments[i].host + ":" + environments[i].port +"<br> ServiceName: "+environments[i].serviceName +"<br> OverrideSchema: "+environments[i].overrideName;
        element.dataset.index= i;
        element.dataset.toggle= "tooltip";
        element.dataset.html="true";
        element.dataset.placement= "top";
        element.appendChild(span);
        addControlsButtons(element,{
            'editControl':false,
            'closeControl':true
        });
        $("#enviromentListContainer").append(element);
        $("#enviromentListContainer > .chip").tooltip();
    }
}
function updateTables(){
    $("#tablesListContainer").empty();
    $("#columnsListContainer").empty();
    //var i = 0;
    var tables =  databaseObject.database.tables;
    
    for(var i= 0 ; i < tables.length; i++){
        var element = document.createElement('div');
        var span = document.createElement('span');
        
        element.className = "chip-fluid table-chip";
        span.className = "chipText";
        span.innerHTML = tables[i].name;
        element.id= tables[i].name;
        element.dataset.index= i;
        element.appendChild(span);  
        
        addControlsButtons(element,{
            'editControl':true,
            'closeControl':true
        });
        $("#tablesListContainer").append(element);
    }
    /*for(table of tables){
        var element = document.createElement('div');
        var span = document.createElement('span');
        
        element.className = "chip-fluid table-chip";
        span.className = "chipText";
        span.innerHTML = table.name;
        element.id= table.name;
        element.dataset.index= i++;
        element.appendChild(span);  
        
        addControlsButtons(element,{
            'editControl':true,
            'closeControl':true
        });
        $("#tablesListContainer").append(element);
    }*/
}


function updateColumns(index){
    
    $("#columnsListContainer").empty();
    //var i = 0;
    var columns =  databaseObject.database.tables[index].columns;
    for(var i = 0; i < columns.length; i++ ){
        var element = document.createElement('div');
        var span = document.createElement('span');
        element.className = "chip-fluid";
        span.className = "chipText";
        span.innerHTML = columns[i].name;
        element.id= columns[i].name;
        element.dataset.index= i;
        element.dataset.indexTable= index;
        element.appendChild(span);  
        addControlsButtons(element,{
            'editControl':true,
            'closeControl':true
        });
        $("#columnsListContainer").append(element);
    }
    /*for(column of tables[index].columns){
        var element = document.createElement('div');
        var span = document.createElement('span');
        element.className = "chip-fluid";
        span.className = "chipText";
        span.innerHTML = column.name;
        element.id= column.name;
        element.dataset.index= i++;
        element.dataset.indexTable= index;
        element.appendChild(span);  
        addControlsButtons(element,{
            'editControl':true,
            'closeControl':true
        });
        $("#columnsListContainer").append(element);
    }*/
}

function updateImportedTables(){
    $("#tableSelectList").empty();
    $("#modelListContainer").empty();   
    var element = document.createElement('option');
    element.disabled = false;
    element.innerHTML = "Seleccione una tabla";
    $("#tableSelectList").append(element);
    for(var i = 0; i <  tablesImported.length; i++){
        element = document.createElement('option');
        element.value= i;
        element.innerHTML = tablesImported[i].name;
        $("#tableSelectList").append(element);
    }
}
function updateImportedColumns(index){   
    $("#modelListContainer").empty(); 
    for(var i = 0; i <  tablesImported[index].columns.length; i++){
        var element = document.createElement('div');
        var span = document.createElement('span');
        
        
        element.className = "chip-fluid multiselect ";        
        // If is needed for IE11. If the disable="false" is in the element, IE11 doens't let the user select the column.
        if(tablesImported[index].columns[i].PK || tablesImported[index].columns[i].FK) {
            element.setAttribute("disabled", tablesImported[index].columns[i].PK || tablesImported[index].columns[i].FK);
        }
        span.className = "chipText";
        span.innerHTML = tablesImported[index].columns[i].name ;
        
        element.id= tablesImported[index].columns[i].name;
        element.dataset.index= i;
        element.dataset.indexImportedTable= index;
        
        if(tablesImported[index].columns[i].PK || tablesImported[index].columns[i].FK){
            element.className += "disabled ";
            span.innerHTML += " &nbsp; - &nbsp;";
            let iconWarning = document.createElement('i');
            let TEXTPK = (tablesImported[index].columns[i].PK)? "PK":"" ;
            let TEXTFK = (tablesImported[index].columns[i].FK)? " FK" : "";
            iconWarning.className = "fas fa-exclamation-circle tooltipIcon";
            iconWarning.dataset.toggle = "tooltip";
            iconWarning.setAttribute("title", TEXTPK + " " + TEXTFK);
            iconWarning.dataset.placement = "top";
            iconWarning.dataset.originalTitle = TEXTPK + " " + TEXTFK;
            
            span.appendChild(iconWarning);
        }
        if(tablesImported[index].columns[i].isSelected){
            element.className += "active" ;
        }
        
        element.appendChild(span);
        $("#modelListContainer").append(element);
    }
    $(".fa-exclamation-circle.tooltipIcon").tooltip();
}
function cleanAllInputs(){
    $("input.data-modified-control").val("");    
    $(".container-to-clean").empty();
    $("#inputBlockSize").val("0");
}
function createNewDB(){
    var element = document.createElement('div');
    var img = document.createElement('img');
    var span = document.createElement('span');
    var icon = document.createElement('i');
    cleanAllInputs();
    element.className = "database-chip chip-fluid icon-adjust active new-database";
    span.className = "chipText";
    //img.src = "img/icons/database-icon2.png";
    span.innerHTML =" nuevaBD";
    icon.className="fas fa-database";
    element.appendChild(icon);
    element.appendChild(span);

    $("#databaseListContainer > .chip-fluid").removeClass("active");
    isDataModified= true;
    element.id= "nuevaBD";
    element.dataset.databaseName = "nuevaBD";
    
    $("#inputDatabaseName").attr("data-bindedChip",element.id);
    $("#databaseListContainer").append(element);
    $("#labelDatabaseOnForm").text("Editando: nuevaBD");
    activeDB= "nuevaBD";
    $(".data-modified-control").trigger("database:toggle");
    databaseObject = {
            'database':{
                'name':'',
                'appOwner': '',
                'dateCreation': "",
                'dateModification':"",
                'environments': [],
                'tables':[],
                'vendorDataBase':'',
                'versionDataBase':"10g"
            },
            'previousName':'',
            'isNewDataBase': true
        };
}

function createNewEnviroment(env){
    isDataModified= true;
    //environments.push(env);
    databaseObject.database.environments.push(env);
    //updateEnvironments();
}

function createNewTable(table){
    isDataModified= true;
    //tables.push(table);
    databaseObject.database.tables.push(table);    
}
function createNewColumn(index,column){    
    isDataModified= true;
    databaseObject.database.tables[index].columns.push(column);
    //databaseObject.database.tables = tables;
}
function showParamsInputs(method){    
    //Hide all params. The switch will show the required one.
    $(".methodsParam").hide();
    $(".methodsParam").removeClass("active");
    switch(method){
        case 'VALOR_FIJO':                       
            $(".fixedValueParam").show(); 
            $("#fixedValue").addClass("active"); 
            
            break;
        case 'INTERCAMBIO_LETRAS':
            //No params
            
            break;
        case 'COPIAR_COLUMNA':
            //No params
            $(".CopyFromValueParam").show(); 
            $("#CopyFromValue").addClass("active");
            break;
    }
}
function validateInputs(){
    //console.log("[validateInputs]");
    //return true;
    if(databaseObject.database.name === undefined || databaseObject.database.name.trim().length <= 1){
        $("#inputDatabaseName").addClass("requiredInput");
        $("#inputDatabaseName").focus();
        showError("El nombre de la base de datos es requerido.");
        return false;
    }
    if(databaseObject.database.vendorDataBase === undefined || databaseObject.database.vendorDataBase.trim().length <= 1){
        //venderSelect
        $("#venderSelect").addClass("requiredInput");
        $("#venderSelect").focus();
        showError("Tipo de base de datos es requerido.");
        return false;
    }
    if(databaseObject.database.versionDataBase === undefined || databaseObject.database.versionDataBase.trim().length <= 1){
        $("#venderSelect").addClass("requiredInput");
        $("#venderSelect").focus();
        showError("Tipo de base de datos es requerido.");
        return false;
    }
    if(databaseObject.database.environments === undefined || databaseObject.database.environments.length < 1){
        showError("No ha definido ningún ambiente. Debe definir al menos uno!");
         $("#inputEnviromentName").focus();
        return false;
    }else{
        for(var i = 0; i < databaseObject.database.environments.length; i++ ){
            if(databaseObject.database.environments[i].name === undefined || databaseObject.database.environments[i].name.trim().length < 2){
                showError("El nombre de ambiente es requerido y debe tener al menos 2 caracteres");
                $("#enviromentListContainer > .chip[data-index='"+i+"']").trigger("click");
                $("#inputEnviromentNameEdit").focus();
                $("#inputEnviromentNameEdit").addClass("requiredInput");
                return false;
            }
            if(databaseObject.database.environments[i].host === undefined || databaseObject.database.environments[i].host.trim().length <= 2){
                showError("El host es requerido.");
                $("#enviromentListContainer > .chip[data-index='"+i+"']").trigger("click");
                $("#host").focus();
                $("#host").addClass("requiredInput");;
                return false;
            }
            var isPortServiceName = (databaseObject.database.environments[i].port === undefined || databaseObject.database.environments[i].port.trim().length <= 0) 
                    && (databaseObject.database.environments[i].serviceName === undefined || databaseObject.database.environments[i].serviceName.trim().length <= 0);
            if(isPortServiceName){
                showError("Debe definir el puerto o el service name.");
                $("#enviromentListContainer > .chip[data-index='"+i+"']").trigger("click");
                $("#port").focus();
                $("#port").addClass("requiredInput");
                return false;
            }
        }
    }
    if(databaseObject.database.tables=== undefined || databaseObject.database.tables.length < 1){
        showError("No ha definido ninguna Tabla. Debe definir al menos una!");
        return false;
    }
    for(var i = 0; i < databaseObject.database.tables.length; i++){
        console.log("[validateInputs] Table: "+i);
        console.log("[validateInputs] Table name with trim: "+databaseObject.database.tables[i].name.trim());
        console.log("[validateInputs] Table lenght: "+databaseObject.database.tables[i].name.trim().length);
        console.log("[validateInputs] Table name undefined?: "+ databaseObject.database.tables[i].name.trim().length <= 1);
        console.log("[validateInputs] Table name less than 2 characters?: "+ databaseObject.database.tables[i].name.trim().lenght <= 1);
        
        
        if(databaseObject.database.tables[i].name === undefined ||databaseObject.database.tables[i].name.trim().length <= 1){
           // console.log("[validateInputs] Table: "+ name);
            $(".table-chip[data-index='"+i+"']").trigger("click");
            $("#inputTableName").focus();
            $("#inputTableName").addClass("requiredInput");
            $('#tableModal').modal('show');
            return false;
        }
        for(var j = 0; j < databaseObject.database.tables[i].columns.length; j++ ){
            if(databaseObject.database.tables[i].columns[j].name === undefined ||databaseObject.database.tables[i].columns[j].name.trim().lenght <= 1){
                $("").trigger("click");
                $("").focus();
                $("").addClass("requiredInput");     
                return false;
            }
            if(databaseObject.database.tables[i].columns[j].ofuscationMethod === undefined ||databaseObject.database.tables[i].columns[j].ofuscationMethod.name.trim().lenght <= 1){
                $("").trigger("click");
                $("").focus();
                $("").addClass("requiredInput");     
                return false;
            }
        }
    }
    return true;
    
}

function addAllControlsButtons(element){
    var divControls = document.createElement('div');
    var spanEditIcon = document.createElement('span');        
    var spanCloseIcon = document.createElement('span');
    //controlls
    divControls.className="float-right";
    spanEditIcon.className="fas fa-edit icon-control";       

    spanCloseIcon.className="fas fa-times icon-control";

    divControls.appendChild(spanEditIcon);
    divControls.appendChild(spanCloseIcon);
    element.appendChild(divControls);
}
function addControlsButtons(element, options){
    var divControls = document.createElement('div');
    divControls.className="float-right controls-container";
    if(options.editControl){
        var spanEditIcon = document.createElement('span');        
        spanEditIcon.className="fas fa-edit icon-control btnEdit"; 
        divControls.appendChild(spanEditIcon);
    }
    if(options.closeControl){
        var spanCloseIcon = document.createElement('span');        
        spanCloseIcon.className="fas fa-times icon-control btnClose";
        divControls.appendChild(spanCloseIcon);
    }
    element.appendChild(divControls);
}

function customConfirm(options){
    if(options.message){
        $("#messageBodyModal").html(options.message);        
        
        $("#avisoModalControlls").empty();
        if(options.yesButton){
            //console.log("On yes button binding event");
            $("#avisoModalControlls").append('<button type="button" data-dismiss="modal" class="btn btn-outline-secondary">Cancelar</button>');
            var actionButton = document.createElement('button');
            actionButton.className = "btn btn-outline-primary";
            actionButton.type = "button";
            actionButton.dataset.dismiss = "modal";
            actionButton.onclick = options.yesButton;
            actionButton.innerHTML = "Aceptar";
            $("#avisoModalControlls").append(actionButton);
            
        }else{
            $("#avisoModalControlls").html('<button type="button" data-dismiss="modal" class="btn btn-outline-primary">Aceptar</button>');
        }
        $("#messageModal").modal('show');
       
    }  
}
 function requestBD(scopeThis){
        var databaseId = scopeThis.prop('id'); 
        cleanAllInputs();
        envEditIndex = -1;
        tableEditIndex = -1;
        columnEditIndex = -1;
        //If is the same DB selected just deactivate it.
        if(activeDB===databaseId){            
             $("#labelDatabaseOnForm").text("Seleccione una BD");
             $("#inputDatabaseName").attr("data-bindedChip","");
             activeDB="";
        }else if(scopeThis.hasClass("new-database")) {
             //If is a just created Database, do not search for ir on the server. 
            $("#labelDatabaseOnForm").text("Editando: "+ databaseId);
            $("#inputDatabaseName").attr("data-bindedChip",scopeThis.attr('id'));
            activeDB= databaseId;            
            //Clean data ob databaseObject
            databaseObject = {'database':{
                        'name':'',
                        'appOwner': '',
                        'dateCreation': "",
                        'dateModification':"",
                        'environments': [],
                        'tables':[],
                        'vendorDataBase':'',
                        'versionDataBase':"10g"
                   }};
        }else{
            //If it's a new chip DB selected, request with ajax the data.
            $("#labelDatabaseOnForm").text("Editando: "+ databaseId);
            $("#inputDatabaseName").attr("data-bindedChip",databaseId);
            activeDB= databaseId;            
            //console.log("Sending data to action AJAX");
            var xmlName = databaseId.trim()+".xml";            
            var xmlNameObject = {'name': xmlName };
            //console.log(xmlNameObject);
            var data = JSON.stringify(xmlNameObject);
            
            $.ajax({
                url: "database.action",
                data: data,
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                cache: false,
                async: true,
                statusCode:{
                    403: function(request, status, error) {
                        showError("No posees los permisos suficientes para realizar esta acción.");
                    }
                },
                success: function (res) {                
                    console.log(res);
                    if(!validateSession(res.result)) return;
                    $("#inputDatabaseName").val(res.database.name);
                    $("#inputAppOwner").val(res.database.appOwner);
                    $("#venderSelect").val(res.database.vendorDataBase+"-"+res.database.versionDataBase);
                    databaseObject.database = res.database;
                    databaseObject.previousName = res.database.name;
                    databaseObject.isNewDataBase = false;
                    if(res.database.environments){
                        //environments = res.database.environments;
                        updateEnvironments();                    
                    }                   
                    if(res.database.tables){                      
                       // tables = res.database.tables;
                        updateTables();
                    }
                    showSuccess("Se ha cargado la BD seleccionada");
                },
                error: function(){
                    databaseObject = {'database':{
                        'name':'',
                        'appOwner': '',
                        'dateCreation': "",
                        'dateModification':"",
                        'environments': [],
                        'tables':[],
                        'vendorDataBase':'',
                        'versionDataBase':"10g"
                   }};
                    showError("Ha ocurrido un error inesperado");
                }
                
            });
        }
        $(".data-modified-control").trigger("database:toggle");
    }
   function deleteDatabase(databaseId){        
        cleanAllInputs();
        envEditIndex = -1;
        tableEditIndex = -1;
        columnEditIndex = -1;
        
        var xmlName = databaseId.trim()+".xml";            
        var xmlNameObject = {'name': xmlName };
        
        var data = JSON.stringify(xmlNameObject);
        $.ajax({
                url: "deletedatabase.action",
                data: data,
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                cache: false,
                async: true,
                statusCode:{
                    403: function(request, status, error) {
                        showError("No posees los permisos suficientes para realizar esta acción.");
                    }
                },
                success: function (res) {                        
                    getDatabaseList();
                    showSuccess(res.message);
                },
                error: function(res){   
                    getDatabaseList();                    
                    showError(res.message);
                }
                
            });
    }
    function getDatabaseList(){
        var data = JSON.stringify({});
        $.ajax({
                url: "listdatabase.action", 
                data: data,
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                cache: false,
                async: true,
                statusCode:{
                    403: function(request, status, error) {
                        showError("No posees los permisos suficientes para realizar esta acción.");
                    }
                },
                success: function (res) {                
                    if(!validateSession(res.result)) return;
                    console.log(res);
                    databaseList = res.dbItems;  
                    databaseList.sort(function(a, b){
                        var value1 =a.appOwner.toLowerCase(), value2=b.appOwner.toLowerCase();
                        if (value1 < value2) //sort string ascending
                            return -1;
                        if (value1 > value2)
                            return 1;
                        return 0; //default return value (no sorting)
                    });
                    
                    updateDatabaseList();
                    //console.log(databaseList);
                },
                error: function(){
                    
                    showError("Ha ocurrido un error inesperado al intentar obtener las lista de Base de datos.");
                }
                
            });
    }
    function updateDatabaseList(){
        //databaseList[]
        console.log('updating database list');
        $("#databaseListContainer").empty();
        for(var i = 0; i < databaseList.length; i++ ){
            var element = document.createElement('div');
            var img = document.createElement('img');
            var span = document.createElement('span');
            var icon = document.createElement('i');
            cleanAllInputs();
            element.className = "database-chip chip-fluid icon-adjust";
            span.className = "chipText";
            //img.src = "img/icons/database-icon2.png";
            span.innerHTML =" " +databaseList[i].appOwner  + "-" + databaseList[i].databaseName;
            icon.className="fas fa-database";
            element.appendChild(icon);
            element.appendChild(span);
            
            element.id= databaseList[i].databaseName ;
            element.dataset.databaseName = databaseList[i].databaseName;
            element.dataset.appOwner = databaseList[i].appOwner;
            
            addControlsButtons(element,{
                'editControl':false,
                'closeControl':true
            });
            $("#databaseListContainer").append(element);
            
        }
        
    }
    function filterDatabaseList(filter){
        //databaseList[]
      
        $("#databaseListContainer").empty();
        for(var i = 0; i < databaseList.length; i++ ){
            textToFilter = databaseList[i].databaseName.trim() + "-"+ databaseList[i].appOwner.trim();            
            textToFilter = textToFilter.toLowerCase();
            filterBoolean = textToFilter.indexOf(filter.toLowerCase()) >= 0;
            if(!filterBoolean) {                
                continue;
            }
            var element = document.createElement('div');
            var img = document.createElement('img');
            var span = document.createElement('span');
            var icon = document.createElement('i');
            cleanAllInputs();
            element.className = "database-chip chip-fluid icon-adjust";
            span.className = "chipText";
            span.innerHTML =" " +databaseList[i].appOwner  + "-" + databaseList[i].databaseName;
            icon.className="fas fa-database";
            element.appendChild(icon);
            element.appendChild(span);
            element.id= databaseList[i].databaseName ;
            element.dataset.databaseName = databaseList[i].databaseName;
            element.dataset.appOwner = databaseList[i].appOwner;
            
            $("#databaseListContainer").append(element);
            
        }
        
    }
    function fillInputParam(columnObject) {
        if (columnObject.ofuscationMethod.params) {
            switch (columnObject.ofuscationMethod.name) {
                case 'VALOR_FIJO':
                    $("#fixedValue").val(columnObject.ofuscationMethod.params[0].value);
                    break;
                case 'intercambio_letras':
                    //No params                    
                    break;
                case 'COPIAR_COLUMNA':
                    //No params                     
                    $("#CopyFromValue").val(columnObject.ofuscationMethod.params[0].value);
                    break;
            }
        } 
    }
    function validateSession(result){
        
        if(result === undefined || result === null || result.toLowerCase().indexOf("session")>=0){
            setTimeout(function(){
                 window.location.href="/obfuscator/index";
            },3000);
            return false;
        }else{
            return true;
        }
    }
    
    function showError(message){
        var notice = new PNotify({
            text: '<em>'+ message + '</em>',
            type: 'error',
            addclass: 'stack-bottomright translucent',
            stack: stack_bottomright,
            icon: 'fas fa-exclamation',
            hide: false,
            buttons: {
                sticker: false
            },
            animate: {
                animate: true,
                in_class: 'bounceInLeft',
                out_class: 'bounceOutRight'
            }
        });
   
        notice.get().click(function() {
            $(this).remove();
        });
    }
    function showSuccess(message){
         var notice = new PNotify({
            text: message,
            type: 'success',
            addclass: 'stack-bottomright translucent',
            stack: stack_bottomright,
            icon: 'fas fa-check-double',
            hide: true,
            delay:4000,
            buttons: {
                sticker: false
            },
            animate: {
                animate: true,
                in_class: 'bounceInLeft',
                out_class: 'bounceOutRight'
            }
            
        });
        notice.get().click(function() {
            $(this).remove();
        });
    }
   function toggleDisableButton(selector, isDisabled){
       if(isDisabled){
            $(selector).addClass("disabled btn-outline-secondary");
            $(selector).removeClass("btn-outline-danger");
            $(selector).prop('disabled', true);
       }else{
            $(selector).removeClass("disabled btn-outline-secondary");
            $(selector).addClass("btn-outline-danger");
            $(selector).prop('disabled', false); 
       }       
   }
   function checkFunctions(){
       if(funcs.indexOf("Editar")< 0){
           toggleDisableButton(".btnEditFunction",true);    
           ableToEdit = false;
       }else{
           ableToEdit = true;
       }
       
       if(funcs.indexOf("Reportar")< 0){
            toggleDisableButton(".btnReportFunction",true);           
            ableToReport = false;
       }else{
           ableToReport = true;
       }
       
       if(funcs.indexOf("Auditar")< 0){
            toggleDisableButton(".btnAuditarFunction",true);           
            ableToAudit = false;
       }else{
           ableToAudit = true;
       }
       if(funcs.indexOf("Obfuscar")< 0){
            toggleDisableButton("#btnObfuscate",true);           
            ableToObfuscate = false;
       }else{
           ableToObfuscate = true;
       }
   }
   
   function importModel(){
       //Se recorre el modelo y se importa las columnas seleccionadas.
       showSuccess("Se ha importado con éxito el modelo.");
       var columnsToImport;
       for(var indexTable = 0; indexTable < tablesImported.length; indexTable++ ){
           var tableToImportName = tablesImported[indexTable].name;
           var columns = tablesImported[indexTable].columns;           
            
           //var tablesInView = databaseObject.database.tables;           
           //Fix to IE 11
           var tablesInView = Array.prototype.slice.call(databaseObject.database.tables);           
           
           columnsToImport = columns.filter( function(columnLooking){
                return columnLooking.isSelected;
            });  
           //Looking if the table exist already:
           var existedIndex = tablesInView.findIndex(function(tableLooking){
               return tableLooking.name === tableToImportName;
           });
           if(existedIndex > -1){               
                //Checking if there's any column to import from this table:
                columnsToImport.forEach(function(columnToImport,index){                                  
                    //Check if the column exist
                    var existedColumnIndex = tablesInView[existedIndex].columns.findIndex(function(columnLooking){
                        return columnLooking.name === columnToImport.name;
                    });
                    if(existedColumnIndex === -1){
                        tablesInView[existedIndex].columns.push(columnToImport);
                    }  
                });  
           }else if(columnsToImport.length > 0){ 
                //If the table do no exist... 
                databaseObject.database.tables.push({
                    name: tableToImportName,
                    columns: columnsToImport
                });
            }
       }
       updateTables();
       isDataModified= true;
       $("#modelModal").modal('hide');
   }
  
   
