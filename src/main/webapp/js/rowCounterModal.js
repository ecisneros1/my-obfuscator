/* 
 * This file is included in the jsp tableCountModal.jsp. 
 * This file use variables defined in the principalPanel.js.
 */
function fillEnvironmentSelector(){
    var  environments = databaseObject.database.environments;
    $("#rowCounterListContainer").empty();   
    $("#rowCountEnvironmentSelectList").empty();   
    var element = document.createElement('option');
    element.disabled = false;
    element.innerHTML = "Seleccione un ambiente";
    
    $("#rowCountEnvironmentSelectList").append(element);
    
    for(var i = 0; i <  databaseObject.database.environments.length; i++){
        element = document.createElement('option');
        element.value= i;
        element.innerHTML = environments[i].name + " - "  + environments[i].host;
        $("#rowCountEnvironmentSelectList").append(element);
    }
    
}
function fillRowsCountList(tableList){
    console.log(tableList);
    
    for(var i = 0; i < databaseObject.database.tables.length; i++){
        var element = document.createElement('div');
        var tableName = databaseObject.database.tables[i].name;
        element.innerHTML = tableName + ": "  + tableList[tableName];
        element.className = "chip-fluid";
        $("#rowCounterListContainer").append(element);
    }
}
$(document).ready(function(){
    $("#btnCountRows").click(function(e){
        e.preventDefault();                    
        var databaseName = $("#inputDatabaseName").val();
        var vendorDataBase = $("#venderSelect").val().split("-")[0];
        var versionDataBase = $("#venderSelect").val().split("-")[1];
        var envIndex = $("#rowCountEnvironmentSelectList").val();
        var environment = databaseObject.database.environments[envIndex];
        if(!environment){
            showError("Debe escoger un ambiente para poder contar los registros");
            return;
        }
        var tableList = [];
        for(var i = 0; i < databaseObject.database.tables.length; i++){
            tableList.push(databaseObject.database.tables[i].name);
        }
        
        
        var dataObject = {
            databaseName: databaseName,
            vendorDataBase: vendorDataBase,
            versionDataBase: versionDataBase,
            environment: environment,
            tableList: tableList
        };
        
        if(validateInputs()){           
            var data = JSON.stringify(dataObject);
            
            $.ajax({
                url: "countrowstables.action",
                data: data,
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                async: true,
                statusCode:{
                    403: function(request, status, error) {
                        showError("No posees los permisos suficientes para realizar esta acción.");
                    }
                },
                success: function (res) {                
                    if(res.result==="success"){
                        showSuccess(res.message);
                        console.log(res);
                        fillRowsCountList(res.tableRowCounter);
                        
                    }else{
                        showError(res.message);
                        validateSession(res.result);
                    }                 
                }
            });  
        }
        
    });
    
    $("#btnOpenRowCount").click(function(e){
        e.preventDefault();
        fillEnvironmentSelector();
        $("#rowsCounterModal").modal("show");
    });
});

