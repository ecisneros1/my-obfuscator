/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Ajax loading animation:

$(document).on({
    ajaxStart: function() { $(".ajaxBackground").addClass("loading"); $(".ajaxLoadingAnimation").css("display","inline");  },
    ajaxStop: function() { $(".ajaxBackground").removeClass("loading"); $(".ajaxLoadingAnimation").css("display","none");  }    
});

//Globals
var databaseObject = {};
var envEditIndex = -1;
var tableEditIndex = -1;
var columnEditIndex = -1;
var activeDB;
var isDataModified= false;
var logsList = [];
var logLines = [];
var logsFontSize = 1;
var logsMinFontSize = 1;
var logsMaxFontSize = 3;

//Functions
var ableToEdit= false;
var ableToReport= false;
var ableToAudit= false;
var ableToObfuscate= false;
PNotify.prototype.options.styling = "fontawesome";
var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};

$(document).ready(function(){  
    checkFunctions();
    loadSessionVar();
    $("#btnExport").hide();
    
    $('[data-toggle="tooltip"]').tooltip();
    // VALIDATIONS -----------------------------------------------------------
    // VALIDATIONS -----------------------------------------------------------
    $("#inputDatabaseSearch").on({
        keyup: function(e) {           
          filterDatabaseList($("#inputDatabaseSearch").val());
        }
    });
    $("input.no-specialcharacter-allowed").on({
        keydown: function(e) {
          this.value = this.value.replace(/[!@#\$%\^\&\*\)\(\+\=\]<>\[¿?\|\¬~¡"*]/g, "");
          this.value = this.value.replace(/\s*\n*/g, "");
          //Si intenta colocar un espacio return;
          if (e.which === 32)
            return false;
        },
        focus: function (){
            this.value = this.value.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            this.value = this.value.replace(/\s*\n*/g, "");
        },
        blur: function (){
            this.value = this.value.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
            this.value = this.value.replace(/\s*\n*/g, "");
        },
        change: function() {
          this.value = this.value.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>¿?¡"\|\¬~]*/g, "");
          this.value = this.value.replace(/\s*\n*/g, "");
        },
        paste: function(event){            
            let paste = (event.originalEvent.clipboardData || window.clipboardData).getData('text');            
            paste = paste.replace(/[!@#\$%\^\&\*\)\[\]\(\+\=<>\.¿?¡"\|\¬~]*/g, "");
            paste = paste.replace(/\s*\n*/g, "");            
            event.originalEvent.clipboardData.text = paste;            
            event.target.value += paste;
            return false;
        }
    });
    
    $("#btnRefreshDatabases").on("click",function(){
       getLogsList(); 
    });
    $("#btnInfoFilter").on("click",function(){
       updateLogLinesFilter("[INFO"); 
    });
    $("#btnErrorFilter").on("click",function(){
       updateLogLinesFilter("[ERROR]"); 
    });
    $("#btnNoneFilter").on("click",function(){
        updateLogLines(); 
    });
    $("#btnIncreaseFont").on("click",function(){
        increaseFontSize(); 
        updateFontSize();
    });
    $("#btnDecreaseFont").on("click",function(){
        decreaseFontSize(); 
        updateFontSize();
    });
    
   
    //General chips activation/deactivation
    $(document).on("click",".chip-fluid, .chip ",function(){  
        var isAlreadyActive = $(this).hasClass("active");
        if(!isAlreadyActive){
            $(this).parent().children().each(function(index, element){
                $(this).removeClass("active");
            }); 
        }
        
        $(this).toggleClass("active");
    });
    
   
   
    //Log selected
    $(document).on("click","#logListContainer > .chip-fluid",function(){       
        var scopeThis = $(this);      
        requestLog(scopeThis);
        
    });
    
    $("#inputLogSearch").on({
        keyup: function(e) {           
          filterLogList($("#inputLogSearch").val());
        }
    });
        
   
    // CUSTOM EVENTS ---------------------------------------------------------
    //Activación o desactivación de un Chips de Database.
    $(".data-modified-control").on("database:toggle", function(){        
        
        if($(".database-chip").is(".active")){    
            if(ableToReport) toggleDisableButton("#btnExport", false);
            if(!ableToObfuscate){
                toggleDisableButton("#btnObfuscate", true);
            }
        }else{           
            toggleDisableButton("#btnExport", true );  
        }
       
    });
    
    
   
    //Fill the list of logs.
    getLogsList();
});

function filterLogList(filter){
        //databaseList[]
      
        $("#logListContainer").empty();
        for(var i = 0; i < logsList.length; i++ ){
            textToFilter = logsList[i].trim();            
            textToFilter = textToFilter.toLowerCase();
            filterBoolean = textToFilter.indexOf(filter.toLowerCase()) >= 0;
            if(!filterBoolean) {                
                continue;
            }
            var element = document.createElement('div');
            var img = document.createElement('img');
            var span = document.createElement('span');
            var icon = document.createElement('i');
            
            element.className = "database-chip chip-fluid icon-adjust";
            span.className = "chipText";
            //img.src = "img/icons/database-icon2.png";
            span.innerHTML =" " +logsList[i] ;
            icon.className="fas fa-file-alt";
            element.appendChild(icon);
            element.appendChild(span);
            
            element.id= logsList[i];            
            
            $("#logListContainer").append(element);
            
        }
        
    }
function customConfirm(options){
    if(options.message){
        $("#messageBodyModal").html(options.message);        
        
        $("#avisoModalControlls").empty();
        if(options.yesButton){
            //console.log("On yes button binding event");
            $("#avisoModalControlls").append('<button type="button" data-dismiss="modal" class="btn btn-outline-secondary">Cancelar</button>');
            var actionButton = document.createElement('button');
            actionButton.className = "btn btn-outline-primary";
            actionButton.type = "button";
            actionButton.dataset.dismiss = "modal";
            actionButton.onclick = options.yesButton;
            actionButton.innerHTML = "Aceptar";
            $("#avisoModalControlls").append(actionButton);
            
        }else{
            $("#avisoModalControlls").html('<button type="button" data-dismiss="modal" class="btn btn-outline-primary">Aceptar</button>');
        }
        $("#messageModal").modal('show');
       
    }  
}
 function requestLog(scopeThis){
        var logId = scopeThis.prop('id');        
        
        $("#labelDatabaseOnForm").text("Log: "+ logId);

        activeDB= logId;            
        //console.log("Sending data to action AJAX");
        var xmlName = logId.trim()+".log";            
        var xmlNameObject = {'name': xmlName };
        //console.log(xmlNameObject);
        var data = JSON.stringify(xmlNameObject);

        $.ajax({
            url: "log.action",
            data: data,
            dataType: 'json',
            contentType: 'application/json',
            type: 'POST',
            cache: false,
            async: true,
            statusCode:{
                403: function(request, status, error) {
                    showError("No posees los permisos suficientes para realizar esta acción.");
                }
            },
            success: function (res) {                
                console.log(res);
                $("#logsContainer").empty();
                if(!validateSession(res.result)) return;                    
                logLines = res.logs;  
                updateLogLines();
                showSuccess("Se ha cargado el log seleccionado");
            },
            error: function(){                    
                showError("Ha ocurrido un error inesperado");
            }

        });
        
        $(".data-modified-control").trigger("database:toggle");
    }
    
    function getLogsList(){
        var data = JSON.stringify({});
        $.ajax({
                url: "listlogs.action", 
                data: data,
                dataType: 'json',
                contentType: 'application/json',
                type: 'POST',
                cache: false,
                async: true,
                statusCode:{
                    403: function(request, status, error) {
                        showError("No posees los permisos suficientes para realizar esta acción.");
                    }
                },
                success: function (res) {                
                    //if(!validateSession(res.result)) return;
                    console.log(res);
                    logsList = res.dbItems; 
                    updateLogsList();
                    
                },
                error: function(){
                    
                    showError("Ha ocurrido un error inesperado al intentar obtener las lista de Base de datos.");
                }
                
            });
    }
    function updateLogsList(){
        //databaseList[]       
        $("#logListContainer").empty();
        //logsList.sort();
        for(var i = 0; i < logsList.length; i++ ){
            var element = document.createElement('div');
            var img = document.createElement('img');
            var span = document.createElement('span');
            var icon = document.createElement('i');
            
            if(logsList[i].indexOf("gui") > 0){
                continue;
            }
            
            element.className = "database-chip chip-fluid icon-adjust";
            span.className = "chipText";
            //img.src = "img/icons/database-icon2.png";
            span.innerHTML =" " +logsList[i] ;
            icon.className="fas fa-file-alt";
            element.appendChild(icon);
            element.appendChild(span);
            
            element.id= logsList[i];
            element.dataset.databaseName = logsList[i];           
            
            /*addControlsButtons(element,{
                'editControl':false,
                'closeControl':true
            });*/
            
            $("#logListContainer").append(element);
            
        }
        
    }
    function filterDatabaseList(filter){
              
        $("#logListContainer").empty();
        for(var i = 0; i < databaseList.length; i++ ){
            textToFilter = databaseList[i].databaseName.trim() + "-"+ databaseList[i].appOwner.trim();            
            textToFilter = textToFilter.toLowerCase();
            filterBoolean = textToFilter.indexOf(filter.toLowerCase()) >= 0;
            if(!filterBoolean) {                
                continue;
            }
            var element = document.createElement('div');
            var img = document.createElement('img');
            var span = document.createElement('span');
            var icon = document.createElement('i');
            
            element.className = "database-chip chip-fluid icon-adjust";
            span.className = "chipText";
            span.innerHTML =" " +databaseList[i].appOwner  + "-" + databaseList[i].databaseName;
            icon.className="fas fa-file-alt";
            element.appendChild(icon);
            element.appendChild(span);
            element.id= databaseList[i].name ;
            element.dataset.databaseName = databaseList[i].databaseName;
            element.dataset.appOwner = databaseList[i].appOwner;
            
            $("#logListContainer").append(element);
            
        }
        
    }
    
    function validateSession(result){
        
        if(result === undefined || result === null || result.toLowerCase().indexOf("session")>=0){
            setTimeout(function(){
                 window.location.href="/obfuscator/index";
            },3000);
            return false;
        }else{
            return true;
        }
    }
    
    function showError(message){
        var notice = new PNotify({
            text: '<em>'+ message + '</em>',
            type: 'error',
            addclass: 'stack-bottomright translucent',
            stack: stack_bottomright,
            icon: 'fas fa-exclamation',
            hide: false,
            buttons: {
                sticker: false
            },
            animate: {
                animate: true,
                in_class: 'bounceInLeft',
                out_class: 'bounceOutRight'
            }
        });
   
        notice.get().click(function() {
            $(this).remove();
        });
    }
    function showSuccess(message){
         var notice = new PNotify({
            text: message,
            type: 'success',
            addclass: 'stack-bottomright translucent',
            stack: stack_bottomright,
            icon: 'fas fa-check-double',
            hide: true,
            delay: 4000,
            buttons: {
                sticker: false
            },
            animate: {
                animate: true,
                in_class: 'bounceInLeft',
                out_class: 'bounceOutRight'
            }
        });
        notice.get().click(function() {
            $(this).remove();
        });
    }
   function toggleDisableButton(selector, isDisabled){
       if(isDisabled){
            $(selector).addClass("disabled btn-outline-secondary");
            $(selector).removeClass("btn-outline-danger");
            $(selector).prop('disabled', true);
       }else{
            $(selector).removeClass("disabled btn-outline-secondary");
            $(selector).addClass("btn-outline-danger");
            $(selector).prop('disabled', false); 
       }       
   }
   function checkFunctions(){
       if(funcs.indexOf("Editar")< 0){
           toggleDisableButton(".btnEditFunction",true);    
           ableToEdit = false;
       }else{
           ableToEdit = true;
       }
       
       if(funcs.indexOf("Reportar")< 0){
            toggleDisableButton(".btnReportFunction",true);           
            ableToReport = false;
       }else{
           ableToReport = true;
       }
       
       if(funcs.indexOf("Auditar")< 0){
            toggleDisableButton(".btnAuditarFunction",true);           
            ableToAudit = false;
       }else{
           ableToAudit = true;
       }
       if(funcs.indexOf("Obfuscar")< 0){
            toggleDisableButton("#btnObfuscate",true);           
            ableToObfuscate = false;
       }else{
           ableToObfuscate = true;
       }
   }
   function updateLogLines(){
       $("#logsContainer").empty();
       for(var i = 0; i < logLines.length; i++ ){
           if(!checkSpecialLogTag(logLines[i])) {               
                var element = document.createElement('div');                        
                var span = document.createElement('span');                        

                element.className = "";
                span.className = "chipText";                        
                span.innerHTML =" " +logLines[i] ; 
                styleLines(span);
                element.appendChild(span);
                element.id= i;    
                $("#logsContainer").append(element);
            }
        }
   }
   function checkSpecialLogTag(line){
        var result = false;
        if(line.indexOf("{BR}") > 0){
            var element = document.createElement('br');
            $("#logsContainer").append(element);
            result = true;
        }
        if(line.indexOf("{HR}") > 0){
            var element = document.createElement('hr');
            $("#logsContainer").append(element);
            result = true;
        }
        
        return result;
   }
   function updateLogLinesFilter(filter){
        $("#logsContainer").empty();
        for(var i = 0; i < logLines.length; i++ ){
           if(logLines[i].indexOf(filter) >=0 ){               
                var element = document.createElement('div');                        
                var span = document.createElement('span');                        
                
                element.className = "";
                span.className = "chipText";                        
                span.innerHTML =" " +logLines[i] ; 
                styleLines(span);
                element.appendChild(span);
                element.id= i;    
                $("#logsContainer").append(element);
           }
        }
   }
   function styleLines(span){
       if(span.innerHTML.indexOf("[INFO") > 0){
           span.className += " text-info";
       }else if(span.innerHTML.indexOf("[ERROR") > 0){
           span.className += " text-danger";
       }
   }
   function increaseFontSize(){
       var errorMargin = logsMaxFontSize - logsFontSize;
       if(logsFontSize < logsMaxFontSize &&  errorMargin >= 0.5){
           logsFontSize = logsFontSize + 0.5; //JS decimals are not exact, some times can return a error of 0,0000001.
       }else{
           showError("Ha alcanzado el tamaño máximo de letra.");
       }
   }
   function decreaseFontSize(){
       var errorMargin =  logsFontSize - logsMinFontSize;
       if(logsFontSize > logsMinFontSize &&  errorMargin >= 0.5){
           logsFontSize = logsFontSize - 0.5;
       }else{
           showError("Ha alcanzado el tamaño minimo de letra.");
       }
   }
   function updateFontSize(){
       $("#logsContainer").css("font-size", logsFontSize + "em"); 
       localStorage.setItem('logsFontSize', logsFontSize);
   }
   function loadSessionVar(){       
       if(localStorage.getItem('logsFontSize')){
            logsFontSize = parseFloat(localStorage.getItem('logsFontSize')); //This prevent an string concatenation.
            updateFontSize();
       }
   }