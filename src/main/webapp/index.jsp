<%-- 
    Document   : index
    Created on : Aug 1, 2018, 1:54:13 PM
    Author     : arodrigues <arodrigues@itpatagonia.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="./views/includes/head.jsp" %>       
        <link href="${pageContext.request.contextPath}/css/login.css" rel="stylesheet" type="text/css">   
        
    </head>
    <body>
        <small style="bottom: 15%;position: absolute;right: 46%;left:49%;color: red;font-weight: 800;"><s:text name = "global.version"  /></small>
        <img id="logo-login" class="d-lg-block" src="<s:url action='ImageAction.action' />">
        <div id="container">
            <div class="loader">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
         <div id="login-container" class="d-flex justify-content-center align-items-center align-self-center">            
            <div class="login-block bb">
                
                <h2 style="
                    font-size: 2rem;
                    color: black;
                    text-align: center;
                    padding-top: 0.5em;
                    padding-bottom: 0.5em;
                    font-weight: 800;
                ">OBFUSCATOR</h2>
                <s:form action="login">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="addon-1">@</span>
                        </div>
                        <s:textfield name="username" class="form-control"  placeholder="Username" aria-label="Username" aria-describedby="addon-1" />
                        
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="addon-2">**</span>
                        </div>
                        <s:password name="password" class="form-control"  placeholder="Password" aria-label="Password" aria-describedby="addon-2" />
                        
                    </div> 
                    <div class="">
                        <s:actionerror />    
                    </div>
                    <div class="d-flex justify-content-center">                            
                        <s:submit key="global.login.loginbutton" type="button" class="btn btn-outline-danger btn-block" />                          
                    </div> 
                    
                    
                </s:form>
                            
            </div>                
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
    </body>
</html>

