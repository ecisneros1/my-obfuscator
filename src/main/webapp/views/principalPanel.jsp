<%-- 
    Document   : principalPanel
    Created on : Aug 1, 2018, 6:45:00 PM
    Author     : arodrigues <arodrigues@itpatagonia.com>
--%>

<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="./includes/head.jsp" %>
        <link href="${pageContext.request.contextPath}/css/principalPanel.css" rel="stylesheet" type="text/css">        
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
        
    </head>
    <body>
               
        <%@include file="./includes/navbar.jsp" %>
        <div class="container-fluid expanded-container">
            <div class="row">
                <div class="col col-sm-12 col-lg-3 expanded-container">
                    <div id="databaselistComponent" class="box-component full-viewport vertical-list-component">
                        <span class="headers"> <s:text name = "global.panel.label.database" /> </span>
                        <div id="databaseListContainer"class="expanded-container list-chips-container">
                            
                        </div>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend mb-1">
                                <span class="input-group-text" id="addon-searchBD"><span class="fas fa-search"></span></span>
                            </div>
                            <s:textfield id="inputDatabaseSearch" name="inputDatabaseSearch" aria-describedby="addon-searchBD" class="form-control no-specialcharacter-allowed" placeholder="Nombre de la BD" aria-label="Búsqueda BD" />                            
                        </div>
                        <div id="btnAddDatabase" class="btn btn-outline-danger btn-block"> 
                            <s:text name = "global.panel.button.newdb"  />
                        </div>
                    </div>
                </div>
                <div class="col col-sm-12 col-lg-9 expanded-container">
                    <div id="databaseForm" class="box-component full-viewport">
                        <span id="labelDatabaseOnForm" class="headers"><s:text name = "global.panel.button.newdb" /></span>
                        <span class="fas fa-edit" style="font-size: 1.5em; color: #eee;"></span>
                        <hr style="margin-top:  0.1em;">
                        <s:form action="panelProcessAction">                            
                            <div id="databaseInfoContainer" class="row ">
                                <div class="col col-s4">
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend mb-1">
                                            <span class="input-group-text" id="addon-nombreBD">Base/Schema</span>
                                        </div>
                                        <s:textfield id="inputDatabaseName" name="databaBaseName" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" aria-describedby="addon-nombreBD" placeholder="Nombre de la bd"/>                                    
                                    </div>
                                </div>
                                <div class="col col-s4">
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend mb-1">
                                            <span class="input-group-text" id="addon-AppOwner">Aplicación</span>
                                        </div>
                                        <s:textfield id="inputAppOwner" name="inputAppOwner" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" aria-describedby="addon-AppOwner" placeholder="Nombre aplicación"/>                                    
                                    </div>
                                </div>
                                <div class="col col-s4"> 
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend mb-1">
                                            <span class="input-group-text" id="addon-nombreBD">Tipo BD</span>
                                        </div>
                                        <s:select 
                                               name="vendorVersion"
                                               list="vendors"                                  
                                               required="true"                                                
                                               class="form-control form-control-sm data-modified-control" 
                                               id="venderSelect"
                                        />  
                                    </div>
                                </div>
                                <div class="col col-s4 d-none">                                    
                                    <s:textfield name="version" class="form-control form-control-sm data-modified-control" type="text" placeholder="Version"/>
                                </div>
                            </div>    
                            <hr>
                            <div id="databaseEnviromentsContainer" class="componentMargins">
                                <h6><s:text name = "global.panel.label.environments" /></h6>
                                <div id="enviromentListContainer" class="container-to-clean">
                                    
                                </div>
                                <div class="input-group input-group-sm mb-3">
                                    <s:textfield id="inputEnviromentName" name="enviromentName" class="form-control no-specialcharacter-allowed data-modified-control" placeholder="identificador del ambiente" aria-label="Recipient's username" aria-describedby="button-addon2" />
                                    <div class="input-group-append">
                                        <button id="btnAddEnviroment" class="btn btn-outline-danger data-modified-control" type="button" id="button-addon2" data-toggle="tooltip" data-placement="top" title="Agregar el ambiente a la lista">+</button>
                                    </div>
                                </div>
                            </div> 
                                    
 
                            <div class="row componentMargins">                                
                                <div id="databaseTablesContainer" class="col col-sm-6 vertical-list-component">
                                    <h6><s:text name = "global.panel.label.table" /></h6>
                                    <div id="tablesListContainer" class="expanded-container list-chips-container container-to-clean">
                                                                     
                                    </div>
                                    <button id="btnAddTable" class="btn btn-outline-danger btn-block data-modified-control" style="height: 3em;" data-toggle="tooltip" data-placement="top" title="Agregar nueva tabla a la lista">
                                        <s:text name = "global.panel.button.newtable" />
                                    </button>
                                </div>                        
                                <div id="databaseColumnsContainer" class="col col-sm-6 vertical-list-component">
                                    <h6><s:text name = "global.panel.label.columns" /></h6> 
                                    <div id="columnsListContainer" class="expanded-container list-chips-container container-to-clean" data-table-bind="">
                                        
                                                                   
                                    </div>
                                    <button id="btnAddColumn" class="btn btn-outline-danger btn-block data-modified-control" style="height: 3em;" data-toggle="tooltip" data-placement="top" title="Agregar nueva columna a la lista">
                                        <s:text name = "global.panel.button.newcolumn" />
                                    </button>
                                </div>                        
                            </div>
                            <div class="row">
                                <div class="col col-2  offset-6">  
                                    <a id="btnOpenRowCount" class="btn btn-outline-danger btn-block"  href="#" role="button" data-toggle="tooltip" data-placement="top" title="Obtiene la cantidad de rows existentes para cada tabla definida."><i class="fas fa-search " ></i> Contar Rows </a>                                        
                                </div>
                                <div class="col col-2">  
                                    <a id="btnExport" class="btn btn-outline-danger btn-block btnReportFunction"  href="#" role="button" data-toggle="tooltip" data-placement="top" title="Exportar el detalle de la BD Seleccionada"><i class="fas fa-file-download " ></i> Exportar</a>                                        
                                </div>
                                <div class="col col-2 ">
                                    <s:submit id="btnGuardarXML" key="global.panel.button.save" type="button" class="btn btn-danger btn-block btnEditFunction" data-toggle="tooltip" data-placement="top" title="Guardar la información de la base de datos en el sistema"/> 
                                </div>
                            </div>
                        </s:form>
                    </div>
                </div>
            </div>
            
        </div>
                    
         <%@include file="./includes/modals/environmentModal.jsp" %>           
         <%@include file="./includes/modals/tableModal.jsp" %>           
         <%@include file="./includes/modals/columnModal.jsp" %>           
         <%@include file="./includes/modals/messageModal.jsp" %>           
         <%@include file="./includes/modals/modelModal.jsp" %>            
        
                        
        <div class="ajaxBackground"></div> 
        <script src="${pageContext.request.contextPath}/js/IESupportForEach.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/IESupportFindIndex.js" type="text/javascript"></script>
        <script>
            var count = <%= (new Date()).getTime()%>;
            var funcs = "<s:property value="functions" />" ;
        </script>
                    
        <%@include file="./includes/javascriptConstants.jsp" %>
        <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.bundle.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pnotify.custom.min.js" type="text/javascript"></script>        
        <script src="${pageContext.request.contextPath}/js/principalPanel.js"></script>
<!--        Includes that must be call after principalPanel.js is loaded.-->
        <%@include file="./includes/modals/tableCountModal.jsp" %>
        
    </body>
</html>
