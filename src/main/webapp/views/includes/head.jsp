<%-- 
    Document   : head
    Created on : Aug 13, 2018, 11:34:43 AM
    Author     : AR
--%>

<title>Obfuscator</title>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=11" >
<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="0">
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/css/common.css" rel="stylesheet" type="text/css">  
<link href="${pageContext.request.contextPath}/img/icons/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
