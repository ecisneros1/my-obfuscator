 <div class="modal fade" id="environmentModal" tabindex="-1" role="dialog" aria-labelledby="enviromentModalLabel" aria-hidden="true">
    <div class="modal-dialog .modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="enviromentModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <s:form action="panelProcessAction" method="createEnviroment">
                    
                    <h6>Nombre Ambiente</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="inputEnviromentNameEdit" name="inputEnviromentNameEdit" class="form-control form-control-sm data-modified-control hide adjust-to-helptext no-specialcharacter-allowed" type="text" placeholder="Identificador Ambiente"/>
                        <small id="envHostHelpBlock" class="form-text text-muted">
                            Ej: dev
                        </small>
                    </div>
                    
                    <h6>Host - Puerto</h6>
                    <div class="input-group input-group-sm ">
                        <s:textfield id="host" name="host" aria-describedby="hostHelpBlock" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" placeholder="Host Url"/>
                        <s:textfield id="port" name="port" maxlength="5" aria-describedby="portHelpBlock" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" placeholder="Puerto"/>
                    </div>
                    <small id="portHelpBlock" class="form-text text-muted mb-3">
                        Host Ej: 192.168.0.0 - miServer.com / Port Ej: 1531 - S�lo n�meros desde 0 hasta 65536 
                    </small>

                    <h6>Service Name/Instancia</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="serviceName" name="serviceName" aria-describedby="serviceHelpBlock" class="form-control form-control-sm data-modified-control adjust-to-helptext no-specialcharacter-allowed" type="text" placeholder="Service Name"/>
                        <br><small id="serviceHelpBlock" class="form-text text-muted">
                            Ej: XE - Puede dejarse vacio.
                        </small>
                    </div>
                    
                    <h6>Diferente Base/Schema</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="inputOverrideNameEdit" name="inputOverrideNameEdit" class="form-control form-control-sm data-modified-control hide adjust-to-helptext no-specialcharacter-allowed" type="text" placeholder="Base/Schema"/>
                        <small id="envHostHelpBlock" class="form-text text-muted">
                            Permite conectarse y ofuscar la misma base, cuando esta posee un nombre/schema distinto en el ambiente.
                        </small>
                    </div>
                    
                    <h6>MSC ID</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="mscID" name="mscID" aria-describedby="mscHelpBlock" class="form-control form-control-sm data-modified-control adjust-to-helptext no-specialcharacter-allowed" type="text" placeholder="MSC ID" value="%{defaultMSC}"/>
                        <br><small id="mscHelpBlock" class="form-text text-muted">
                            Ej: ID del credencial de MSC
                        </small>
                    </div>
                    
                </s:form>
                
                <div class="d-flex justify-content-around" role="group" aria-label="Test Connection and Model">
                    <button id="btnTestConnection" class="btn btn-outline-secondary data-modified-control">
                        <s:text name = "global.panel.button.testConnection" />
                    </button>
                    <button id="btnTestModel" class="btn btn-outline-secondary data-modified-control">
                        <s:text name = "global.panel.button.testModel" />
                    </button>
                    <button id="btnImportModelEnv" class="btn btn-outline-secondary data-modified-control">
                        <s:text name = "global.panel.button.importModelEnv" />
                    </button>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button id="btnObfuscate" class="btn btn-outline-danger btn-block data-modified-control">
                    <s:text name = "global.panel.button.obfuscate" />
                    <img src="${pageContext.request.contextPath}/img/ajax-loader.gif" alt="loading.." class="ajaxLoadingAnimation" style="height: 1.2em; width: 1.2em">
                </button>
                <button id="btnSlowObfuscate" class="btn btn-outline-danger data-modified-control">
                    <s:text name = "global.panel.button.slow.obfuscate" />
                    <img src="${pageContext.request.contextPath}/img/ajax-loader.gif" alt="loading.." class="ajaxLoadingAnimation" style="height: 1.2em; width: 1.2em">
                </button>
                <button id="btnSaveEnviroment" type="button" class="btn btn-outline-primary">
                    <s:text name = "global.panel.button.save" />
                </button>
            </div>
        </div>
    </div>
</div>
