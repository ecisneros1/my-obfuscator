<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="avisoModalLabel"><s:text name = "global.panel.modal.aviso.title" /></h5>                        
            </div>
            <div  class="modal-body">
                <h6 id="messageBodyModal"></h6>
            </div>
            <div id="avisoModalControlls"class="modal-footer">                        
                <button type="button" data-dismiss="modal" class="btn btn-outline-primary">Aceptar</button>
            </div>
        </div>
    </div>            
</div>
