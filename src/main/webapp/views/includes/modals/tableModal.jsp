<div class="modal fade" id="tableModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tableModalLabel"><s:text name = "global.panel.modal.table.title" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <s:form action="panelProcessAction" method="createTable">
                    <h6>Nombre de la tabla</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="inputTableName" name="nombre" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" placeholder="Nombre de la tabla"/>                                
                    </div>       
                    <h6 >Block Size</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="inputBlockSize" name="inputBlockSize" class="form-control form-control-sm data-modified-control adjust-to-helptext no-specialcharacter-allowed" type="text" placeholder="Block Size"  />                                
                        <br>
                        <small id="serviceHelpBlock" class="form-text text-muted">
                            Cantidad de registros actualizados simultaneamente. Default: Vacio.  Max: 1000.
                        </small>
                    </div>                           
                </s:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button id="btnSaveTable" type="button" class="btn btn-outline-primary">Guardar Cambios</button>
            </div>
        </div>
    </div>            
</div>
