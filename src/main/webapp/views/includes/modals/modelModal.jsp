<div class="modal fade" id="modelModal" tabindex="-1" role="dialog" aria-labelledby="modelModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modelModalLabel"><s:text name = "global.panel.modal.model.title" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <s:form action="panelProcessAction" method="createTable">
                    <h6>Seleccione las tablas y columnas a importar:</h6>
                    <div id="databaseModelContainer" class="col col-sm-12 vertical-list-component">
                        <h6><s:text name = "global.panel.label.table" /></h6>
                        <div class="input-group input-group-sm mb-3">                                    
                            <select id="tableSelectList" class="form-control form-control-sm">
                                <option disabled>Seleccione una tabla</option>
                            </select>
                            <div class="input-group-append">
                                <label class="input-group-text" for="tableSelectList"><span id="explainModelImport" class="fas fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="right" title="Debes seleccionar una tabla de la lista y luego marcar las columnas que deseas. <b class='text-danger'>Puedes importar tantas tablas como quieras de una vez </b>, si seleccionas otra tabla, las columnas que hayas seleccionado de la anterior, se conservaran marcadas para importar."></span></label>
                            </div>

                        </div>
                        <h6><s:text name = "global.panel.label.columns" /></h6>
                        <small class="form-text text-danger " style="padding-bottom: 1em;">
                            No podr� seleccionar columnas que sean PK o FK, estaran marcadas con color azul y un 
                            <i class="fas fa-exclamation-circle"></i>.
                        </small>

                        <div id="modelListContainer" class="expanded-container list-chips-container multiselect" style="height: 25em;">

                        </div>

                    </div>                         
                </s:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button id="btnImportModel" class="btn btn-outline-danger data-modified-control"  data-toggle="tooltip" data-placement="top" title="Importa las tablas y columnas seleccionadas desde la metadata">
                    <s:text name = "global.panel.button.importModel" />
                </button>
            </div>
        </div>
    </div>            
</div>
