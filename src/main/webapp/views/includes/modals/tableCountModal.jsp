<div class="modal fade" id="rowsCounterModal" tabindex="-1" role="dialog" aria-labelledby="modelModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rowsCounterModalLabel"><s:text name = "global.panel.modal.table.count.title" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <s:form action="panelProcessAction" method="createTable">
                    <h6><s:text name = "global.panel.modal.table.count.selectEnvironment" /></h6>
                    <div id="rowCounterContainer" class="col col-sm-12 vertical-list-component">
                        <h6><s:text name = "global.panel.label.environments" /></h6>
                        <div class="input-group input-group-sm mb-3">                                    
                            <select id="rowCountEnvironmentSelectList" class="form-control form-control-sm">
                                <option disabled><s:text name = "global.panel.modal.table.count.selectAnEnvironment" /></option>
                            </select>
                        </div>
                        <h6><s:text name = "global.panel.label.table" /></h6>
                        <p class="form-text text-danger " style="padding-bottom: 1em;">
                            <s:text name = "global.panel.modal.table.count.description" />
                            <i class="fas fa-exclamation-circle"></i>.
                        </p>

                        <div id="rowCounterListContainer" class="expanded-container list-chips-container" style="height: 25em;">

                        </div>

                    </div>                         
                </s:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">
                    <s:text name = "global.panel.button.close" />
                </button>
                <button id="btnCountRows" class="btn btn-outline-danger data-modified-control"  data-toggle="tooltip" data-placement="top" title="Busca la cantidad de rows para cada tabla">
                    <s:text name = "global.panel.button.countRows" />
                </button>
            </div>
        </div>
    </div>            
</div>
<script src="${pageContext.request.contextPath}/js/rowCounterModal.js"></script>