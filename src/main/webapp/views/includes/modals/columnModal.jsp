<div class="modal fade" id="columnModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="columnModalLabel"><s:text name = "global.panel.modal.column.title" /></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <s:form action="panelProcessAction" method="createColumn">
                    <h6>Nombre de la columna</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:textfield id="inputColumnName" name="nombre" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" placeholder="Nombre de la columna"/>                                
                    </div>               

                    <div style="display:none" class="input-group input-group-sm mb-3">
                        <s:textfield id="inputColumnDataType" name="dataType" class="form-control form-control-sm data-modified-control no-specialcharacter-allowed" type="text" placeholder="Tipo de data"/>                                
                    </div>
                    <h6>M�todo de Ofuscamiento</h6>
                    <div class="input-group input-group-sm mb-3">
                        <s:select 
                            name="obfuscationMethods"
                            list="methods"                                
                            class="form-control form-control-sm data-modified-control" 
                            id="selectObfuscationMethod"
                        /> 
                    </div>

                    <div id="paramsContainer" class="">
                        <h6 class="fixedValueParam methodsParam">Valor fijo</h6>
                        <div class="input-group input-group-sm mb-3 fixedValueParam methodsParam">
                            <s:textfield id="fixedValue" name="inputFixedValue" class="form-control form-control-sm data-modified-control  methodsParam fixedValueParam active" type="text" placeholder="Valor Fijo"/>                                
                        </div>      
                        <h6 class="CopyFromValueParam methodsParam">Columna a copiar</h6>
                        <div class="input-group input-group-sm mb-3 CopyFromValueParam methodsParam">
                            <s:textfield id="CopyFromValue" name="inputCopyFromValue" class="form-control form-control-sm data-modified-control methodsParam CopyFromValueParam active" type="text" placeholder="Nombre de la Columna"/>                                
                        </div>                                 
                    </div>
                </s:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button id="btnSaveColumn" type="button" class="btn btn-outline-primary">Guardar Cambios</button>
            </div>
        </div>
    </div>            
</div>
