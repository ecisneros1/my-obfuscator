<%-- 
    Document   : navbar
    Created on : Aug 13, 2018, 11:54:04 AM
    Author     : arodrigues <arodrigues@itpatagonia.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    //Instead of the meta tags, this approach it's better because the server's headers
    //have priority over the metatags. So if the server define a cache policy the
    //browser will pay attention to it instead of the meta tags.
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");//HTTP 1.1
    response.setHeader("Pragma", "no-cache"); //HTTP 1.0
    response.setDateHeader("Expires", 0); //prevents caching at the proxy server
    
%>


<nav class="navbar navbar-expand-lg  bg-light navbar-light componentMargins navbar-expand d-flex justify-content-between">
    <a class="navbar-brand" href="#" style="max-height: 40px;">
        <img src="<s:url action='ImageAction.action' />" alt="ITPATAGONIA" height="40px" style="max-height: 40px; margin-top: -5px;" />
        <span style="font-weight: 700; font-family: monospace;" >Obfuscator</span>
    </a>
   
    <div class=" right" id="navbarNavAltMarkup"> 
        <div class="navbar-nav">
            <a id="btnHome" class="nav-item nav-link btnAuditarFunction" href="panel" data-toggle="tooltip" data-placement="bottom" title="Home"><i class="fas fa-home fa-lg"></i> </a>
            <a id="btnAuditarLogs" class="nav-item nav-link btnAuditarFunction" href="auditar" data-toggle="tooltip" data-placement="bottom" title="Visualizar logs de auditoria."><i class="fas fa-bullseye fa-lg"></i> </a>
            <a id="btnReportDatabases" class="nav-item nav-link btnReportFunction" href="databasesreport" data-toggle="tooltip" data-placement="bottom" title="Descargar reporte general"><i class="fas fa-file-download fa-lg"></i> </a>
            <a id="btnRefreshDatabases" class="nav-item nav-link active" href="#" data-toggle="tooltip" data-placement="bottom" title="Actualizar la lista de BD"><i class="fas fa-sync fa-lg"></i> </a>
            <a id="btnLogOut" class="nav-item nav-link" href="logout" data-toggle="tooltip" data-placement="bottom" title="Cerrar Session"><i class="fas fa-lg fa-sign-out-alt"></i></a>           
<!--        <div class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Acciones
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>-->
        </div>
    </div>    
     
</nav>
