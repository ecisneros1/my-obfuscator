<%--
    Document   : withoutPrivilegies
    Created on : Oct 9, 2018, 11:59:18 AM
    Author     : Angel Rodrigues <arodrigues@itpatagonia.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="../includes/head.jsp" %>
    </head>
    <body>
        <div class="container" style="margin-top: 25em;">
            <div class="row">
              <div class="col-sm">
                    
              </div>              
              <div class="col-sm col-md-8">
                <div class="alert alert-danger" role="alert">
                    <h4><s:text name = "global.errorpage.noPrivilegies"  /></h4>
                    <a type="button" class="btn btn-danger" href="login.action" >Regresar</a>
                </div>
              </div>              
              <div class="col-sm">
                
              </div>              
            </div>
        </div>
        <div>            
            
        </div>
    </body>
</html>
