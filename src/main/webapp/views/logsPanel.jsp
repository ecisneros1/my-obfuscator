<%-- 
    Document   : logsPanel
    Created on : Oct 11, 2018, 3:00:00 PM
    Author     : arodrigues <arodrigues@itpatagonia.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html>
    <head>
        <%@include file="./includes/head.jsp" %>
        <link href="${pageContext.request.contextPath}/css/principalPanel.css" rel="stylesheet" type="text/css">        
        <link href="${pageContext.request.contextPath}/css/logs.css" rel="stylesheet" type="text/css">        
        <link href="${pageContext.request.contextPath}/css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="${pageContext.request.contextPath}/css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
        
    </head>
    <body>    
        <%@include file="./includes/navbar.jsp" %>
        <div class="container-fluid expanded-container">
            <div class="row">
                <div class="col col-sm-12 col-lg-3 expanded-container">
                    <div id="logslistComponent" class="box-component full-viewport vertical-list-component">
                        <span class="headers"> <s:text name = "global.logs.panel.label.logs" /> </span>                        
                        <div id="logListContainer"class="expanded-container list-chips-container">
                            
                        </div>
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend mb-1">
                                <span class="input-group-text" id="addon-searchBD"><span class="fas fa-search"></span></span>
                            </div>
                            <s:textfield id="inputLogSearch" name="inputLogSearch" aria-describedby="addon-searchBD" class="form-control no-specialcharacter-allowed" placeholder="Búsqueda" aria-label="Búsqueda BD" />                            
                        </div>                        
                    </div>
                </div>              
                <div class="col col-sm-12 col-lg-9 expanded-container">
                    <div id="logsReaderComponent" class="box-component full-viewport vertical-list-component" >
                        <div class="row">                            
                            <span class="headers col-9"> <s:text name = "global.logs.panel.label.read" /> </span>
                            <div class="col-3 col d-flex align-items-center justify-content-between">                                
                                <span>Font:</span>
                                <span id="btnIncreaseFont" class="fas fa-plus-circle icon-btn"></span>
                                <span id="btnDecreaseFont"class="fas fa-minus-circle icon-btn"></span>
                                <span>Filtros:</span>
                                <button id="btnInfoFilter" class="btn badge badge-info">INFO</button>
                                <button id="btnErrorFilter" class="btn badge badge-danger">ERROR</button>                        
                                <button id="btnNoneFilter" class="btn badge badge-secondary">ALL</button>                        
                            </div>
                        </div>
                        <div id="logsContainer"class="expanded-container list-chips-container" >
                            
                        </div>
                                               
                    </div>
                </div>              
            </div>            
        </div>  
        <div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog .modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="avisoModalLabel"><s:text name = "global.panel.modal.aviso.title" /></h5>                        
                    </div>
                    <div  class="modal-body">
                        <h6 id="messageBodyModal"></h6>
                    </div>
                    <div id="avisoModalControlls"class="modal-footer">                        
                        <button type="button" data-dismiss="modal" class="btn btn-outline-primary">Aceptar</button>
                    </div>
                </div>
            </div>            
        </div>
        <div class="ajaxBackground"></div>            
        <script src="${pageContext.request.contextPath}/js/IESupportForEach.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/js/IESupportFindIndex.js" type="text/javascript"></script>
        <script>
            
            var funcs = "<s:property value="functions" />" ;
        </script>               
        <%@include file="./includes/javascriptConstants.jsp" %>
        <script src="${pageContext.request.contextPath}/js/jquery-3.3.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/popper.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.bundle.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/pnotify.custom.min.js" type="text/javascript"></script>        
        <script src="${pageContext.request.contextPath}/js/logsPanel.js"></script>
    </body>
</html>
