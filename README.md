## Obfuscator

This application Obfuscate data for databases using a simple and reversible algorithm (Work for SqlServer, Oracle, DB2).

###To run this application you will need:
	- Java 8
	- JCC (Java 8 Policies for encryptation)
	- Maven
	- Tomcat
	- IDE (Netbeans would be great)
	
###Also the aplication will try to connect to a LDAP server and an IdentityService, (this last one can be configured on the config.xml file to run with the FakeIdentityService. This is used in case there's no any IdentityService avalaible to connect to it. LDAP Server would be required and can be configured in the same file config.xml.







