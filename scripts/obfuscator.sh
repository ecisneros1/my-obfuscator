echo "**------------------------------------------------------"
echo "Iniciando Obfucator..."
databasename=$1
environment=$2
username=$3
echo "Se ofuscara: $databasename"
postdata="{\"databaseName\":\"$databasename\", \"environment\":\"$environment\", \"username\":\"$username\"}"

resultado=$(curl -s -H "Content-Type: application/json" -d \
"$postdata" http://buadlnx003:9084/obfuscator/remote | grep -Po '(?<=\"errorCode\":)(.*?)(?=,)'  )
echo "result: $resultado"
echo "**------------------------------------------------------"
return $resultado